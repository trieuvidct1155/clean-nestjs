import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { CreateProductBrandEntityDTO } from '@/infrastructure/data-transfers/productBrand/create-entitty.dto';
import { SlugKenZy } from '@/infrastructure/helpers/slug-kenzy';
import { ProductBrandRepository } from '@/infrastructure/repositories/productBrand.repository';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { PRODUCT_BRAND_UC_PROXY_TYPES } from './brand-uc-proxy.type';

export class CreateProductBrandsUseCase {
  constructor(
    private readonly _repository: ProductBrandRepository,
    private readonly slack: SlackService,
  ) {}

  async existErpCode(erpCode: string): Promise<boolean> {
    const signalGet = await this._repository.get({
      conditions: { erpCode: erpCode },
    });

    return !!signalGet;
  }

  async execute(payloads: CreateProductBrandEntityDTO): Promise<boolean> {
    try {
      const { erpCode, name, lang } = payloads;
      const slug = SlugKenZy.createURL(name);
      const signalGet = await this._repository.get({
        conditions: { slug: slug },
        populates: [],
        select: '',
      });
      const _isError = !!signalGet;
      if (_isError) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Create Brand: Đã tồn tại slug!',
          data: signalGet,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
        // log and slack
      }

      const existsErpCode = await this.existErpCode(erpCode);
      if (existsErpCode) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Create Brand: Đã tồn tại erpCode!',
          data: existsErpCode,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
      }

      const signalCreate = await this._repository.createOrUpdate({
        conditions: { erpCode: erpCode },
        dataUpdate: {
          name: name,
          slug: slug,
          lang: lang,
        },
        options: { upsert: true, new: true },
        populates: [],
      });

      const _slackType = !!signalCreate ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Create Brand: Create Brand!`,
        data: signalCreate,
        payload: payloads,
        type: _slackType,
      });

      return !!signalCreate;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: 'Message from API Create Brand: Create Brand Fail!',
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderCreateProductBrandsUseCaseProxy = {
  inject: [ProductBrandRepository, SlackService],
  provide: PRODUCT_BRAND_UC_PROXY_TYPES.CREATE_PRODUCT_BRAND_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ProductBrandRepository,
    slack: SlackService,
  ) => new UseCaseProxy(new CreateProductBrandsUseCase(_repository, slack)),
};
