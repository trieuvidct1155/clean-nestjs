import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { UpdateProductBrandEntityDTO } from '@/infrastructure/data-transfers/productBrand/update-entitty.dto';
import { SlugKenZy } from '@/infrastructure/helpers/slug-kenzy';
import { ProductBrandRepository } from '@/infrastructure/repositories/productBrand.repository';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { PRODUCT_BRAND_UC_PROXY_TYPES } from './brand-uc-proxy.type';

export class UpdateProductBrandsUseCaseProxy {
  constructor(
    private readonly _repository: ProductBrandRepository,
    private readonly slack: SlackService,
  ) {}

  async execute(payloads: UpdateProductBrandEntityDTO): Promise<boolean> {
    try {
      const { erpCode, name, lang } = payloads;
      const slug = SlugKenZy.createURL(name);
      const signalResponse = await this._repository.updateOne({
        conditions: { erpCode: erpCode },
        dataUpdate: {
          name: name,
          slug: slug,
          lang: lang,
        },
      });
      const _slackType = !!signalResponse ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Brand: Update Brand!`,
        data: signalResponse,
        payload: payloads,
        type: _slackType,
      });

      return !!signalResponse;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: `Message from API Brand: Update Brand fail!`,
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderUpdateProductBrandsUseCaseProxyProxy = {
  inject: [ProductBrandRepository, SlackService],
  provide: PRODUCT_BRAND_UC_PROXY_TYPES.UPDATE_PRODUCT_BRAND_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ProductBrandRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(new UpdateProductBrandsUseCaseProxy(_repository, slack)),
};
