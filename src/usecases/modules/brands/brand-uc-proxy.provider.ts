import { ProviderCreateProductBrandsUseCaseProxy } from './create-brand-uc-proxy.service';
import { ProviderUpdateProductBrandsUseCaseProxyProxy } from './update-brand-uc-proxy.service';

export const ProviderProductBrands = [
  ProviderCreateProductBrandsUseCaseProxy,
  ProviderUpdateProductBrandsUseCaseProxyProxy,
];
