export enum CATEGORY_UC_PROXY_TYPES {
  CREATE_CATEGORY_UC_PROXY = 'CREATE_CATEGORY_UC_PROXY',
  UPDATE_CATEGORY_UC_PROXY = 'UPDATE_CATEGORY_UC_PROXY',
  GET_LIST_CATEGORY_UC_PROXY = 'GET_LIST_CATEGORY_UC_PROXY',
}
