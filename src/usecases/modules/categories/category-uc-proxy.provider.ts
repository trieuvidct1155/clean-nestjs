import { ProviderCreateCategoriesUseCaseProxy } from './create-category-uc-proxy.service';
import { ProviderUpdateCategoriesUseCaseProxy } from './update-category-uc-proxy.service';

export const ProviderCategories = [
  ProviderCreateCategoriesUseCaseProxy,
  ProviderUpdateCategoriesUseCaseProxy,
];
