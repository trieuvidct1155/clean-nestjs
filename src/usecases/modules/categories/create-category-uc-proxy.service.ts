import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { CreateCategoryEntityDTO } from '@/infrastructure/data-transfers/category/create-entity.dto';
import { SlugKenZy } from '@/infrastructure/helpers/slug-kenzy';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { ProductCategoryRepository } from '@repositories/productCategory.repository';
import { ProductCategoryInfoRepository } from '@repositories/productCategoryInfo.repository';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import mongoose from 'mongoose';
import { CATEGORY_UC_PROXY_TYPES } from './category-uc-proxy.type';

export class CreateCategoriesUseCase {
  constructor(
    private readonly _repository: ProductCategoryRepository,
    private readonly _infoRepository: ProductCategoryInfoRepository,
    private readonly slack: SlackService,
  ) {}

  async existSlug(slug: string): Promise<boolean> {
    const signalGet = await this._infoRepository.get({
      conditions: { $or: [{ slug: slug }, { slug: slug }] },
      populates: [],
      select: '',
    });

    return !!signalGet;
  }

  async existErpCode(erpCode: string): Promise<boolean> {
    const signalGet = await this._repository.get({
      conditions: { erpCode: erpCode },
    });

    return !!signalGet;
  }

  private cloneInfoWithLang(info: any, lang): any {
    if (!info) return null;
    const clone = { ...info };

    clone['lang'] = lang;
    clone['_id'] = new mongoose.Types.ObjectId();
    clone['status'] = -1;
    clone['slug'] = info['slug'] + '-' + lang;
    clone['name'] = info['name'] + '-' + lang;

    return clone;
  }

  private async createCategoriesInfo({ name, slug, lang }) {
    const _idInfo = new mongoose.Types.ObjectId();
    const _id = new mongoose.Types.ObjectId();

    const status = 2;

    const payloadVi = {
      _id: _idInfo,
      name: name,
      slug: slug,
      lang: lang,
      status,
      productCategory: _id,
    };

    const payloadEn = this.cloneInfoWithLang(payloadVi, 'en');

    //create category Info
    const signalUpdateInfo = await this._infoRepository.createMany([
      payloadVi,
      payloadEn,
    ]);
    const [vi, en] = signalUpdateInfo;
    return { vi, en, _id };
  }

  private async createCategory({ _id, erpCode, vi, en }) {
    const dataUpdate = {
      _id,
      erpCode,
      vi: vi._id,
      en: en._id,
      viName: vi.name,
      enName: en.name,
      viSlug: vi.slug,
      enSlug: en.slug,
    };
    const signalUpdate = await this._repository.createOrUpdate({
      conditions: { _id: _id },
      dataUpdate: { $set: dataUpdate },
      options: { upsert: true, new: true },
    });

    return signalUpdate;
  }

  async execute(payloads: CreateCategoryEntityDTO): Promise<boolean> {
    try {
      const { erpCode, name, lang } = payloads;

      const slug = SlugKenZy.createURL(name);

      const existsSlug = await this.existSlug(slug);
      if (existsSlug) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Product Category: Đã tồn tại slug!',
          data: existsSlug,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
      }

      const existsErpCode = await this.existErpCode(erpCode);
      if (existsErpCode) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Product Category: Đã tồn tại erpCode!',
          data: existsErpCode,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
      }

      const { vi, en, _id } = await this.createCategoriesInfo({
        name,
        slug,
        lang,
      });

      const category = await this.createCategory({
        _id,
        erpCode,
        vi,
        en,
      });

      const _slackType = !!category ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Product Category: Create Product Category!`,
        data: category,
        payload: payloads,
        type: _slackType,
      });

      return !!category;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title:
          'Message from API Product Category: Create Product Category Fail!',
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderCreateCategoriesUseCaseProxy = {
  inject: [
    ProductCategoryRepository,
    ProductCategoryInfoRepository,
    SlackService,
  ],
  provide: CATEGORY_UC_PROXY_TYPES.CREATE_CATEGORY_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ProductCategoryRepository,
    _infoRepository: ProductCategoryInfoRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new CreateCategoriesUseCase(_repository, _infoRepository, slack),
    ),
};
