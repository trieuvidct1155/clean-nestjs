import { ELang } from '@/domain/decorators/lang.decorator';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { UpdateCategoryEntityDTO } from '@/infrastructure/data-transfers/category/update-entity.dto';
import { ProductCategoryRepository } from '@/infrastructure/repositories/productCategory.repository';
import { ProductCategoryInfoRepository } from '@/infrastructure/repositories/productCategoryInfo.repository';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { UseCaseProxy } from '@/infrastructure/usecases-proxy/usecases-proxy';
import { ProductCategoryEntity } from '@infrastructure/entities/productCategory.entity';
import { SlugKenZy } from '@infrastructure/helpers/slug-kenzy';
import { CATEGORY_UC_PROXY_TYPES } from './category-uc-proxy.type';

export class UpdateCategoriesUseCase {
  constructor(
    private readonly _repository: ProductCategoryRepository,
    private readonly _infoRepository: ProductCategoryInfoRepository,
    private readonly slack: SlackService,
  ) {}

  private async getCategory(erpCode: string): Promise<ProductCategoryEntity> {
    const signalGet = await this._repository.get({
      conditions: { erpCode: erpCode },
      populates: [],
      select: '',
    });
    return signalGet;
  }

  private dataUpdateByLang({ lang, name, slug }) {
    const dataUpdate = {};
    if (lang === ELang.VI) {
      dataUpdate['viName'] = name;
      dataUpdate['viSlug'] = slug;
    } else if (lang === ELang.EN) {
      dataUpdate['enName'] = name;
      dataUpdate['enSlug'] = slug;
    }

    return dataUpdate;
  }

  async execute(payloads: UpdateCategoryEntityDTO): Promise<boolean> {
    try {
      const { erpCode, name, lang } = payloads;

      const slug = SlugKenZy.createURL(name);

      const category = await this.getCategory(erpCode);
      if (!category) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Product Category: Không tồn tại!',
          data: category,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return !!category;
      }

      const signalUpdateInfo = await this._infoRepository.createOrUpdate({
        conditions: { productCategory: category._id, lang },
        dataUpdate: {
          name: name,
          slug: slug,
          lang: lang,
        },
        options: { upsert: true, new: true },
      });

      const signalUpdate = await this._repository.createOrUpdate({
        conditions: { _id: category._id },
        dataUpdate: {
          $set: {
            ...this.dataUpdateByLang({
              lang,
              name: signalUpdateInfo.name,
              slug: signalUpdateInfo.slug,
            }),
          },
        },
        options: { upsert: true, new: true },
        populates: [],
      });

      const _slackType = !!signalUpdate ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Product Category: Update Product Category!`,
        data: signalUpdate,
        payload: payloads,
        type: _slackType,
      });

      return !!signalUpdate;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title:
          'Message from API Product Category: Update Product Category Fail!',
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderUpdateCategoriesUseCaseProxy = {
  inject: [
    ProductCategoryRepository,
    ProductCategoryInfoRepository,
    SlackService,
  ],
  provide: CATEGORY_UC_PROXY_TYPES.UPDATE_CATEGORY_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ProductCategoryRepository,
    _infoRepository: ProductCategoryInfoRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new UpdateCategoriesUseCase(_repository, _infoRepository, slack),
    ),
};
