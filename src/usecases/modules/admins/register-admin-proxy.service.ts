import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import { ADMIN_UC_PROXY_TYPES } from './admin-uc-proxy.type';
import { ERPRepository } from '@/infrastructure/repositories/erp.repository';
import { BcryptService } from '@/infrastructure/services/bcrypt/bcrypt.service';
import mongoose from 'mongoose';
import { AUTH_ROLES } from '@/infrastructure/entities/erp.entity';

export class RegisterAdminUseCase {
    constructor(
        private readonly _repository: ERPRepository,
        private readonly bcryptService: BcryptService,

    ) { }

    async execute(payload: any): Promise<any> {
        try {
            const { password = '', name = '', username = '' } = payload;
            const hashedPassword = await this.bcryptService.hash(password);

            const getDataAdmin = await this._repository.get({
                conditions: {
                    username: username,
                },
            });
            if (getDataAdmin) {
                return {
                    error: true,
                    data: undefined,
                    message: 'account_already_exists',
                };
            }
            const conditions = {
                username: username,
                password: hashedPassword,
            };
            const data = {
                username: username,
                password: hashedPassword,
                name: name,
                roles: AUTH_ROLES.ADMIN,
            };
            const signalCreateAdmin = await this._repository.create(data);
            return {
                error: false,
                data: signalCreateAdmin,
                message: 'register_succeed',
            };
        } catch (error) {
            return {
                error: true,
                data: undefined,
                message: error,
            };
        }
    }
}

export const ProviderRegisterAdminUseCaseProxy = {
    inject: [
        ERPRepository, BcryptService
    ],
    provide: ADMIN_UC_PROXY_TYPES.RESGISTER_ADMIN_USECASE_PROXY,
    useFactory: (
        _repository: ERPRepository,
        bcryptService: BcryptService,
    ) =>
        new UseCaseProxy(new RegisterAdminUseCase(_repository, bcryptService)),
};
