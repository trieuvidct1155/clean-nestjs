import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import { ADMIN_UC_PROXY_TYPES } from './admin-uc-proxy.type';
import { ERPRepository } from '@/infrastructure/repositories/erp.repository';
import { BcryptService } from '@/infrastructure/services/bcrypt/bcrypt.service';
import { LoginErpConnectDTO } from '@/infrastructure/data-transfers/erp-connect/login.dto';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';

export class LoginAdminUseCase {
    constructor(
        private readonly _repository: ERPRepository,
        private readonly bcryptService: BcryptService,
        private readonly slack: SlackService,

    ) {
    }

    async execute(payload: LoginErpConnectDTO): Promise<any> {
        try {
            const { username, password } = payload;
            const signalGetAdmin = await this._repository.get({
                conditions: { username: username },
            });

            if (!signalGetAdmin) {
                this.slack.sendSlackMessageWarn({
                    title: 'Message from API Erp Connect: Không tồn tại username!',
                    data: signalGetAdmin,
                    payload: payload,
                    type: SLACK_TYPE.ERR,
                });
                return null;
            }

            const samePassword = await this.bcryptService.compare(
                password,
                signalGetAdmin.password,
            );

            if (!samePassword) {
                this.slack.sendSlackMessageWarn({
                    title: 'Message from API Erp Connect: Sai password!',
                    data: samePassword,
                    payload: payload,
                    type: SLACK_TYPE.ERR,
                });
                return null;
            }

            const _data = {
                _id: signalGetAdmin._id,
                username: signalGetAdmin.username,
            };

            return _data;
        } catch (error) {
            this.slack.sendSlackMessageWarn({
                title: `Message from API Erp Connect: Đăng nhập thành công!`,
                data: error,
                payload: payload,
                type: SLACK_TYPE.ERR,
            });
            return null;
        }
    }
}

export const ProviderLoginAdminUseCaseProxy = {
    inject: [ERPRepository, BcryptService, SlackService],
    provide: ADMIN_UC_PROXY_TYPES.LOGIN_ADMIN_USECASE_PROXY,
    useFactory: (
        _repository: ERPRepository,
        bcryptService: BcryptService,
        slack: SlackService,

    ) =>
        new UseCaseProxy(new LoginAdminUseCase(_repository, bcryptService, slack)),
};
