import { ProviderGetAdminUseCaseProxy } from './get-admin-proxy.service';
import { ProviderLoginAdminUseCaseProxy } from './login-admin-proxy.service';
import { ProviderRegisterAdminUseCaseProxy } from './register-admin-proxy.service';

export const ProviderErpConnect = [
  ProviderGetAdminUseCaseProxy,
  ProviderLoginAdminUseCaseProxy,
  ProviderRegisterAdminUseCaseProxy,
];
