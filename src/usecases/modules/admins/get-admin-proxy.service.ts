import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import { ADMIN_UC_PROXY_TYPES } from './admin-uc-proxy.type';
import { ErpAdminEntity } from '@/infrastructure/entities/erp.entity';
import { ERPRepository } from '@/infrastructure/repositories/erp.repository';

export class GetAdminUseCase {
  constructor(private readonly _repository: ERPRepository) {}

  async execute(payload: any): Promise<ErpAdminEntity> {
    try {
      const { username, _id } = payload;
      const signalGetAdmin = await this._repository.get({
        conditions: { username: username, _id },
        populates: [],
        select: '',
      });

      if (!signalGetAdmin) return null;

      return signalGetAdmin;
    } catch (error) {
      return null;
    }
  }
}

export const ProviderGetAdminUseCaseProxy = {
  inject: [ERPRepository],
  provide: ADMIN_UC_PROXY_TYPES.GET_ADMIN_USECASE_PROXY,
  useFactory: (_repository: ERPRepository) =>
    new UseCaseProxy(new GetAdminUseCase(_repository)),
};
