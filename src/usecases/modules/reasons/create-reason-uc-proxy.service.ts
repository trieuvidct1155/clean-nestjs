import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { AbsCreateReasonsUseCase } from '@/domain/usecases-proxy/modules/reasons/create-reason-uc-proxy.service';
import { ReasonEntity } from '@/infrastructure/entities/reason.entity';
import { ReasonRepository } from '@/infrastructure/repositories/reason.repository';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { UseCaseProxy } from '@infrastructure/usecases-proxy/usecases-proxy';
import { REASON_UC_PROXY_TYPES } from './reason-uc-proxy.type';

export class CreateReasonsUseCase extends AbsCreateReasonsUseCase {
  constructor(
    private readonly _repository: ReasonRepository,
    private readonly slack: SlackService,
  ) {
    super();
  }

  async execute(payloads: Array<Partial<ReasonEntity>>): Promise<void> {
    const signalCreate = await this._repository.createMany(payloads);
    const _slackType = !!signalCreate ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
    this.slack.sendSlackMessageWarn({
      title: `Message from API Reason: Create Reason!`,
      data: signalCreate,
      payload: payloads,
      type: _slackType,
    });
  }
}

export const ProviderCreateReasonsUseCaseProxy = {
  inject: [ReasonRepository, SlackService],
  provide: REASON_UC_PROXY_TYPES.CREATE_REASON_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ReasonRepository,
    slack: SlackService,
  ) => new UseCaseProxy(new CreateReasonsUseCase(_repository, slack)),
};
