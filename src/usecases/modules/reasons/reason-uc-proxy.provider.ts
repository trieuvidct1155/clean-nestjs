import { ProviderCreateReasonsUseCaseProxy } from './create-reason-uc-proxy.service';
import { ProviderUpdateReasonsUseCaseProxy } from './update-reason-uc-proxy.service';

export const ProviderReasons = [
  ProviderCreateReasonsUseCaseProxy,
  ProviderUpdateReasonsUseCaseProxy,
];
