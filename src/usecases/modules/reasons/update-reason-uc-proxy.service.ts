import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import {
  AbsUpdateReasonsUseCase,
  IPayloadUpdateReason,
} from '@/domain/usecases-proxy/modules/reasons/update-reason-uc-proxy.service';
import { ReasonEntity } from '@/infrastructure/entities/reason.entity';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { ReasonRepository } from '@infrastructure/repositories/reason.repository';
import { UseCaseProxy } from '@infrastructure/usecases-proxy/usecases-proxy';
import { REASON_UC_PROXY_TYPES } from './reason-uc-proxy.type';

export class UpdateReasonsUseCase extends AbsUpdateReasonsUseCase {
  constructor(
    private readonly _repository: ReasonRepository,
    private readonly slack: SlackService,
  ) {
    super();
  }

  async execute(payload: IPayloadUpdateReason): Promise<ReasonEntity> {
    const signalCreate = await this._repository.updateOne(payload);
    const _slackType = !!signalCreate ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
    this.slack.sendSlackMessageWarn({
      title: `Message from API Reason: Create Reason!`,
      data: signalCreate,
      payload: payload,
      type: _slackType,
    });

    return signalCreate;
  }
}

export const ProviderUpdateReasonsUseCaseProxy = {
  inject: [ReasonRepository, SlackService],
  provide: REASON_UC_PROXY_TYPES.UPDATE_REASON_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ReasonRepository,
    slack: SlackService,
  ) => new UseCaseProxy(new UpdateReasonsUseCase(_repository, slack)),
};
