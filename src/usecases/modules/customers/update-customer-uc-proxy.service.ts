import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { UpdateCustomerDTO } from '@/infrastructure/data-transfers/customer/update.dto';
import { AddressRepository } from '@/infrastructure/repositories/address.repository';
import { CustomerRepository } from '@/infrastructure/repositories/customer.repository';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { CUSTOMER_UC_PROXY_TYPES } from './customer-uc-proxy.type';

export class UpdateCustomersUseCase {
  constructor(
    private readonly _repository: CustomerRepository,
    private readonly _addressRepository: AddressRepository,
    private readonly slack: SlackService,
  ) {}

  private async updateCustomer(id_customer, dataPayload) {
    const getDataCustomer = await this._repository.get({
      conditions: { erpCode: id_customer },
    });
    const id_address = getDataCustomer?.listAddress?.default || null;

    const dataUpdateAddress = {
      address: dataPayload.address,
      name: dataPayload.name,
      phone: dataPayload.phone,
      email: dataPayload.email,
      erpDistrict: dataPayload.name_district,
      erpWard: dataPayload.name_ward,
      erpProvince: dataPayload.name_province,
    };
    await this._addressRepository.updateAddressId(
      id_address,
      dataUpdateAddress,
    );

    const dataUpdate = {
      name: dataPayload.name,
      phone: dataPayload.phone,
      address: dataPayload.address,
      email: dataPayload.email,
      erpCode: id_customer,
    };

    return this._repository.createOrUpdate({
      conditions: { erpCode: id_customer },
      dataUpdate: dataUpdate,
      options: { upsert: true, new: true },
    });
  }

  async execute(payloads: UpdateCustomerDTO): Promise<boolean> {
    try {
      const { id_customer, ...dataPayload } = payloads;

      const signalCustomer = await this._repository.existIdCustomer(
        id_customer,
      );
      if (!signalCustomer) {
        this.slack.sendSlackMessageWarn({
          data: signalCustomer,
          payload: payloads,
          title: 'Message from API Customer: Khách hàng không tồn tại!',
          type: 'err',
        });
        return !signalCustomer;
      }
      const signalCreate = await this.updateCustomer(id_customer, dataPayload);

      const _slackType = !!signalCreate ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Customer: Update customer!`,
        data: signalCreate,
        payload: payloads,
        type: _slackType,
      });

      return !!signalCreate;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: `Message from API Customer: Update customer!`,
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      throw error;
    }
  }
}

export const ProviderUpdateCustomersUseCaseProxy = {
  inject: [CustomerRepository, AddressRepository, SlackService],
  provide: CUSTOMER_UC_PROXY_TYPES.UPDATE_CUSTOMER_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: CustomerRepository,
    _addressRepository: AddressRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new UpdateCustomersUseCase(_repository, _addressRepository, slack),
    ),
};
