import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { CreateCustomerDTO } from '@data-transfers/customer/create.dto';
import { AddressRepository } from '@repositories/address.repository';
import { CustomerRepository } from '@repositories/customer.repository';
import { SlackService } from '@services/slack/slack.service';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import { CUSTOMER_UC_PROXY_TYPES } from './customer-uc-proxy.type';

export class CreateCustomersUseCase {
  constructor(
    private readonly _repository: CustomerRepository,
    private readonly _addressRepository: AddressRepository,
    private readonly slack: SlackService,
  ) {}

  private async createCustomer(id_customer, dataPayload) {
    const dataUpdateAddressCustomer =
      await this._addressRepository.getDataListAddress(dataPayload);

    const dataUpdate = {
      name: dataPayload.name,
      phone: dataPayload.phone,
      address: dataPayload.address,
      email: dataPayload.email,
      erpCode: id_customer,
      listAddress: dataUpdateAddressCustomer,
    };

    return this._repository.createOrUpdate({
      conditions: { erpCode: id_customer },
      dataUpdate: dataUpdate,
      options: { upsert: true, new: true },
    });
  }

  async execute(payloads: CreateCustomerDTO): Promise<boolean> {
    try {
      const { id_customer, ...dataPayload } = payloads;

      const signalCustomer = await this._repository.existIdCustomer(
        id_customer,
      );
      if (signalCustomer) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Customer: Khách hàng đã tồn tại!',
          data: signalCustomer,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return signalCustomer;
      }

      const signalCreateCustomer = await this.createCustomer(
        id_customer,
        dataPayload,
      );

      const _slackType = !!signalCreateCustomer
        ? SLACK_TYPE.MESS
        : SLACK_TYPE.ERR;

      this.slack.sendSlackMessageWarn({
        title: `Message from API Customer: New customer!`,
        data: signalCreateCustomer,
        payload: payloads,
        type: _slackType,
      });

      return !!signalCreateCustomer;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: `Message from API Customer: Create customer!`,
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderCreateCustomersUseCaseProxy = {
  inject: [CustomerRepository, AddressRepository, SlackService],
  provide: CUSTOMER_UC_PROXY_TYPES.CREATE_CUSTOMER_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: CustomerRepository,
    _addressRepository: AddressRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new CreateCustomersUseCase(_repository, _addressRepository, slack),
    ),
};
