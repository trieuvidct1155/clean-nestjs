import { ProviderCreateCustomersUseCaseProxy } from './create-customer-uc-proxy.service';
import { ProviderUpdateCustomersUseCaseProxy } from './update-customer-uc-proxy.service';

export const ProviderCustomer = [
  ProviderCreateCustomersUseCaseProxy,
  ProviderUpdateCustomersUseCaseProxy,
];
