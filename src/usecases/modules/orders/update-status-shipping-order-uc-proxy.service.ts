import { UseCaseProxy } from '@infrastructure/usecases-proxy/usecases-proxy';
import { ORDER_UC_PROXY_TYPES } from './order-uc-proxy.type';
import { OrderRepository } from '@/infrastructure/repositories/order.repository';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';

export class UpdateStatusShippingOrdersUseCase {
  constructor(
    private readonly _repository: OrderRepository,
    private readonly slack: SlackService,
  ) {}

  async execute(payloads: any): Promise<boolean> {
    try {
      const { id_order, status } = payloads;
      const signalUpdate = await this._repository.updateOne({
        conditions: { erpCode: id_order },
        dataUpdate: {
          erpCode: id_order,
          shippingStatus: status,
        },
      });

      const _slackType = !!signalUpdate ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;

      this.slack.sendSlackMessageWarn({
        title: `Message from API Update Status Shipping Order: Update Status Shipping Order!`,
        data: signalUpdate,
        payload: payloads,
        type: _slackType,
      });

      return !!signalUpdate;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: `Message from API Update Status Shipping Order: Update Status Shipping Order Fail!`,
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderUpdateStatusShippingOrdersUseCaseProxy = {
  inject: [OrderRepository, SlackService],
  provide: ORDER_UC_PROXY_TYPES.UPDATE_ORDER_STATUS_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: OrderRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(new UpdateStatusShippingOrdersUseCase(_repository, slack)),
};
