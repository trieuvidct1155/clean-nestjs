import { ProviderCancelOrdersUseCaseProxy } from './cancel-order-uc-proxy.service';
import { ProviderCreateOrdersUseCaseProxy } from './create-order-uc-proxy.service';
import { ProviderUpdateStatusShippingOrdersUseCaseProxy } from './update-status-shipping-order-uc-proxy.service';

export const ProviderOrders = [
  ProviderUpdateStatusShippingOrdersUseCaseProxy,
  ProviderCreateOrdersUseCaseProxy,
  ProviderCancelOrdersUseCaseProxy,
];
