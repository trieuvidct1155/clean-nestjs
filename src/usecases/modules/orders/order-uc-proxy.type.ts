export enum ORDER_UC_PROXY_TYPES {
  CREATE_ORDER_UC_PROXY = 'CREATE_ORDER_UC_PROXY',
  UPDATE_ORDER_UC_PROXY = 'UPDATE_ORDER_UC_PROXY',
  GET_LIST_ORDER_UC_PROXY = 'GET_LIST_ORDER_UC_PROXY',
  DELETE_ORDER_UC_PROXY = 'DELETE_ORDER_UC_PROXY',
  UPDATE_ORDER_STATUS_UC_PROXY = 'UPDATE_ORDER_STATUS_UC_PROXY',
  CANCEL_ORDER_UC_PROXY = 'CANCEL_ORDER_UC_PROXY',
}
