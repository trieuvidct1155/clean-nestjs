import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { ORDER_UC_PROXY_TYPES } from './order-uc-proxy.type';
import { OrderRepository } from '@/infrastructure/repositories/order.repository';
import { OrderItemRepository } from '@/infrastructure/repositories/orderItem.repository';
import mongoose from 'mongoose';
import { ProductRepository } from '@/infrastructure/repositories/product.repository';
import { PromotionRepository } from '@/infrastructure/repositories/promotion.repository';
import { CustomerRepository } from '@/infrastructure/repositories/customer.repository';
import { SlugKenZy } from '@/infrastructure/helpers/slug-kenzy';
import { DATE_FORMAT_FN } from '@/infrastructure/helpers/date-time-format';
import { StoreLocationRepository } from '@/infrastructure/repositories/storeLocation.repository';
import { LoggerRepository } from '@/infrastructure/repositories/logger.repostitory';
import { CONTACTPAY } from '@/infrastructure/entities/order.entity';
import { statusOrder } from '@/infrastructure/constants/base.constants';
import { CreateOrderEntityDTO } from '@/infrastructure/data-transfers/order/create-entitty.dto';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';

export class CreateOrdersUseCase {
  constructor(
    private readonly _repository: OrderRepository,
    private readonly _infoRepository: OrderItemRepository,
    private readonly _productRepository: ProductRepository,
    private readonly _promotionRepository: PromotionRepository,
    private readonly _customerRepository: CustomerRepository,
    private readonly _storeLocationRepository: StoreLocationRepository,
    private readonly _loggerRepository: LoggerRepository,
    private readonly slack: SlackService,
  ) {}

  async execute(payloads: CreateOrderEntityDTO): Promise<boolean> {
    try {
      const {
        id_order,
        id_customer,
        order_items,
        totalAmount,
        totalAmountTemporary = 0,
        id_promotion,
        contactPay,
        creator,
        staffName,
        id_store,
      } = payloads;

      const id = new mongoose.Types.ObjectId();

      const signalCustomer = await this._customerRepository.existIdCustomer(
        id_customer,
      );
      if (!signalCustomer) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Create Order: Khách hàng không tồn tại!',
          data: signalCustomer,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
      }

      const _tmpsignalGet = await this._repository.get({
        conditions: { erpCode: id_order },
      });
      if (!!_tmpsignalGet) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Create Order: id_order đã tồn tại!',
          data: _tmpsignalGet,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
      }

      let promotionInfo = null;
      let promotion = id_promotion === '' ? null : id_promotion;
      if (promotion && promotion != 'undefined') {
        const promotionInfo = await this._promotionRepository.getDataPromotion(
          promotion,
        );
        promotion = promotionInfo?._id;
      }

      let signalProducts = await Promise.all(
        order_items.map((e) =>
          this._productRepository.get({
            conditions: { erpCode: e.product_code },
            populates: [],
            select: 'category quantities',
          }),
        ),
      );
      let isOrderSuccess = 0; // check quantities ERP neu lon hon quantities Ec thi khong cho cap nhat lai quantities va staus = 0
      let orderItemsID = [];
      for (let i = 0; i < order_items.length; i++) {
        if (
          order_items?.[i]?.quantities > (signalProducts[i]?.quantities || 0)
        ) {
          isOrderSuccess = 1;
        }
        const payload = {
          signalProducts: signalProducts?.[i] || {},
          order_items: order_items?.[i] || {},
          id,
          id_order,
        };
        const createNewItem = await this._infoRepository.createOrderItem(
          payload,
        );
        orderItemsID.push(createNewItem?._id);
      }

      if (isOrderSuccess == 0) {
        const condition = {
          _id: { $in: orderItemsID },
        };
        const signalGetOrderItems =
          await this._infoRepository.getDataListOrderItem(condition);
        // quantities khong du so luong de cap nhat lai quantity
        await this._productRepository.updateQuantityProductOrderSuccess(
          signalGetOrderItems,
        );
      }

      const signalNewCustomer =
        await this._customerRepository.getDataCustomerWithErpCode(id_customer);

      const today = new Date();
      const randomString = (Math.random() + 1).toString(36).substring(7);
      let orderID = `${SlugKenZy.createShortenName(
        signalNewCustomer?.name,
      )}-${DATE_FORMAT_FN(today, 'YYMMDDHH')}-${randomString}`;
      orderID = orderID.toUpperCase();

      let totalAmountPromotion = 0;
      if (promotionInfo) {
        totalAmountPromotion =
          promotionInfo.type == 1
            ? parseInt(promotionInfo.price)
            : (totalAmount / 100) * parseInt(promotionInfo.price);
      }

      const loggerId = new mongoose.Types.ObjectId();
      const histories = loggerId;
      const signalStoreLocation = await this._storeLocationRepository.get({
        conditions: { erpCode: id_store },
      });
      const storeLocationID = signalStoreLocation?._id || '';

      const _infoCustomer: any = {};
      _infoCustomer.location = signalNewCustomer?.location || {};
      _infoCustomer.name = signalNewCustomer?.name || '';
      _infoCustomer.email = signalNewCustomer?.email || '';
      _infoCustomer.customer = signalNewCustomer?.customer || null;
      _infoCustomer.customerPhone = signalNewCustomer?.customerPhone || '';

      const dataUpdate = {
        _id: id,
        note: '',
        contactPay: contactPay == '0' ? CONTACTPAY.CASH : CONTACTPAY.ONLINE,
        totalAmountPromotion,
        orderItems: orderItemsID,
        infoCustomer: _infoCustomer,
        totalAmountTemporary,
        totalAmount,
        orderID,
        deliveryCharges: 0,
        promotion: promotion || null,
        fromSystemCreate: 1,
        status: isOrderSuccess == 0 ? 3 : 0, // quantity khong du so luong nen de status 0
        shippingStatus: isOrderSuccess == 0 ? 3 : 0, // quantity khong du so luong nen de status 0
        processUser: creator || null,
        erpCode: id_order,
        staffName,
        histories,
        storeLocation: storeLocationID || null,
      };
      const signalNewOrder = await this._repository.createOrUpdate({
        conditions: { _id: id },
        dataUpdate: dataUpdate,
        options: { upsert: true, new: true },
      });
      const order = signalNewOrder;
      const dataCreateLogger = {
        status: statusOrder[order?.status.toString()] || null,
        noteAdmin: order?.noteAdmin || '',
        optionNote: null,
        storeLocation: order?.storeLocation || null,
      };
      await this._loggerRepository.createLogger({
        _id: loggerId,
        user: signalNewCustomer?.customer || null,
        type: 'order_update',
        actions: dataCreateLogger,
      });

      const _slackType = !!signalNewOrder ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;

      this.slack.sendSlackMessageWarn({
        title: `Message from API Create Order: New Order!`,
        data: signalNewOrder,
        payload: payloads,
        type: _slackType,
      });

      return !!signalNewOrder;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: `Message from API Create Order: New Order Fail!`,
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderCreateOrdersUseCaseProxy = {
  inject: [
    OrderRepository,
    OrderItemRepository,
    ProductRepository,
    PromotionRepository,
    CustomerRepository,
    StoreLocationRepository,
    LoggerRepository,
    SlackService,
  ],
  provide: ORDER_UC_PROXY_TYPES.CREATE_ORDER_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: OrderRepository,
    _infoRepository: OrderItemRepository,
    _productRepository: ProductRepository,
    _promotionRepository: PromotionRepository,
    _customerRepository: CustomerRepository,
    _storeLocationRepository: StoreLocationRepository,
    _loggerRepository: LoggerRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new CreateOrdersUseCase(
        _repository,
        _infoRepository,
        _productRepository,
        _promotionRepository,
        _customerRepository,
        _storeLocationRepository,
        _loggerRepository,
        slack,
      ),
    ),
};
