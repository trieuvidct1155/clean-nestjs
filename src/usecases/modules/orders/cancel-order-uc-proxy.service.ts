import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { ORDER_UC_PROXY_TYPES } from './order-uc-proxy.type';
import { OrderRepository } from '@/infrastructure/repositories/order.repository';
import { OrderItemRepository } from '@/infrastructure/repositories/orderItem.repository';
import mongoose from 'mongoose';
import { ProductRepository } from '@/infrastructure/repositories/product.repository';
import { CustomerRepository } from '@/infrastructure/repositories/customer.repository';
import { StoreLocationRepository } from '@/infrastructure/repositories/storeLocation.repository';
import { LoggerRepository } from '@/infrastructure/repositories/logger.repostitory';
import { statusOrder } from '@/infrastructure/constants/base.constants';
import { ReasonRepository } from '@/infrastructure/repositories/reason.repository';
import { CancelOrderEntityDTO } from '@/infrastructure/data-transfers/order/cancel.dto';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';

export class CancelOrdersUseCase {
  constructor(
    private readonly _repository: OrderRepository,
    private readonly _infoRepository: OrderItemRepository,
    private readonly _productRepository: ProductRepository,
    private readonly _customerRepository: CustomerRepository,
    private readonly _storeLocationRepository: StoreLocationRepository,
    private readonly _loggerRepository: LoggerRepository,
    private readonly _reasonRepository: ReasonRepository,
    private readonly slack: SlackService,
  ) {}

  async execute(payloads: CancelOrderEntityDTO): Promise<boolean> {
    try {
      const { id_order, id_customer, id_reason, id_store } = payloads;

      const signalGet = await this._repository.get({
        conditions: { erpCode: id_order },
      });
      if (!!!signalGet) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Cancel Order: id_order không tồn tại!',
          data: signalGet,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
      }
      const id = signalGet?._id || null;
      let dataUpdate: any = {};

      const _signalGetCustomer = await this._customerRepository.get({
        conditions: { erpCode: id_customer },
      });
      if (!!!_signalGetCustomer) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Cancel Order: Khách hàng không tồn tại!',
          data: _signalGetCustomer,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
      }

      dataUpdate.processUser = _signalGetCustomer?._id || null;
      dataUpdate.status = -2;
      dataUpdate.shippingStatus = -2;
      dataUpdate.cancelDate = new Date();

      if (id_reason) {
        const _signalGetReason = await this._reasonRepository.get({
          conditions: { erpCode: id_reason },
        });
        dataUpdate.reason = _signalGetReason?._id || null;
      }

      if (id_store && id_store != '') {
        const _signalGetStoreLocation = await this._storeLocationRepository.get(
          {
            conditions: { erpCode: id_store },
          },
        );
        dataUpdate.storeLocation = _signalGetStoreLocation?._id || null;
      }

      const loggerId = new mongoose.Types.ObjectId();
      const signalGetOrder = await this._repository.get({
        conditions: { _id: id },
      });
      const _order = signalGetOrder;

      if (_order?.status && _order.status.toString() == '-2') {
        return false;
        // return console.log("The order has been canceled, cannot be processed!")
      }
      dataUpdate.histories = _order?.histories
        ? [..._order.histories, loggerId]
        : loggerId;

      const condition = {
        _id: { $in: _order?.orderItems },
      };
      const signalGetOrderItems =
        await this._infoRepository.getDataListOrderItem(condition);
      // quantities khong du so luong de cap nhat lai quantity
      await this._productRepository.updateQuantityProductOrderCancel(
        signalGetOrderItems,
      );
      const signalUpdate = await this._repository.updateOne({
        conditions: { _id: id },
        dataUpdate: dataUpdate,
      });
      const order = signalUpdate;
      const signalGetReason = await this._reasonRepository.get({
        conditions: { _id: dataUpdate?.reason },
      });
      const _reason = signalGetReason;
      const dataCreateLogger = {
        status: statusOrder[dataUpdate?.status?.toString()] || '',
        noteAdmin: order?.noteAdmin || '',
        optionNote: _reason?.name || null,
        storeLocation: order?.storeLocation || null,
      };
      await this._loggerRepository.createLogger({
        _id: loggerId,
        user: _signalGetCustomer?._id || null,
        type: 'order_update',
        actions: dataCreateLogger,
      });

      const _slackType = !!order ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;

      this.slack.sendSlackMessageWarn({
        title: `Message from API Cancel Order: Cancel Order!`,
        data: order,
        payload: payloads,
        type: _slackType,
      });

      return !!order;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: `Message from API Cancel Order: Cancel Order Fail!`,
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderCancelOrdersUseCaseProxy = {
  inject: [
    OrderRepository,
    OrderItemRepository,
    ProductRepository,
    CustomerRepository,
    StoreLocationRepository,
    LoggerRepository,
    ReasonRepository,
    SlackService,
  ],
  provide: ORDER_UC_PROXY_TYPES.CANCEL_ORDER_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: OrderRepository,
    _infoRepository: OrderItemRepository,
    _productRepository: ProductRepository,
    _customerRepository: CustomerRepository,
    _storeLocationRepository: StoreLocationRepository,
    _loggerRepository: LoggerRepository,
    _reasonRepository: ReasonRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new CancelOrdersUseCase(
        _repository,
        _infoRepository,
        _productRepository,
        _customerRepository,
        _storeLocationRepository,
        _loggerRepository,
        _reasonRepository,
        slack,
      ),
    ),
};
