export enum ORIGIN_UC_PROXY_TYPES {
  CREATE_ORIGIN_UC_PROXY = 'CREATE_ORIGIN_UC_PROXY',
  UPDATE_ORIGIN_UC_PROXY = 'UPDATE_ORIGIN_UC_PROXY',
  GET_LIST_ORIGIN_UC_PROXY = 'GET_LIST_ORIGIN_UC_PROXY',
}
