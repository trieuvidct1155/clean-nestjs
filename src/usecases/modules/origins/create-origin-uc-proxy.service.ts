import { CountryOriginRepository } from '@/infrastructure/repositories/countryOrigin.repository';
import { CountryOriginInfoRepository } from '@/infrastructure/repositories/countryOriginInfo.repository';
import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { ORIGIN_UC_PROXY_TYPES } from './origin-uc-proxy.type';
import mongoose from 'mongoose';
import { SlugKenZy } from '@/infrastructure/helpers/slug-kenzy';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { CreateCountryOriginEntityDTO } from '@/infrastructure/data-transfers/countryOrigin/create-entitty.dto';

export class CreateOriginsUseCase {
  constructor(
    private readonly _repository: CountryOriginRepository,
    private readonly _infoRepository: CountryOriginInfoRepository,
    private readonly slack: SlackService,
  ) {}

  private async existsSlug(slug: string) {
    const signalGet = await this._infoRepository.get({
      conditions: { $or: [{ slug: slug }, { slug: slug }] },
      populates: [],
      select: '',
    });

    return !!signalGet;
  }

  async existErpCode(erpCode: string): Promise<boolean> {
    const signalGet = await this._repository.get({
      conditions: { erpCode: erpCode },
    });

    return !!signalGet;
  }

  private cloneInfoWithLang(info: any, lang): any {
    if (!info) return null;
    const clone = { ...info };

    clone['lang'] = lang;
    clone['_id'] = new mongoose.Types.ObjectId();
    clone['slug'] = info['slug'] + '-' + lang;
    clone['name'] = info['name'] + '-' + lang;

    return clone;
  }

  private async createCategoriesInfo({ name, slug, lang }) {
    const _idInfo = new mongoose.Types.ObjectId();
    const _id = new mongoose.Types.ObjectId();
    const status = 2;
    const payloadVi = {
      _id: _idInfo,
      name: name,
      slug: slug,
      lang: lang,
      status,
      countryOrigin: _id,
    };

    const payloadEn = this.cloneInfoWithLang(payloadVi, 'en');

    //create category Info
    const signalUpdateInfo = await this._infoRepository.createMany([
      payloadVi,
      payloadEn,
    ]);
    const [vi, en] = signalUpdateInfo;
    return { vi, en, _id };
  }

  private async createOrigin({ _id, erpCode, vi, en }) {
    const dataUpdate = {
      _id,
      erpCode,
      vi: vi._id,
      en: en._id,
      viName: vi.name,
      enName: en.name,
      viSlug: vi.slug,
      enSlug: en.slug,
    };

    const signalUpdate = await this._repository.createOrUpdate({
      conditions: { _id: _id },
      dataUpdate: { $set: dataUpdate },
      options: { upsert: true, new: true },
    });

    return signalUpdate;
  }

  async execute(payloads: CreateCountryOriginEntityDTO): Promise<boolean> {
    try {
      const { erpCode, name, lang } = payloads;

      const slug = SlugKenZy.createURL(name);

      const existsSlug = await this.existsSlug(slug);
      if (existsSlug) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Create Country Origin: Đã tồn tại slug!',
          data: existsSlug,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
        // log and slack
      }

      const existsErpCode = await this.existErpCode(erpCode);
      if (existsErpCode) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Create Country Origin: Đã tồn tại erpCode!',
          data: existsErpCode,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
        // log and slack
      }

      const { vi, en, _id } = await this.createCategoriesInfo({
        name,
        slug,
        lang,
      });

      const origin = await this.createOrigin({
        _id,
        erpCode,
        vi,
        en,
      });

      const _slackType = !!origin ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Create Country Origin: Create Country Origin!`,
        data: origin,
        payload: payloads,
        type: _slackType,
      });

      return !!origin;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title:
          'Message from API Create Country Origin: Create Country Origin Fail!',
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderCreateOriginsUseCaseProxy = {
  inject: [CountryOriginRepository, CountryOriginInfoRepository, SlackService],
  provide: ORIGIN_UC_PROXY_TYPES.CREATE_ORIGIN_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: CountryOriginRepository,
    _infoRepository: CountryOriginInfoRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new CreateOriginsUseCase(_repository, _infoRepository, slack),
    ),
};
