import { CountryOriginRepository } from '@/infrastructure/repositories/countryOrigin.repository';
import { CountryOriginInfoRepository } from '@/infrastructure/repositories/countryOriginInfo.repository';
import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { ORIGIN_UC_PROXY_TYPES } from './origin-uc-proxy.type';
import { ERPcreateSlugFromTitle } from '@/infrastructure/helpers/common/index';
import { CountryOriginEntity } from '@/infrastructure/entities/countryOrigin.entity';
import { ELang } from '@/domain/decorators/lang.decorator';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { UpdateCountryOriginEntityDTO } from '@/infrastructure/data-transfers/countryOrigin/update-entitty.dto';

export class UpdateOriginsUseCase {
  constructor(
    private readonly _repository: CountryOriginRepository,
    private readonly _infoRepository: CountryOriginInfoRepository,
    private readonly slack: SlackService,
  ) {}

  private async getOrigin(erpCode: string): Promise<CountryOriginEntity> {
    const signalGet = await this._repository.get({
      conditions: { erpCode: erpCode },
      populates: [],
    });
    return signalGet;
  }

  private dataUpdateByLang({ lang, name, slug }) {
    const dataUpdate = {};
    if (lang === ELang.VI) {
      dataUpdate['viName'] = name;
      dataUpdate['viSlug'] = slug;
    } else if (lang === ELang.EN) {
      dataUpdate['enName'] = name;
      dataUpdate['enSlug'] = slug;
    }

    return dataUpdate;
  }

  async execute(payloads: UpdateCountryOriginEntityDTO): Promise<boolean> {
    try {
      const { erpCode, name, lang } = payloads;

      const slug = ERPcreateSlugFromTitle(name);

      const signalGet = await this.getOrigin(erpCode);
      if (!signalGet) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Update Country Origin: Không tồn tại!',
          data: signalGet,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return !!signalGet;
      }

      const id = signalGet?._id || null;

      const signalUpdateInfo = await this._infoRepository.createOrUpdate({
        conditions: { countryOrigin: id, lang },
        dataUpdate: {
          name: name,
          slug: slug,
          lang: lang,
        },
        options: { upsert: true, new: true },
      });

      const signalUpdate = await this._repository.createOrUpdate({
        conditions: { _id: id },
        dataUpdate: {
          ...this.dataUpdateByLang({
            lang,
            name: signalUpdateInfo.name,
            slug: signalUpdateInfo.slug,
          }),
        },
        options: { upsert: true, new: true },
      });

      const _slackType = !!signalUpdate ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Update Country Origin: Update Country Origin!`,
        data: signalUpdate,
        payload: payloads,
        type: _slackType,
      });

      return !!signalUpdate;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title:
          'Message from API Update Country Origin: Update Country Origin Fail!',
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderUpdateOriginsUseCaseProxy = {
  inject: [CountryOriginRepository, CountryOriginInfoRepository, SlackService],
  provide: ORIGIN_UC_PROXY_TYPES.UPDATE_ORIGIN_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: CountryOriginRepository,
    _infoRepository: CountryOriginInfoRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new UpdateOriginsUseCase(_repository, _infoRepository, slack),
    ),
};
