import { ProviderCreateOriginsUseCaseProxy } from './create-origin-uc-proxy.service';
import { ProviderUpdateOriginsUseCaseProxy } from './update-origin-uc-proxy.service';

export const ProviderCountryOrigin = [
  ProviderCreateOriginsUseCaseProxy,
  ProviderUpdateOriginsUseCaseProxy,
];
