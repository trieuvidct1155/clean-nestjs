import { ReturnData } from '@data-transfers/return-data/return-data.present';
import { CreateImagesDTO } from '@data-transfers/image/create-image.dto';
import { ImageRepository } from '@repositories/image.repository';
import { IMAGE_UC_PROXY_TYPES } from './image-uc-proxy.type';
import { ImageEntity } from '@entities/image.entity';
import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';

export class CreateImagesUseCase {
  constructor(private readonly _repository: ImageRepository) {}

  async execute(
    payloads: Array<CreateImagesDTO>,
  ): Promise<ReturnData<ImageEntity[]>> {
    const signalCreate = await this._repository.createMany(payloads);
    const msg = signalCreate ? 'Create images success' : 'Create images fail';
    const isError = !signalCreate;
    return {
      error: isError,
      data: signalCreate,
      message: msg,
    };
  }
}

export const ProviderCreateImagesUseCaseProxy = {
  inject: [ImageRepository],
  provide: IMAGE_UC_PROXY_TYPES.CREATE_IMAGE_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ImageRepository,
  ) => new UseCaseProxy(new CreateImagesUseCase(_repository)),
};
