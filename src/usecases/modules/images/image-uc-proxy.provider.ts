import { ProviderCreateImagesUseCaseProxy } from './create-image-uc-proxy.service';
import { ProviderDeleteImagesUseCaseProxy } from './delete-image-uc-proxy.service';
import { ProviderGetListImagesUseCaseProxy } from './get-list-image-uc-proxy.service';

export const ProviderImages = [
  ProviderCreateImagesUseCaseProxy,
  ProviderGetListImagesUseCaseProxy,
  ProviderDeleteImagesUseCaseProxy,
];
