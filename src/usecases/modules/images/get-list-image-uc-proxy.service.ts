import { ImageRepository } from '@repositories/image.repository';
import { IMAGE_UC_PROXY_TYPES } from './image-uc-proxy.type';
import { ImageEntity } from '@entities/image.entity';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import {
  AbsGetListImagesUseCase,
  IPayloadGetListImage,
} from '@/domain/usecases-proxy/modules/images/get-list-image-uc-proxy.service';

export class GetListImagesUseCase extends AbsGetListImagesUseCase {
  constructor(private readonly _repository: ImageRepository) {
    super();
  }

  async execute(payload: IPayloadGetListImage): Promise<ImageEntity[]> {
    const { search, paging } = payload;
    const signalCreate = await this._repository.getList({ search, ...paging });
    return signalCreate;
  }
}

export const ProviderGetListImagesUseCaseProxy = {
  inject: [ImageRepository],
  provide: IMAGE_UC_PROXY_TYPES.GET_LIST_IMAGE_UC_PROXY,
  useFactory: (_repository: ImageRepository) =>
    new UseCaseProxy(new GetListImagesUseCase(_repository)),
};
