import { ImageRepository } from '@repositories/image.repository';
import { IMAGE_UC_PROXY_TYPES } from './image-uc-proxy.type';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import {
  AbsDeleteImagesUseCase,
  IPayloadDeleteImage,
} from '@/domain/usecases-proxy/modules/images/delete-image-uc-proxy.service';

export class DeleteImagesUseCase extends AbsDeleteImagesUseCase {
  constructor(private readonly _repository: ImageRepository) {
    super();
  }

  async execute(payload: IPayloadDeleteImage): Promise<boolean> {
    const { ids } = payload;
    const signalCreate = await this._repository.removeMany({
      conditions: { _id: { $in: ids } },
    });
    return signalCreate;
  }
}

export const ProviderDeleteImagesUseCaseProxy = {
  inject: [ImageRepository],
  provide: IMAGE_UC_PROXY_TYPES.DELETE_IMAGE_UC_PROXY,
  useFactory: (_repository: ImageRepository) =>
    new UseCaseProxy(new DeleteImagesUseCase(_repository)),
};
