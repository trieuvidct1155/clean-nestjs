import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { ProductRepository } from '@/infrastructure/repositories/product.repository';
import { ProductInfoRepository } from '@/infrastructure/repositories/productInfo.repository';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { PRODUCT_UC_PROXY_TYPES } from './product-uc-proxy.type';

export class DeleteProductsUseCase {
  constructor(
    private readonly _repository: ProductRepository,
    private readonly _infoRepostitory: ProductInfoRepository,
    private readonly slack: SlackService,
  ) {}

  async existErpCode(item_code: string): Promise<any> {
    return this._repository.get({
      conditions: { erpCode: item_code },
      populates: [],
      select: '',
    });
  }

  async execute(payloads: any): Promise<boolean> {
    try {
      const { item_code } = payloads;
      const _status = -2;
      const signalGet = await this.existErpCode(item_code);
      if (!!!signalGet) {
        // log and slack
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Delete Product: Không tồn tại item_code!',
          data: signalGet,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return !!signalGet;
      }

      const signalDelete = await this._repository.updateOne({
        conditions: { _id: signalGet?._id },
        dataUpdate: {
          viStatus: _status,
          enStatus: _status,
        },
      });

      await this._infoRepostitory.updateOne({
        conditions: { product: signalGet?._id },
        dataUpdate: {
          status: _status,
        },
      });

      const _slackType = !!signalDelete ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Delete Product: Delete Product!`,
        data: signalDelete,
        payload: payloads,
        type: _slackType,
      });

      return !!signalDelete;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: 'Message from API Delete Product: Delete Product Fail!',
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderDeleteProductsUseCaseProxy = {
  inject: [ProductRepository, ProductInfoRepository, SlackService],
  provide: PRODUCT_UC_PROXY_TYPES.DELETE_PRODUCT_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ProductRepository,
    _infoRepostitory: ProductInfoRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new DeleteProductsUseCase(_repository, _infoRepostitory, slack),
    ),
};
