import { ProviderCreateProductsUseCaseProxy } from './create-product-uc-proxy.service';
import { ProviderDeleteProductsUseCaseProxy } from './delete-product-proxy.service';
import { ProviderUpdateProductsUseCaseProxy } from './update-product-uc-proxy.service';

export const ProviderProducts = [
  ProviderCreateProductsUseCaseProxy,
  ProviderUpdateProductsUseCaseProxy,
  ProviderDeleteProductsUseCaseProxy,
];
