import { IpayloadUpdateProduct } from '@/domain/repositories/product.interface';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { UpdateProductEntityDTO } from '@/infrastructure/data-transfers/product/update-entity.dto';
import { ProductRepository } from '@/infrastructure/repositories/product.repository';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { UseCaseProxy } from '../../../infrastructure/usecases-proxy/usecases-proxy';
import { PRODUCT_UC_PROXY_TYPES } from './product-uc-proxy.type';

export class UpdateProductsUseCase {
  constructor(
    private readonly _repository: ProductRepository,
    private readonly slack: SlackService,
  ) {}

  async existErpCode(item_code: string): Promise<boolean> {
    const signalGet = await this._repository.get({
      conditions: { erpCode: item_code },
    });

    return !!signalGet;
  }

  async execute(payloads: UpdateProductEntityDTO): Promise<any> {
    try {
      const {
        item_code,
        barCode,
        quantities_change = 0,
        sku,
        product_date,
        price,
        weight,
        abv,
        volume,
      } = payloads;

      const dataUpdate: IpayloadUpdateProduct = {};
      barCode && (dataUpdate.barCode = barCode);
      sku && (dataUpdate.sku = sku);
      product_date && (dataUpdate.productDate = product_date);
      price && (dataUpdate.viPrice = price);
      weight && (dataUpdate.weight = weight);
      abv && (dataUpdate.abv = abv);
      volume && (dataUpdate.volume = volume);

      const existsErpCode = await this.existErpCode(item_code);
      if (!existsErpCode) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Create Product: Không tồn tại item_code!',
          data: existsErpCode,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
        // log and slack
      }

      const signalUpdate = await this._repository.updateOneV2({
        conditions: { erpCode: item_code },
        dataUpdate: {
          ...dataUpdate,
          modifiedAt: new Date(),
          $inc: { quantities: +quantities_change },
        },
      });
      const _slackType = !!signalUpdate ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Product: Create Product!`,
        data: signalUpdate,
        payload: payloads,
        type: _slackType,
      });

      return !!signalUpdate;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: 'Message from API Product: Create Product Fail!',
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderUpdateProductsUseCaseProxy = {
  inject: [ProductRepository, SlackService],
  provide: PRODUCT_UC_PROXY_TYPES.UPDATE_PRODUCT_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ProductRepository,
    slack: SlackService,
  ) => new UseCaseProxy(new UpdateProductsUseCase(_repository, slack)),
};
