import { CountryOriginRepository } from '@repositories/countryOrigin.repository';
import { ProductRepository } from '@repositories/product.repository';
import { ProductBrandRepository } from '@repositories/productBrand.repository';
import { ProductCategoryRepository } from '@repositories/productCategory.repository';
import { ProductInfoRepository } from '@repositories/productInfo.repository';
import { ProductVariantRepository } from '@repositories/productVariant.repository';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import { PRODUCT_UC_PROXY_TYPES } from './product-uc-proxy.type';
import mongoose from 'mongoose';
import { IPayloadCreateProductVariant } from '@/domain/repositories/product-variant.interface';
import { IPayloadCreateProduct } from '@/domain/repositories/product.interface';
import {
  LANGUAGES,
  LANGUAGES_REVERSE,
} from '@/infrastructure/constants/base.constants';
import { SlugKenZy } from '@/infrastructure/helpers/slug-kenzy';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import { SLACK_TYPE } from '@/domain/slack/slack.enum';
import { CreateProductEntityDTO } from '@/infrastructure/data-transfers/product/create-entity.dto';

export class CreateProductsUseCase {
  constructor(
    private readonly _repository: ProductRepository,
    private readonly _infoRepository: ProductInfoRepository,
    private readonly _productCateRepository: ProductCategoryRepository,
    private readonly _productBrandRepository: ProductBrandRepository,
    private readonly _countryOriginRepository: CountryOriginRepository,
    private readonly _productVariantRepository: ProductVariantRepository,
    private readonly slack: SlackService,
  ) {}

  async existSlugAndSlugEn(slug: string, lang): Promise<boolean> {
    const reverseSlug = slug + '-' + lang;
    const signalGet = await this._infoRepository.get({
      conditions: { $or: [{ slug: slug }, { slug: reverseSlug }], lang: lang },
    });

    return !!signalGet;
  }

  async existErpCode(item_code: string): Promise<boolean> {
    const signalGet = await this._repository.get({
      conditions: { erpCode: item_code },
    });

    return !!signalGet;
  }

  private async createProductVariant({ _id, slug, name }): Promise<any> {
    const payload: IPayloadCreateProductVariant = {
      products: [_id],
      defaultProduct: _id,
      viSlug: slug,
      viName: name,
      enName: '',
      enSlug: '',
    };

    const result = await this._productVariantRepository.create(payload);
    return result?._id || null;
  }

  private cloneInfoWithLang(info: any, lang): any {
    if (!info) return null;
    const clone = { ...info };

    clone['lang'] = lang;
    clone['status'] = -1;
    clone['_id'] = new mongoose.Types.ObjectId();

    clone['slug'] = info['slug'] + '-' + lang;
    clone['name'] = info['name'] + '-' + lang;

    return clone;
  }

  private async getDataErpCodeModel(
    _model: any,
    erpCode: string,
  ): Promise<any> {
    return _model.get({
      conditions: { erpCode: erpCode },
      populates: [],
      select: '',
    });
  }

  private async getIdBrandOriginCateToErpcode({
    brand_code,
    origin_code,
    category_code,
  }): Promise<any> {
    const signalGetBrand = await this.getDataErpCodeModel(
      this._productBrandRepository,
      brand_code,
    );
    const brandId = signalGetBrand?._id || null;
    const signalGetCountryOrigin = await this.getDataErpCodeModel(
      this._countryOriginRepository,
      origin_code,
    );
    const originId = signalGetCountryOrigin?._id || null;
    const signalGetProductCate = await this.getDataErpCodeModel(
      this._productCateRepository,
      category_code,
    );
    const cateId = signalGetProductCate?._id || null;
    return {
      brandId,
      originId,
      cateId,
    };
  }

  private async createProductInfo({
    _id,
    lang,
    category_code,
    name,
    status,
    slug,
  }) {
    const _idInfo = new mongoose.Types.ObjectId();

    const payloadVi = {
      _id: _idInfo,
      product: _id,
      lang,
      category: category_code,
      name: name,
      status: status,
      slug: slug,
    };
    const payloadEn = this.cloneInfoWithLang(
      payloadVi,
      LANGUAGES_REVERSE[lang],
    );

    const signalUpdateInfo = await this._infoRepository.createMany([
      payloadVi,
      payloadEn,
    ]);
    const [vi, en] = signalUpdateInfo;
    return { _id, vi, en };
  }

  private async createProduct({ _id, vi, en, dataUpdateProduct }) {
    const dataUpdate = {
      _id,
      vi: vi._id,
      en: en._id,
      viName: vi.name,
      enName: en.name,
      viSlug: vi.slug,
      enSlug: en.slug,
      ...dataUpdateProduct,
    };

    const signalUpdate = await this._repository.createOrUpdate({
      conditions: { _id: _id },
      dataUpdate: { $set: dataUpdate },
      options: { upsert: true, new: true },
    });

    return signalUpdate;
  }

  async execute(payloads: CreateProductEntityDTO): Promise<any> {
    try {
      const {
        name,
        barCode,
        weight,
        product_date,
        abv,
        volume,
        item_code,
        quantities,
        price = 0,
        sku,
        brand_code,
        origin_code,
        category_code,
        ...dataUpdateInfo
      } = payloads;
      const { brandId, originId, cateId } =
        await this.getIdBrandOriginCateToErpcode({
          brand_code,
          origin_code,
          category_code,
        });
      const new_brand_code = brandId;
      const new_origin_code = originId;
      const new_category_code = cateId;
      const _id = new mongoose.Types.ObjectId();
      const slug = SlugKenZy.createURL(name);
      const lang = dataUpdateInfo?.lang || 'vi';
      const status = 2;

      const existsSlug = await this.existSlugAndSlugEn(
        slug,
        LANGUAGES_REVERSE[lang],
      );
      if (existsSlug) {
        // log and slack
        this.slack.sendSlackMessageWarn({
          title: `Message from API Create Product: Sản phẩm đã tồn tại slug!`,
          data: existsSlug,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
      }

      const existsErpCode = await this.existErpCode(item_code);
      if (existsErpCode) {
        this.slack.sendSlackMessageWarn({
          title: 'Message from API Create Product: Đã tồn tại item_code!',
          data: existsErpCode,
          payload: payloads,
          type: SLACK_TYPE.ERR,
        });
        return false;
        // log and slack
      }

      const variantID = await this.createProductVariant({ _id, name, slug });

      const dataUpdateProduct: IPayloadCreateProduct = {
        erpCode: item_code,
        barCode,
        weight,
        brand: new_brand_code,
        productDate: product_date,
        origin: new_origin_code,
        category: new_category_code,
        abv,
        volume,
        quantities,
        sku,
      };

      dataUpdateProduct.variant = variantID;
      if (lang == LANGUAGES.vi) {
        dataUpdateProduct.viPrice = price;
        dataUpdateProduct.viOldPrice = price;
        dataUpdateProduct.viStatus = status;
      }

      if (lang == LANGUAGES.en) {
        dataUpdateProduct.enPrice = price;
        dataUpdateProduct.enOldPrice = price;
        dataUpdateProduct.enStatus = status;
      }

      const { vi, en } = await this.createProductInfo({
        _id,
        lang,
        category_code: new_category_code,
        name,
        status,
        slug,
      });
      const product = await this.createProduct({
        _id,
        vi,
        en,
        dataUpdateProduct,
      });
      const _slackType = !!product ? SLACK_TYPE.MESS : SLACK_TYPE.ERR;
      this.slack.sendSlackMessageWarn({
        title: `Message from API Create Product: Create Product!`,
        data: product,
        payload: payloads,
        type: _slackType,
      });

      return !!product;
    } catch (error) {
      this.slack.sendSlackMessageWarn({
        title: `Message from API Create Product: Create Product fail!`,
        data: error,
        payload: payloads,
        type: SLACK_TYPE.ERR,
      });
      return false;
    }
  }
}

export const ProviderCreateProductsUseCaseProxy = {
  inject: [
    ProductRepository,
    ProductInfoRepository,
    ProductCategoryRepository,
    ProductBrandRepository,
    CountryOriginRepository,
    ProductVariantRepository,
    SlackService,
  ],
  provide: PRODUCT_UC_PROXY_TYPES.CREATE_PRODUCT_UC_PROXY,
  useFactory: (
    // logger: LoggerService,
    _repository: ProductRepository,
    _infoRepository: ProductInfoRepository,
    _productCateRepository: ProductCategoryRepository,
    _productBrandRepository: ProductBrandRepository,
    _countryOriginRepository: CountryOriginRepository,
    _productVariantRepository: ProductVariantRepository,
    slack: SlackService,
  ) =>
    new UseCaseProxy(
      new CreateProductsUseCase(
        _repository,
        _infoRepository,
        _productCateRepository,
        _productBrandRepository,
        _countryOriginRepository,
        _productVariantRepository,
        slack,
      ),
    ),
};
