import { IUCProxy } from '@/domain/usecases-proxy';
import { BcryptService } from '@/infrastructure/services/bcrypt/bcrypt.service';
import { ProviderErpConnect } from './modules/admins/admin-uc-proxy.provider';
import { ProviderProductBrands } from './modules/brands/brand-uc-proxy.provider';
import { ProviderCategories } from './modules/categories/category-uc-proxy.provider';
import { ProviderCustomer } from './modules/customers/customer-uc-proxy.provider';
import { ProviderImages } from './modules/images/image-uc-proxy.provider';
import { ProviderOrders } from './modules/orders/order-uc-proxy.provider';
import { ProviderCountryOrigin } from './modules/origins/origin-uc-proxy.provider';
import { ProviderProducts } from './modules/products/product-uc-proxy.provider';
import { ProviderReasons } from './modules/reasons/reason-uc-proxy.provider';

const providers = [
  ...ProviderImages,
  ...ProviderReasons,
  ...ProviderCategories,
  ...ProviderCustomer,
  ...ProviderCountryOrigin,
  ...ProviderProductBrands,
  ...ProviderProducts,
  ...ProviderOrders,
  ...ProviderErpConnect,
];

export class ProviderUseCasesProxy {
  static register(): IUCProxy {
    return {
      providers: [...providers, BcryptService],
      exports: [...providers],
    };
  }
}
