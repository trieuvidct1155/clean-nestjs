import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AllExceptionFilter } from '@infrastructure/common/filter/exception.filter';
import { ResponseInterceptor } from '@infrastructure/common/interceptors/response.interceptor';
import { EnvironmentConfigService } from '@infrastructure/config/environment-config/environment-config.service';
import { WinstonConfigService } from '@infrastructure/config/winston-config/winston-config.service';
import { LoggerService } from '@infrastructure/logger/logger.service';
import { SlackService } from '@infrastructure/services/slack/slack.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });

  app.useGlobalPipes(new ValidationPipe());

  const configService = app.get(EnvironmentConfigService);
  const environmentConfigService = app.get(EnvironmentConfigService);

  const slack = new SlackService(environmentConfigService);

  const logger = new LoggerService(
    new WinstonConfigService(configService),
    environmentConfigService,
  );

  app.useGlobalFilters(new AllExceptionFilter(logger, slack));

  app.useGlobalInterceptors(new ResponseInterceptor());

  logger.log('App', 'My App Running on port: ' + 3000);
  await app.listen(3000);
}
bootstrap();
