export interface IPayloadCreateProductVariant {
  products: Array<any>;
  defaultProduct: any;
  viName: string;
  viSlug: string;
  enName: string;
  enSlug: string;
}
