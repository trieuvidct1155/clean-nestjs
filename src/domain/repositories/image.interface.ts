import { ImageEntity } from '@entities/image.entity';
import { IBaseRepository } from './base.interface';

export type IImageRepository = IBaseRepository<ImageEntity>;
