import { ProductEntity } from '@entities/product.entity';
import { IBaseRepository } from './base.interface';

export type IProductRepository = IBaseRepository<ProductEntity>;

export interface IPayloadCreateProduct {
  erpCode: string;
  barCode: any;
  weight: number;
  brand: string;
  productDate: Date;
  origin: string;

  category: string;
  abv: number;
  volume: number;
  quantities: number;
  sku: string;

  variant?: string;
  modifiedAt?: Date;
  createdAt?: Date;
  lang?: string;
  viPrice?: number;
  viOldPrice?: number;
  viStatus?: number;

  enPrice?: number;
  enOldPrice?: number;
  enStatus?: number;
}
export interface IpayloadUpdateProduct {
  barCode?: string;
  sku?: string;
  productDate?: Date;
  viPrice?: number;
  weight?: number;
  abv?: number;
  volume?: number;
}
