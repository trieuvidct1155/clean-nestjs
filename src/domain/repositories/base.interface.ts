import {
  FilterQuery,
  PopulateOptions,
  QueryOptions,
  ReturnsNewDoc,
  UpdateQuery,
} from 'mongoose';

type IPopulateOptions = PopulateOptions[];

export interface IGetRepository<MODEL> {
  conditions: FilterQuery<MODEL>;
  populates?: IPopulateOptions;
  select?: string;
}

export interface IGetListRepository<MODEL> {
  conditions: FilterQuery<MODEL>;
  populates: IPopulateOptions;
  select: string;
  search: string;
  skip: number;
  limit: number;
  sort: any;
}

export interface IUpdateRepository<MODEL> {
  conditions: FilterQuery<MODEL>;
  dataUpdate: UpdateQuery<MODEL>;
}

export interface ICreateOrUpdateRepository<MODEL> {
  conditions: FilterQuery<MODEL>;
  dataUpdate: UpdateQuery<MODEL>;
  options: QueryOptions<MODEL> & { upsert: true } & ReturnsNewDoc;
  populates?: IPopulateOptions;
}

export interface ICountRepository<MODEL> {
  conditions: FilterQuery<MODEL>;
}

export interface IRemoveManyRepository<MODEL> {
  conditions: FilterQuery<MODEL>;
}

export interface IBaseRepository<MODEL> {
  get: (payload: IGetRepository<MODEL>) => Promise<MODEL>;
  getList: (payload: Partial<IGetRepository<MODEL>>) => Promise<MODEL[]>;
  create: (payload: Partial<MODEL>) => Promise<MODEL>;
  createMany: (payloads: Array<Partial<MODEL>>) => Promise<MODEL[]>;
  updateMany: (payload: IUpdateRepository<MODEL>) => Promise<MODEL>;
  updateOne: (payload: IUpdateRepository<MODEL>) => Promise<MODEL>;
  createOrUpdate: (payload: ICreateOrUpdateRepository<MODEL>) => Promise<MODEL>;
  count: (payload: ICountRepository<MODEL>) => Promise<number>;
  removeMany: (payload: IRemoveManyRepository<MODEL>) => Promise<boolean>;
}
