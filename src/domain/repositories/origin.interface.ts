import { CountryOriginInfoEntity } from '@/infrastructure/entities/countryOriginInfo.entity';
import { CountryOriginRepository } from '@/infrastructure/repositories/countryOrigin.repository';
import { CountryOriginInfoRepository } from '@/infrastructure/repositories/countryOriginInfo.repository';

export interface IpayloadUpdateOrigin {
  lang: string;
  otherLangInfo: CountryOriginInfoEntity;
  id: any;
  multiLangField: string;
  infoService: CountryOriginInfoRepository;
  service: CountryOriginRepository;
}
