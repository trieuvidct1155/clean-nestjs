import { ProductCategoryInfoEntity } from '@/infrastructure/entities/productCategoryInfo.entity';
import { ProductCategoryRepository } from '@/infrastructure/repositories/productCategory.repository';
import { ProductCategoryInfoRepository } from '@/infrastructure/repositories/productCategoryInfo.repository';

export interface IpayloadUpdateProductCate {
  lang: string;
  otherLangInfo: ProductCategoryInfoEntity;
  id: any;
  multiLangField: string;
  infoService: ProductCategoryInfoRepository;
  service: ProductCategoryRepository;
}
