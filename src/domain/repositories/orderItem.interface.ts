import { OrderItemEntity } from '@/infrastructure/entities/orderItems.entity';
import { ProductEntity } from '@/infrastructure/entities/product.entity';
import { IBaseRepository } from './base.interface';

export type IOrderItemRepository = IBaseRepository<OrderItemEntity>;

export interface ICreateOrderItem {
  signalProducts: any;
  order_items?: any;
  id: any;
  id_order: string;
}
