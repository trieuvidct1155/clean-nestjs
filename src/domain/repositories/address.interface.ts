import { AddressEntity } from '@/infrastructure/entities/address.entity';
import { IBaseRepository } from './base.interface';

export type IAddressRepository = IBaseRepository<AddressEntity>;
