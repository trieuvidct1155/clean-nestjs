import { PromotionEntity } from '@/infrastructure/entities/promotion.entity';
import { IBaseRepository } from './base.interface';

export type IPromotionRepository = IBaseRepository<PromotionEntity>;
