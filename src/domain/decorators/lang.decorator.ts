export enum ELang {
  EN = 'en',
  VI = 'vi',
}
export interface ILanguage {
  lang: ELang;
}
