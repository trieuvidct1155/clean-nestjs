import { SlackBlock } from '@/infrastructure/helpers/slack-block';

export interface IPayloadSlackSendMessage {
  type: string;
  message?: string;
  broker: SlackBlock;
}

export interface IPayloadSlackSendMessageWarn {
  type: string;
  title: string;
  data: any;
  payload: any;
}

export interface ISlackService {
  getChannel(type: string): string;
  sendSlackMessage(slackMessage: IPayloadSlackSendMessage): Promise<void>;
}
