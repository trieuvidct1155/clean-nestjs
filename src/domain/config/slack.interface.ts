export interface SlackConfig {
  getTokenSlack(): string;
  getChancelDev(): string;
  getChancelErr(): string;
}
