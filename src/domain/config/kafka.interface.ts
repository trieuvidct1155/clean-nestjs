export interface KafkaConfig {
  getBrokers(): Array<string>;
  getGroupId(): string;
}
