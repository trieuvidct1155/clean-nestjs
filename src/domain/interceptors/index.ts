export const DIR_UPLOAD = {
  PUBLIC_STORAGE_DIR: './public',

  IMAGES_STORAGE_DIR_SRC: '/uploads/images/',
  VIDEOS_STORAGE_DIR_SRC: '/uploads/videos/',
  DOCS_STORAGE_DIR_SRC: '/uploads/documents/',

  VIDEOS_STORAGE_DIR: './public/uploads/videos/',
  IMAGES_STORAGE_DIR: './public/uploads/images/',
  DOCS_STORAGE_DIR: './public/uploads/documents/',

  MAX_VIDEO_SIZE_MB: 50,
  MAX_IMAGE_SIZE_MB: 500,
};
