export interface IPayloadDeleteImage {
  ids: Array<string>;
}

export interface IDeleteImagesUseCase {
  execute(payload: IPayloadDeleteImage): Promise<boolean>;
}

export abstract class AbsDeleteImagesUseCase implements IDeleteImagesUseCase {
  abstract execute(payload: IPayloadDeleteImage): Promise<boolean>;
}
