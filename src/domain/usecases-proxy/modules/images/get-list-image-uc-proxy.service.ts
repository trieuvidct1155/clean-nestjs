import { IPaging } from '@/domain/decorators/paging.decorator';
import { ImageEntity } from '@/infrastructure/entities/image.entity';

export interface IPayloadGetListImage {
  search: string;
  paging: IPaging;
}

export interface IGetListImagesUseCase {
  execute(payload: IPayloadGetListImage): Promise<ImageEntity[]>;
}

export abstract class AbsGetListImagesUseCase implements IGetListImagesUseCase {
  abstract execute(payload: IPayloadGetListImage): Promise<ImageEntity[]>;
}
