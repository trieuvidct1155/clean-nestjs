import { IUpdateRepository } from '@/domain/repositories/base.interface';
import { ProductCategoryEntity } from '@/infrastructure/entities/productCategory.entity';

export type IPayloadUpdateCategory = IUpdateRepository<ProductCategoryEntity>;

export interface IUpdateCategoriesUseCase {
  execute(payload: IPayloadUpdateCategory): Promise<ProductCategoryEntity>;
}

export abstract class AbsUpdateCategoriesUseCase
  implements IUpdateCategoriesUseCase
{
  abstract execute(
    payload: IPayloadUpdateCategory,
  ): Promise<ProductCategoryEntity>;
}
