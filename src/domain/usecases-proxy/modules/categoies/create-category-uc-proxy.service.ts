import { ProductCategoryEntity } from '@/infrastructure/entities/productCategory.entity';

export type IPayloadCreateCategories = Array<Partial<ProductCategoryEntity>>;

export interface ICreateCategoriesUseCase {
  execute(payload: IPayloadCreateCategories): Promise<void>;
}

export abstract class AbsCreateReasonsUseCase
  implements ICreateCategoriesUseCase
{
  abstract execute(payload: IPayloadCreateCategories): Promise<void>;
}
