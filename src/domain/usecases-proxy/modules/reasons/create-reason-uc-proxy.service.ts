import { ReasonEntity } from '@/infrastructure/entities/reason.entity';

export type IPayloadCreateReason = Array<Partial<ReasonEntity>>;

export interface ICreateReasonsUseCase {
  execute(payload: IPayloadCreateReason): Promise<void>;
}

export abstract class AbsCreateReasonsUseCase implements ICreateReasonsUseCase {
  abstract execute(payload: IPayloadCreateReason): Promise<void>;
}
