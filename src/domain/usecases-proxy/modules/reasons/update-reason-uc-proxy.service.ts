import { IUpdateRepository } from '@/domain/repositories/base.interface';
import { ReasonEntity } from '@/infrastructure/entities/reason.entity';

export type IPayloadUpdateReason = IUpdateRepository<ReasonEntity>;

export interface IUpdateReasonsUseCase {
  execute(payload: IPayloadUpdateReason): Promise<ReasonEntity>;
}

export abstract class AbsUpdateReasonsUseCase implements IUpdateReasonsUseCase {
  abstract execute(payload: IPayloadUpdateReason): Promise<ReasonEntity>;
}
