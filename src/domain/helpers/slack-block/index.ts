import { Block, KnownBlock } from '@slack/web-api';

export interface ISlackBlock {
  setSectionTextBlock(payload: any): void;
  setHeaderBlock(text: string): void;
  setDividerBlock(): void;
  getBlocks(): (Block | KnownBlock)[];
}
