export interface IValidateFile {
  isImage(): boolean;
  isVideo(): boolean;
  isDocument(): boolean;
}
