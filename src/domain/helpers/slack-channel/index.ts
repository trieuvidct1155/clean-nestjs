export interface ISlackChannel {
  getChannel(): string;
}
