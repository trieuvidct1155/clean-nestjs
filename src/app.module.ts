import { UseCasesProxyModule } from './infrastructure/usecases-proxy/usecases-proxy.module';
import { Module } from '@nestjs/common';
import { ControllersModule } from './infrastructure/controllers/controller.module';
import { TypegooseConfigModule } from './infrastructure/config/typegoose/typegoose.module';
import { LoggerModule } from './infrastructure/logger/logger.module';
import { ExceptionsModule } from './infrastructure/exceptions/exceptions.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../', './public/'),
    }),
    LoggerModule,
    ExceptionsModule,
    TypegooseConfigModule,
    UseCasesProxyModule.register(),
    ControllersModule,
  ],
})
export class AppModule {}
