const POST_DEFAULT = {
  viName: '(Chưa có tiêu đề)',
  viSlug: 'chua-co-tieu-de',
  enName: '(non titled)',
  enSlug: 'non-titled',
};

const myMathRound = (num) => num + (num - parseInt(num) > 0 ? 1 : 0);

const RENDER_ID = (id) => {
  const ID = `${id}`;
  return ID.slice(ID.length - 4, ID.length);
};

const PAGINATION = (total = 0, limit = 0, url = '', oldQuery = '') => {
  // const URL = url.match(/\?/) ? url : url + "?";
  const pagination = [];
  // console.log({ total, limit, url, oldQuery });
  for (let i = 1; i <= myMathRound(total / limit); i++) {
    pagination.push(
      url + oldQuery.replace(/&page=\d*|page=\d*/, '') + '&page=' + i,
    );
  }

  return pagination;
};

function FORMAT_ID_FN(id = '') {
  return '......' + id.slice(id.length - 6);
}

function FORMAT_CURRENCY_FN(num) {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const UPDATE_NAME_SLUG_BY_LANG = ({
  dataUpdate = {},
  lang = 'vi',
  name = '',
  slug = '',
}) => {
  const update =
    lang == 'vi'
      ? { viSlug: slug, viName: name }
      : { enSlug: slug, enName: name };
  return { ...dataUpdate, ...update };
};

const LANGUAGES = { vi: 'vi', en: 'en' };
const LANGUAGES_REVERSE = { vi: 'en', en: 'vi' };
const LANGUAGES_REVERSE_STRING = { vi: '-en', en: '-vi' };
const LANGUAGE_ENUM = [LANGUAGES.vi, LANGUAGES.en];

const statusOrder = {
  '3': 'Giao Thành Công',
  '2': 'Đang giao',
  '1': 'Đã xác nhận',
  '0': 'Chờ xác nhận',
  '-1': 'Giao thất bại',
  '-2': 'Đã Hủy',
};

// 👇️ named export
export {
  PAGINATION,
  RENDER_ID,
  FORMAT_ID_FN,
  FORMAT_CURRENCY_FN,
  UPDATE_NAME_SLUG_BY_LANG,
  LANGUAGES,
  LANGUAGES_REVERSE,
  LANGUAGES_REVERSE_STRING,
  LANGUAGE_ENUM,
  statusOrder,
};
