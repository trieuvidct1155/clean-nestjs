import { LANG } from '@/domain/lang/lang.enum';
import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { UserEntity } from './user.entity';

// * 	1 - Admin
// * 	2 - Sales
// *    3 - ....
// * 	4 - ....
// *    5 - ....

export enum AUTH_ROLES {
  ADMIN = 'ADMIN',
  SALE = 'SALE',
  EMPLOYEE = 'EMPLOYEE',
}

@Exclude()
export class AdminEntity extends BaseEntity {
  @prop({ required: true, index: true, minlength: 6, maxlength: 100 })
  public username: string;

  @prop({ required: true, minlength: 6, maxlength: 100 })
  public password: string;

  @prop({})
  public fullName: number;

  @prop({ ref: () => UserEntity, default: null })
  public userId: Ref<UserEntity>;

  @prop({ enum: AUTH_ROLES, default: null })
  roles: AUTH_ROLES;

  static get model(): ModelType<AdminEntity> {
    return getModelForClass(AdminEntity, {
      schemaOptions: { ...schemaOptions, collection: 'admins' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
