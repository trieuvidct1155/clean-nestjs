import { LANG } from '@/domain/lang/lang.enum';
import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';

@Exclude()
export class ReasonEntity extends BaseEntity {
  @prop()
  name: string;

  @Expose()
  @prop({ unique: true })
  erpCode: string;

  @prop({ required: true, default: LANG.VI, enum: LANG })
  lang: LANG;

  static get model(): ModelType<ReasonEntity> {
    return getModelForClass(ReasonEntity, {
      schemaOptions: { ...schemaOptions, collection: 'reasons' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
