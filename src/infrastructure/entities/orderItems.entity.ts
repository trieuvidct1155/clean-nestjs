import { LANG } from '@/domain/lang/lang.enum';
import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { ImageEntity } from './image.entity';
import { OrderEntity } from './order.entity';
import { ProductEntity } from './product.entity';
import { ProductCategoryEntity } from './productCategory.entity';

@Exclude()
export class OrderItemEntity extends BaseEntity {
  @Expose()
  @prop({ ref: () => ProductEntity })
  product: Ref<ProductEntity>;

  @Expose()
  @prop({ ref: () => ProductCategoryEntity })
  category: Ref<ProductCategoryEntity>;

  @prop()
  price: number;

  @prop()
  totalPrice: number;

  @prop()
  quantities: number;

  @prop()
  isGift: number;

  @prop({ ref: () => OrderEntity, required: true })
  order: Ref<OrderEntity>;

  @prop({ ref: () => ImageEntity })
  qrCode: Ref<ImageEntity>;

  @prop({ required: true, default: LANG.VI, enum: LANG })
  lang: LANG;

  static get model(): ModelType<OrderItemEntity> {
    return getModelForClass(OrderItemEntity, {
      schemaOptions: { ...schemaOptions, collection: 'order_items' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
