import { LANG } from '@/domain/lang/lang.enum';
import { getModelForClass, prop, Ref } from '@typegoose/typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { EProductStatus, ProductEntity } from './product.entity';
import { ProductCategoryEntity } from './productCategory.entity';

export class ProductInfoSeo {
  //SEO
  @prop({ required: true })
  seo_title: string;

  @prop({ required: true })
  seo_description: string;

  @prop({ default: null })
  seo_keywords: string;

  @prop({ default: null })
  seo_canonical: number;

  @prop({ default: null })
  seo_schema: string;
}

@Exclude()
export class ProductInfoEntity extends BaseEntity {
  @prop({ required: true, index: true })
  name: string;

  @prop({})
  description: string;

  @prop({ ref: () => ProductEntity })
  product: Ref<ProductEntity>;

  @prop({ ref: () => ProductCategoryEntity })
  category: Ref<ProductCategoryEntity>;

  /*** STATUS
   * 2 - public, index
   * 1 - public, noindex
   * 0 - Redirect ,noIndex
   * -1 - private
   * -2 - trash
   ***/
  @prop({ default: 0 })
  status: number;

  @Expose()
  @prop({ unique: true })
  public slug: string;

  @Expose()
  @prop()
  public content: string;

  @Expose()
  @prop()
  public guidance: string;

  @Expose()
  @prop()
  public promotions: string;

  @Expose()
  @prop()
  public views: number;

  @prop({ required: true, default: LANG.VI, enum: LANG })
  lang: LANG;

  @prop({ type: () => ProductInfoSeo })
  seo: ProductInfoSeo;

  static get model(): ModelType<ProductInfoEntity> {
    return getModelForClass(ProductInfoEntity, {
      schemaOptions: { ...schemaOptions, collection: 'product_infos' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
