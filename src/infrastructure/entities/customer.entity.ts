import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude } from 'class-transformer';
import { AddressEntity } from './address.entity';
import { BaseEntity, schemaOptions } from './base.entity';
import { ProductEntity } from './product.entity';

export class ListAddress {
  @prop({ ref: () => AddressEntity, required: true })
  default: Ref<AddressEntity>;

  @prop({ ref: () => AddressEntity, default: [], allowMixed: 0 })
  list: Ref<AddressEntity>[];
}

@Exclude()
export class CustomerEntity extends BaseEntity {
  @prop({ required: true })
  name: string;

  @prop({})
  description: string;

  @prop({})
  email: string;

  @prop({})
  status: number;

  @prop({})
  createFromType: number;

  @prop({ default: null })
  user: string;

  @prop({})
  erpCode: string;

  @prop({})
  gender: number;

  @prop({})
  phone: string;

  @prop({})
  dateOfBirth: number;

  @prop({})
  monthOfBirth: number;

  @prop({})
  yearOfBirth: number;

  @prop({ type: () => ListAddress })
  listAddress: ListAddress;

  @prop({ ref: () => ProductEntity, required: true })
  favoriteWine: Ref<ProductEntity>[];

  @prop({ ref: () => ProductEntity, required: true })
  favoriteFood: Ref<ProductEntity>[];

  static get model(): ModelType<CustomerEntity> {
    return getModelForClass(CustomerEntity, {
      schemaOptions: { ...schemaOptions, collection: 'customers' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
