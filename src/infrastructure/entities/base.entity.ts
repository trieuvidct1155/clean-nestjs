import { prop } from '@typegoose/typegoose';
import { Expose } from 'class-transformer';

export class BaseEntity {
  @prop()
  @Expose()
  createdAt: Date;

  @prop()
  @Expose()
  modifier: Date;

  @prop()
  @Expose()
  modifiedAt: Date;

  public _id: string;

  @Expose()
  public __v: number;
}

export const schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true,
    getters: true,
  },
};
