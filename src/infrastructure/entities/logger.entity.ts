import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { AdminEntity } from './admin.entity';
import { BaseEntity, schemaOptions } from './base.entity';
import { StoreLocationEntity } from './storeLocation.entity';

export class ACTIONS {
  @prop({})
  status: string;

  @prop({})
  noteAdmin: string;

  @prop({})
  optionNote: string;

  @Expose()
  @prop({ ref: () => StoreLocationEntity, required: false, default: null })
  public storeLocation: Ref<StoreLocationEntity>;
}

@Exclude()
export class LoggerEntity extends BaseEntity {
  @Expose()
  @prop({ ref: () => AdminEntity, required: false, default: null })
  public user: Ref<AdminEntity>;

  @Expose()
  @prop({})
  public type: string;

  @prop({ type: () => ACTIONS })
  actions: ACTIONS;

  static get model(): ModelType<LoggerEntity> {
    return getModelForClass(LoggerEntity, {
      schemaOptions: { ...schemaOptions, collection: 'logger' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
