import { getModelForClass, Index, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { ProductEntity } from '@entities/product.entity';
import { LANG } from '@/domain/lang/lang.enum';

@Exclude()
export class StoreLocationEntity extends BaseEntity {
  @Expose()
  @prop({})
  public title: string;

  @Expose()
  @prop({})
  public specialName: string;

  @Expose()
  @prop({})
  public slug: string;

  @Expose()
  @prop({})
  public address: string;

  @Expose()
  @prop({})
  public openTime: string;

  @Expose()
  @prop({})
  public phone: string;

  @prop({ required: true, default: LANG.VI, enum: LANG })
  lang: LANG;

  @Expose()
  @prop({})
  public thumbnail: string;

  @Expose()
  @prop({})
  public gallery: string;

  @Expose()
  @prop()
  public erpCode: string;

  static get model(): ModelType<StoreLocationEntity> {
    return getModelForClass(StoreLocationEntity, {
      schemaOptions: { ...schemaOptions, collection: 'store_location' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
