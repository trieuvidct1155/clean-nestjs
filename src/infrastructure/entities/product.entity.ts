import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { CountryOriginInfoEntity } from './countryOriginInfo.entity';
import { ImageEntity } from './image.entity';
import { ProductBrandEntity } from './productBrand.entity';
import { ProductCategoryEntity } from './productCategory.entity';
import { ProductInfoEntity } from './productInfo.entity';

export enum EProductStatus {
  PUBLIC = 2,
  PUBLIC_NOINDEX = 1,
  PRIVATE = 0,
}

@Exclude()
export class ProductEntity extends BaseEntity {
  @Expose()
  @prop({ ref: () => ProductInfoEntity, required: false, default: null })
  public vi: Ref<ProductInfoEntity>;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public viName: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public viSlug: string;

  @Expose()
  @prop({ ref: () => ProductInfoEntity, required: false, default: null })
  public en: Ref<ProductInfoEntity>;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public enName: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public enSlug: string;

  @Expose()
  @prop()
  serials: string;

  @Expose()
  @prop()
  quantities: number;

  @Expose()
  @prop()
  weight: number;

  @Expose()
  @prop({ ref: () => ProductBrandEntity, required: false, default: null })
  public brand: Ref<ProductBrandEntity>;

  @Expose()
  @prop({ allowMixed: 0, default: 0 })
  ratingStars: number;

  @Expose()
  @prop({ allowMixed: 0, default: 0 })
  ratingCount: number;

  @Expose()
  @prop({ allowMixed: 0, type: Array, default: [0, 0, 0, 0, 0] })
  ratingSingleCounts: string[];

  @Expose()
  @prop({ default: 0 })
  viPrice: number;

  @Expose()
  @prop({ default: 0 })
  enPrice: number;

  @Expose()
  @prop({ default: 0 })
  viOldPrice: number;

  @Expose()
  @prop({ default: 0 })
  enOldPrice: number;

  @Expose()
  @prop()
  productDate: Date;

  @Expose()
  @prop()
  orderCount: number;

  @Expose()
  @prop({ allowMixed: 0, default: [] })
  public childList: string[];

  @Expose()
  @prop({ default: null })
  public box: string;

  @Expose()
  @prop({ ref: () => CountryOriginInfoEntity, required: false, default: null })
  public origin: Ref<CountryOriginInfoEntity>;

  @Expose()
  @prop({ ref: () => ImageEntity, required: true })
  thumbnail: Ref<ImageEntity>;

  @Expose()
  @prop({ default: null })
  public variant: string;

  @Expose()
  @prop({ ref: () => ProductCategoryEntity, required: true })
  category: Ref<ProductCategoryEntity>;

  @Expose()
  @prop({ ref: () => ProductEntity, required: true })
  relatedProducts: Ref<ProductEntity>;

  @Expose()
  @prop({ ref: () => ProductEntity, required: true })
  brandProducts: Ref<ProductEntity>;

  @prop()
  type: number;

  @prop()
  totalViews: number;

  @prop()
  point: number;

  @prop()
  abv: number;

  @prop()
  volume: number;

  @prop()
  vintage: number;

  @Expose()
  @prop({ default: null })
  public states: string;

  @Expose()
  @prop({ allowMixed: 0, default: null })
  public grape: string[];

  @Expose()
  @prop({ default: null })
  public version: string;

  @Expose()
  @prop({ default: null })
  public productStyle: string;

  @Expose()
  @prop({ default: null })
  public unit: string;

  @prop()
  isShowMap: number;

  @prop()
  viStatus: number;

  @prop()
  enStatus: number;

  @prop()
  sku: string;

  @prop()
  parentID: string;

  @prop()
  productYear: string;

  @prop({ default: [], allowMixed: 0 })
  oldCategories: string[];

  @prop()
  stockStatus: string;

  @prop()
  oldDescription: string;

  @prop()
  id: string;

  @prop()
  barCode: string;

  @prop({ default: null })
  salePromotion: string;

  @prop({ default: null, unique: true })
  erpCode: string;

  static get model(): ModelType<ProductEntity> {
    return getModelForClass(ProductEntity, {
      schemaOptions: { ...schemaOptions, collection: 'products' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
