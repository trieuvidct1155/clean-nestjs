import { getModelForClass, Index, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { ProductEntity } from '@entities/product.entity';

@Index({ viName: 'text', viSlug: 'text', enName: 'text', enSlug: 'text' })
@Exclude()
export class ProductVariantEntity extends BaseEntity {
  @Expose()
  @prop({ required: true, ref: () => ProductEntity })
  public products: Ref<ProductEntity>[];

  @Expose()
  @prop({ required: true, ref: () => ProductEntity })
  public defaultProduct: Ref<ProductEntity>;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public viName: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public viSlug: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public enName: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public enSlug: string;

  static get model(): ModelType<ProductVariantEntity> {
    return getModelForClass(ProductVariantEntity, {
      schemaOptions: { ...schemaOptions, collection: 'product_variants' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
