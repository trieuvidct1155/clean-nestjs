import { prop } from '@typegoose/typegoose';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class SkuEntity {
  @Expose()
  @prop({ required: true, index: true })
  public barCode: string;

  @Expose()
  @prop({ required: true, index: true })
  public importPrice: number;

  @Expose()
  @prop({ required: true, index: true })
  public oldPrice: number;

  @Expose()
  @prop({ required: true, index: true })
  public wholesalePrice: number;

  @Expose()
  @prop({ required: true, index: true })
  public price: number;

  @Expose()
  @prop({ required: true, index: true })
  public quantity: number;

  @Expose()
  @prop({ required: true, index: true })
  public color: string;

  @Expose()
  @prop({ default: new Date() })
  public importDate: Date;

  @Expose()
  @prop({ default: new Date() })
  public exportDate: Date;
}
