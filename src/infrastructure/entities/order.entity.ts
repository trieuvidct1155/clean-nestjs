import { getModelForClass, Pre, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { CustomerEntity } from './customer.entity';
import { LoggerEntity } from './logger.entity';
import { OrderItemEntity } from './orderItems.entity';
import { ReasonEntity } from './reason.entity';

export enum CONTACTPAY {
  CASH = 'cash',
  ONLINE = 'online',
}

export class ADDRESS {
  @prop({})
  address: string;

  @prop({})
  province: string;

  @prop({})
  district: string;

  @prop({})
  ward: string;
}

export class INFOCUSTOMER {
  @prop({ type: () => ADDRESS })
  location: ADDRESS;

  @prop()
  name: string;

  @prop()
  email: string;

  @prop()
  customerPhone: string;

  @prop({ ref: () => CustomerEntity, required: true })
  customer: Ref<CustomerEntity>;
}

@Exclude()
@Pre<OrderEntity>('updateOne', function (next) {
  const _shippingStatus = this?.['_update']?.['$set']?.['shippingStatus'] || 0;
  let _status;
  switch (_shippingStatus) {
    case 0:
      _status = 1;
      break;
    case 1:
      _status = 2;
      break;
    case 2:
      _status = 2;
      break;
    case 3:
      _status = 3;
      break;
    case -2:
      _status = -2;
      break;
    default:
      _status = 1;
      break;
  }

  this.set({ status: _status });
  next();
})
export class OrderEntity extends BaseEntity {
  @prop({ ref: () => OrderItemEntity, required: true })
  orderItems: Ref<OrderItemEntity>[];

  @prop({})
  orderID: string;

  @prop({ default: 0 })
  status: number;

  @prop({ default: 0 })
  shippingStatus: number;

  @prop({})
  note: string;

  @prop({})
  noteAdmin: string;

  @prop({ enum: CONTACTPAY, default: null })
  contactPay: CONTACTPAY;

  @prop({ default: 0 })
  totalAmount: number;

  @prop({ default: 0 })
  totalAmountTemporary: number;

  @prop({ default: 0 })
  totalAmountPromotion: number;

  @prop({ default: 0 })
  deliveryCharges: number;

  @prop({ default: null })
  promotion: string;

  @prop({ ref: () => CustomerEntity, required: true })
  customer: Ref<CustomerEntity>;

  @prop({ default: null })
  processUser: string;

  @prop({ default: null })
  storeLocation: string;

  @prop({ default: null })
  optionNote: number;

  @prop({ ref: () => ReasonEntity, required: true })
  reason: Ref<ReasonEntity>;

  @prop({ default: null })
  isLogProduct: boolean;

  @prop({ ref: () => LoggerEntity, default: null, required: true })
  histories: Ref<LoggerEntity>[];

  @prop()
  cancelDate: Date;

  @prop()
  confirmDate: Date;

  @prop()
  deliveryDate: Date;

  @prop()
  salesChannel: string;

  @prop()
  fromSystemCreate: number;

  @prop()
  erpCode: string;

  @prop({ type: () => INFOCUSTOMER })
  infoCustomer: INFOCUSTOMER;

  static get model(): ModelType<OrderEntity> {
    return getModelForClass(OrderEntity, {
      schemaOptions: { ...schemaOptions, collection: 'orders' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
