import { getModelForClass, Index, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { LANG } from '@/domain/lang/lang.enum';
import { ImageEntity } from './image.entity';
import { CountryOriginInfoEntity } from './countryOriginInfo.entity';

@Exclude()
export class CountryOriginEntity extends BaseEntity {
  @Expose()
  @prop({ ref: () => CountryOriginInfoEntity, required: false, default: null })
  public vi: Ref<CountryOriginInfoEntity>;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public viName: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public viSlug: string;

  @Expose()
  @prop({ ref: () => CountryOriginInfoEntity, required: false, default: null })
  public en: Ref<CountryOriginInfoEntity>;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public enName: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public enSlug: string;

  ///////////////////
  @Expose()
  @prop()
  states: string;

  @prop({ ref: () => ImageEntity, required: true })
  thumbnail: Ref<ImageEntity>;

  @prop({ ref: () => ImageEntity, required: true })
  icon: Ref<ImageEntity>;

  @prop({ ref: () => ImageEntity, required: true })
  banner: Ref<ImageEntity>;

  @Expose()
  @prop({ default: [], allowMixed: 0 })
  relatedPosts: string[];

  @Expose()
  @prop({ unique: true })
  erpCode: string;

  static get model(): ModelType<CountryOriginEntity> {
    return getModelForClass(CountryOriginEntity, {
      schemaOptions: { ...schemaOptions, collection: 'country_origins' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
