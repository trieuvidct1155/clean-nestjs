import { getModelForClass, index, prop } from '@typegoose/typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';

@Exclude()
export class AddressEntity extends BaseEntity {
  @Expose()
  @prop({})
  public address: string;

  @Expose()
  @prop({})
  public province: string;

  @Expose()
  @prop({})
  public district: string;

  @Expose()
  @prop({})
  public ward: string;

  @Expose()
  @prop({})
  public name: string;

  @Expose()
  @prop({})
  public phone: string;

  @Expose()
  @prop({})
  public email: string;

  @prop({})
  erpDistrict: string;

  @prop({})
  erpWard: string;

  @prop({})
  erpProvince: string;

  @Expose()
  @prop({ default: 0 })
  public type: number;

  static get model(): ModelType<AddressEntity> {
    return getModelForClass(AddressEntity, {
      schemaOptions: { ...schemaOptions, collection: 'addresses' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
