import { LANG } from '@/domain/lang/lang.enum';
import { getModelForClass, prop, Ref } from '@typegoose/typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Exclude } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { ProductCategoryEntity } from './productCategory.entity';

export class ProductCategoryInfoSeo {
  //SEO
  @prop({})
  seo_title: string;

  @prop({})
  seo_description: string;

  @prop({ default: null })
  seo_keywords: string;

  @prop({ default: null })
  seo_schema: number;

  @prop({ default: null })
  seo_canonical: number;

  @prop({ default: null })
  seo_redirect: number;

  @prop({ default: null })
  seo_lang: number;
}

@Exclude()
export class ProductCategoryInfoEntity extends BaseEntity {
  @prop({})
  name: string;

  @prop({})
  status: number;

  @prop({})
  description: string;

  @prop({})
  specialName: string;

  @prop({})
  content: string;

  @prop({})
  slug: string;

  @prop({ required: true, default: LANG.VI, enum: LANG })
  lang: LANG;

  @prop({ ref: () => ProductCategoryEntity, required: true })
  productCategory: Ref<ProductCategoryEntity>;

  @prop({ type: () => ProductCategoryInfoSeo })
  seo: ProductCategoryInfoSeo;

  static get model(): ModelType<ProductCategoryInfoEntity> {
    return getModelForClass(ProductCategoryInfoEntity, {
      schemaOptions: { ...schemaOptions, collection: 'product_category_infos' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
