import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { SkuEntity } from './sku.entity';

@Exclude()
export class InventoryEntity extends BaseEntity {
  @Expose()
  @prop({ required: true, index: true })
  public name: string;

  @Expose()
  @prop({ default: [] })
  skus: SkuEntity[];

  @Expose()
  @prop({ default: 0 })
  public vat: number;

  static get model(): ModelType<InventoryEntity> {
    return getModelForClass(InventoryEntity, {
      schemaOptions: { ...schemaOptions, collection: 'inventories' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
