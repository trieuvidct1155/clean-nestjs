import { LANG } from '@/domain/lang/lang.enum';
import { getModelForClass, prop, Ref } from '@typegoose/typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Exclude } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { CountryOriginEntity } from './countryOrigin.entity';

export class CountryOriginInfoSeo {
  //SEO
  @prop({})
  seo_title: string;

  @prop({})
  seo_description: string;

  @prop({ default: null })
  seo_keywords: string;

  @prop({ default: null })
  seo_schema: number;

  @prop({ default: null })
  seo_canonical: number;

  @prop({ default: null })
  seo_redirect: number;

  @prop({ default: null })
  seo_lang: number;
}

@Exclude()
export class CountryOriginInfoEntity extends BaseEntity {
  @prop({})
  name: string;

  @prop({})
  description: string;

  @prop({})
  content: string;

  @prop({})
  specialName: string;

  @prop({})
  seoH1: string;

  @prop({})
  slug: string;

  @prop({ required: true, default: LANG.VI, enum: LANG })
  lang: LANG;

  @prop({ ref: () => CountryOriginEntity, required: true })
  countryOrigin: Ref<CountryOriginEntity>;

  @prop({ type: () => CountryOriginInfoSeo })
  seo: CountryOriginInfoSeo;

  static get model(): ModelType<CountryOriginInfoEntity> {
    return getModelForClass(CountryOriginInfoEntity, {
      schemaOptions: { ...schemaOptions, collection: 'country_origin_infos' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
