import { LANG } from '@/domain/lang/lang.enum';
import { getModelForClass, prop, Ref } from '@typegoose/typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Exclude } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { CountryOriginEntity } from './countryOrigin.entity';
import { ImageEntity } from './image.entity';
import { ProductCategoryEntity } from './productCategory.entity';

export class ProductBrandSeo {
  //SEO
  @prop({})
  seo_title: string;

  @prop({})
  seo_description: string;

  @prop({ default: null })
  seo_keywords: string;

  @prop({ default: null })
  seo_schema: number;

  @prop({ default: null })
  seo_canonical: number;

  @prop({ default: null })
  seo_redirect: number;

  @prop({ default: null })
  seo_lang: number;
}

@Exclude()
export class ProductBrandEntity extends BaseEntity {
  @prop({})
  name: string;

  @prop({})
  specialName: string;

  @prop({ required: true, default: LANG.VI, enum: LANG })
  lang: LANG;

  @prop({})
  slug: string;

  @prop({ ref: () => ImageEntity, required: true })
  thumbnail: Ref<ImageEntity>;

  @prop({ ref: () => ImageEntity, required: true })
  banner: Ref<ImageEntity>;

  @prop({ ref: () => CountryOriginEntity, required: true })
  origin: Ref<CountryOriginEntity>;

  @prop({ ref: () => ProductCategoryEntity, required: true })
  category: Ref<ProductCategoryEntity>;

  @prop({ ref: () => ProductCategoryEntity, required: true })
  categories: Ref<ProductCategoryEntity>[];

  @prop({})
  states: string;

  @prop({})
  grape: string;

  @prop({ default: [], allowMixed: 0 })
  relatedPosts: string[];

  @prop({})
  description: string;

  @prop({})
  content: string;

  @prop({ unique: true })
  erpCode: string;

  @prop({ type: () => ProductBrandSeo })
  seo: ProductBrandSeo;

  static get model(): ModelType<ProductBrandEntity> {
    return getModelForClass(ProductBrandEntity, {
      schemaOptions: { ...schemaOptions, collection: 'product_brands' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
