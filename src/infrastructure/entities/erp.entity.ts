import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';

// * 	1 - Admin
// * 	2 - Sales
// *    3 - ....
// * 	4 - ....
// *    5 - ....

export enum AUTH_ROLES {
  ADMIN = '0',
  SALE = '1',
  EMPLOYEE = '2',
}

@Exclude()
export class ErpAdminEntity extends BaseEntity {
  @prop({ required: true, index: true, minlength: 6, maxlength: 100 })
  public username: string;

  @prop({ required: true, minlength: 6, maxlength: 100 })
  public password: string;

  @prop({})
  public facebookID: string;

  @prop({ required: true })
  public name: string;

  @prop({})
  public email: string;

  @prop({ require: true, default: 1, enum: AUTH_ROLES })
  public roles: AUTH_ROLES;

  @prop({ default: 1 })
  public gender: number;

  @prop({ default: '' })
  public phone: string;

  static get model(): ModelType<ErpAdminEntity> {
    return getModelForClass(ErpAdminEntity, {
      schemaOptions: { ...schemaOptions, collection: 'erp-connects' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
