import { getModelForClass, index, prop } from '@typegoose/typegoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';

@Exclude()
@index({ name: 'text', slug: 'text' })
@index({ slug: 'text' })
export class ImageEntity extends BaseEntity {
  @Expose()
  @prop({ required: true })
  public name: string;

  @Expose()
  @prop({ required: true, unique: true })
  public slug: string;

  @Expose()
  @prop({ default: null })
  public alt: string;

  @Expose()
  @prop({ default: null })
  public type: string;

  static get model(): ModelType<ImageEntity> {
    return getModelForClass(ImageEntity, {
      schemaOptions: { ...schemaOptions, collection: 'images' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
