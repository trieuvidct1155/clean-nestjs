import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';

@Exclude()
export class UserEntity extends BaseEntity {
  @prop({ required: true, index: true })
  public name: string;

  @prop({})
  public phone: string;

  @prop({})
  email: string;

  @prop({})
  device: string;

  // 1 nam, 0 nữ
  @prop({ default: 1 })
  gender: number;

  @Expose()
  @prop({ default: 0 })
  dateOfBirth: number;

  @Expose()
  @prop({ default: 0 })
  monthOfBirth: number;

  @Expose()
  @prop({ default: 0 })
  yearOfBirth: number;

  static get model(): ModelType<UserEntity> {
    return getModelForClass(UserEntity, {
      schemaOptions: { ...schemaOptions, collection: 'users' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
