import { getModelForClass, Index, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { ProductEntity } from '@entities/product.entity';
import { LANG } from '@/domain/lang/lang.enum';

@Exclude()
export class PromotionEntity extends BaseEntity {
  @Expose()
  @prop({})
  public code: string;

  @Expose()
  @prop({})
  public name: string;

  @Expose()
  @prop({})
  public description: string;

  @Expose()
  @prop({})
  public icon: string;

  @Expose()
  @prop()
  price: number;

  @Expose()
  @prop()
  minOrder: number;

  @Expose()
  @prop()
  type: number;

  @prop({ required: true, default: LANG.VI, enum: LANG })
  lang: LANG;

  @prop({ default: new Date() })
  dateExpired: Date;

  @prop({ default: 0 })
  status: Number;

  @Expose()
  @prop({})
  public erpCode: string;

  static get model(): ModelType<PromotionEntity> {
    return getModelForClass(PromotionEntity, {
      schemaOptions: { ...schemaOptions, collection: 'promotions' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
