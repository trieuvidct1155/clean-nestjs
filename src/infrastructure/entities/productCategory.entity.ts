import { getModelForClass, Index, prop } from '@typegoose/typegoose';
import { ModelType, Ref } from '@typegoose/typegoose/lib/types';
import { Exclude, Expose } from 'class-transformer';
import { BaseEntity, schemaOptions } from './base.entity';
import { LANG } from '@/domain/lang/lang.enum';
import { ProductCategoryInfoEntity } from './productCategoryInfo.entity';
import { ImageEntity } from './image.entity';
import { ProductBrandEntity } from './productBrand.entity';
import { ProductEntity } from './product.entity';

@Exclude()
export class ProductCategoryEntity extends BaseEntity {
  @Expose()
  @prop({
    ref: () => ProductCategoryInfoEntity,
    required: false,
    default: null,
  })
  public vi: Ref<ProductCategoryInfoEntity>;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public viName: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public viSlug: string;

  @Expose()
  @prop({
    ref: () => ProductCategoryInfoEntity,
    required: false,
    default: null,
  })
  public en: Ref<ProductCategoryInfoEntity>;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public enName: string;

  @Expose()
  @prop({ trim: true, index: true, default: null })
  public enSlug: string;

  ///////////////////
  @prop({ ref: () => ImageEntity, required: true })
  thumbnail: Ref<ImageEntity>;

  @prop({ ref: () => ImageEntity, required: true })
  banner: Ref<ImageEntity>;

  @prop({ ref: () => ImageEntity, required: true })
  icon: Ref<ImageEntity>;

  @prop()
  isShowMenu: boolean;

  @prop()
  isShowMap: number;

  @prop()
  order: number;

  @prop()
  parentID: string;

  @prop()
  id: string;

  @Expose()
  @prop({ unique: true })
  erpCode: string;

  @prop({ ref: () => ProductBrandEntity, required: true })
  productBrands: Ref<ProductBrandEntity>[];

  @Expose()
  @prop({ default: [], allowMixed: 0 })
  productStates: string[];

  @Expose()
  @prop({ default: [], allowMixed: 0 })
  typeProducts: string[];

  @Expose()
  @prop({ default: [], allowMixed: 0 })
  grapes: string[];

  @Expose()
  @prop({ ref: () => ProductEntity, required: true })
  categories: Ref<ProductEntity>[];

  @Expose()
  @prop({ default: [], allowMixed: 0 })
  relatedPosts: string[];

  @Expose()
  @prop({ ref: () => ProductCategoryEntity, required: true })
  parent: Ref<ProductCategoryEntity>;

  @Expose()
  @prop({ ref: () => ProductCategoryEntity, required: true })
  displayCategories: Ref<ProductCategoryEntity>[];

  @Expose()
  @prop({ ref: () => ProductCategoryEntity, required: true })
  displayMenu: Ref<ProductCategoryEntity>[];

  static get model(): ModelType<ProductCategoryEntity> {
    return getModelForClass(ProductCategoryEntity, {
      schemaOptions: { ...schemaOptions, collection: 'product_categories' },
    });
  }

  static get modelName(): string {
    return this.model.modelName;
  }
}
