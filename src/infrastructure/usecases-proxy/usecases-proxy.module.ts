import { ProviderUseCasesProxy } from '@/usecases';
import { DynamicModule, Module } from '@nestjs/common';
import { RepositoryModule } from '@repositories/repository.module';
import { SlackModule } from '../services/slack/slack.module';

@Module({
  imports: [RepositoryModule, SlackModule],
})
export class UseCasesProxyModule {
  static register(): DynamicModule {
    const { providers, exports } = ProviderUseCasesProxy.register();
    return {
      module: UseCasesProxyModule,
      providers: providers,
      exports: exports,
    };
  }
}
