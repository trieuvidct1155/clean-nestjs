import {
  IException,
  IFormatExceptionMessage,
} from '@domain/exceptions/exception.interface';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

@Injectable()
export class ExceptionsService implements IException {
  BadRequestException(data: IFormatExceptionMessage): void {
    throw new HttpException(
      {
        error: true,
        data: null,
        message: data.message,
      },
      HttpStatus.BAD_REQUEST,
    );
  }
  InternalServerErrorException(data?: IFormatExceptionMessage): void {
    throw new HttpException(
      {
        error: true,
        data: null,
        message: data.message,
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
  ForbiddenException(data?: IFormatExceptionMessage): void {
    throw new HttpException(
      {
        error: true,
        data: null,
        message: data.message,
      },
      HttpStatus.FORBIDDEN,
    );
  }
  UnauthorizedException(data?: IFormatExceptionMessage): void {
    throw new HttpException(
      {
        error: true,
        data: null,
        message: data.message,
      },
      HttpStatus.UNAUTHORIZED,
    );
  }
}
