import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { CreateProductBrandDTO } from './create.dto';

export class CreateListProductBrandDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateProductBrandDTO)
  @ApiProperty({ type: [CreateProductBrandDTO] })
  brands: Array<CreateProductBrandDTO>;
}
