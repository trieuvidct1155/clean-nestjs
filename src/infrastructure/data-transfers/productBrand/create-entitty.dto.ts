import { ELang } from '@/domain/decorators/lang.decorator';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateProductBrandEntityDTO {
  constructor(payload) {
    Object.assign(this, payload);
    Object.assign(this, { lang: ELang.VI });
  }

  @IsString()
  name: string;

  @IsString()
  erpCode: string;

  @ApiProperty({ required: false })
  @IsEnum(ELang)
  @IsOptional()
  lang: ELang = ELang.VI;
}
