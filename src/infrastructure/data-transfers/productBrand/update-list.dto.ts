import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { UpdateProductBrandDTO } from './update.dto';

export class UpdateListProductBrandDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateProductBrandDTO)
  @ApiProperty({ type: [UpdateProductBrandDTO] })
  brands: Array<UpdateProductBrandDTO>;
}
