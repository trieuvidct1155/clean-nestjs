import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { CreateCategoryDTO } from './create.dto';

export class CreateListCategoryDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateCategoryDTO)
  @ApiProperty({ type: [CreateCategoryDTO] })
  categories: Array<CreateCategoryDTO>;
}
