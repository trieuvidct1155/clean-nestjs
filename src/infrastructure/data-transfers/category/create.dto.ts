import { ELang } from '@/domain/decorators/lang.decorator';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';

export class CreateCategoryDTO {
  constructor(payload) {
    Object.assign(this, payload);
    Object.assign(this, { lang: ELang.VI });
  }

  @ApiProperty({ required: true })
  @IsString()
  category_code: string;

  @ApiProperty({ required: false })
  @IsString()
  name: string;

  @ApiProperty({ required: false })
  @IsEnum(ELang)
  @IsOptional()
  lang: ELang = ELang.VI;
}
