import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { UpdateCategoryDTO } from './update.dto';

export class UpdateListCategoryDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateCategoryDTO)
  @ApiProperty({ type: [UpdateCategoryDTO] })
  categories: Array<UpdateCategoryDTO>;
}
