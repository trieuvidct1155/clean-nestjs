import { CreateCategoryDTO } from './create.dto';

export class UpdateCategoryDTO extends CreateCategoryDTO {}
