import { IsNumber, IsOptional, IsString, Max, MaxLength, Min, MinLength } from 'class-validator';

export class LoginErpConnectDTO {
    @MinLength(6)
    @MaxLength(100)
    @IsString()
    username: string;

    @MinLength(6)
    @MaxLength(100)
    @IsString()
    password: string;


}
