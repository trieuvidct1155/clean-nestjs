import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { CreateCountryOriginDTO } from './create.dto';

export class CreateListCountryOriginDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateCountryOriginDTO)
  @ApiProperty({ type: [CreateCountryOriginDTO] })
  origins: Array<CreateCountryOriginDTO>;
}
