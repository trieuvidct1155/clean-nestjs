import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { UpdateCountryOriginDTO } from './update.dto';

export class UpdateListCountryOriginDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateCountryOriginDTO)
  @ApiProperty({ type: [UpdateCountryOriginDTO] })
  origins: Array<UpdateCountryOriginDTO>;
}
