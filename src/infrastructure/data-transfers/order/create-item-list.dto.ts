import { IsNumber, IsString } from 'class-validator';

export class CreateListOrderItemDTO {
  @IsString()
  product_code: string;

  @IsNumber()
  quantities: number;

  @IsNumber()
  price: number;

  @IsNumber()
  totalPrice: number;
}
