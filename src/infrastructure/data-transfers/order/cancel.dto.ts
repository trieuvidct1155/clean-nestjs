import {
  IsArray,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export class CancelOrderEntityDTO {
  @IsString()
  id_order: string;

  @IsString()
  id_customer: string;

  @IsString()
  id_reason: string;

  @IsString()
  id_store: string;
}
