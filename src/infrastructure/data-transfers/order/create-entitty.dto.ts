import { ELang } from '@/domain/decorators/lang.decorator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { CreateListOrderItemDTO } from './create-item-list.dto';

export class CreateOrderEntityDTO {
  @IsString()
  id_order: string;

  @IsString()
  id_customer: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateListOrderItemDTO)
  @ApiProperty({ type: [CreateListOrderItemDTO] })
  order_items: Array<CreateListOrderItemDTO>;

  @IsNumber()
  totalAmount: number;

  @IsNumber()
  totalAmountTemporary: number;

  @IsString()
  contactPay: string;

  @IsOptional()
  @IsString()
  id_promotion: string;

  @IsOptional()
  @IsString()
  creator: string;

  @IsString()
  staffName: string;

  @IsString()
  id_store: string;
}
