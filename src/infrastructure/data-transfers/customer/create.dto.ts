import { IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateCustomerDTO {
  //   @IsOptional()
  @IsString()
  name: string;

  @IsString()
  phone: string;

  @IsString()
  address: string;

  @IsString()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  name_district: string;

  @IsString()
  @IsOptional()
  name_ward: string;

  @IsString()
  @IsOptional()
  name_province: string;

  @IsString()
  id_customer: string;

  @IsOptional()
  @IsNumber({})
  lang: number;
}
