import { IsAlpha, IsNumber, IsOptional, IsString } from 'class-validator';

export class ParamUpdateCustomerDTO {
  id_customer: any;
}

export class UpdateCustomerDTO extends ParamUpdateCustomerDTO {
  //   @IsOptional()
  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  phone: string;

  @IsString()
  @IsOptional()
  address: string;

  @IsString()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  name_district: string;

  @IsString()
  @IsOptional()
  name_ward: string;

  @IsString()
  @IsOptional()
  name_province: string;

  @IsOptional()
  @IsNumber({})
  lang: number;
}
