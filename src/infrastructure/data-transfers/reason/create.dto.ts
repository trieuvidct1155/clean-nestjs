import { IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateReasonDTO {
  @IsString()
  name: string;

  @IsString()
  reason_code: string;

  @IsOptional()
  @IsNumber({})
  lang: number;
}
