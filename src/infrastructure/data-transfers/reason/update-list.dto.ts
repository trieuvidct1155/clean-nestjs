import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { UpdateReasonDTO } from './update.dto';

export class UpdateListReasonDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateReasonDTO)
  @ApiProperty({ type: [UpdateReasonDTO] })
  reasons: Array<UpdateReasonDTO>;
}
