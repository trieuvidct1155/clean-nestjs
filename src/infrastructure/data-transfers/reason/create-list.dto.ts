import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { CreateReasonDTO } from './create.dto';

export class CreateListReasonDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateReasonDTO)
  @ApiProperty({ type: [CreateReasonDTO] })
  reasons: Array<CreateReasonDTO>;
}
