import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsNotEmpty, IsString } from 'class-validator';

export class DeleteImagesDTO {
  @ApiProperty({ required: false })
  @IsNotEmpty({ message: 'The field "ids" is required', each: true })
  @ArrayNotEmpty()
  @IsString({ each: true })
  ids: Array<string>;
}
