import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateImagesDTO {
  @ApiProperty({ required: true })
  @IsString()
  name: string;

  @ApiProperty({ required: false })
  @IsString()
  src: string;

  @ApiProperty({ required: false })
  @IsString()
  alt: string;

  @ApiProperty({ required: false })
  @IsString()
  type: string;

  @ApiProperty({ required: false })
  @IsString()
  id: string;
}
