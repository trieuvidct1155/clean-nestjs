import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateImagesDTO {
  @ApiProperty({ required: true })
  @IsString()
  name: string;

  @ApiProperty({ required: false })
  @IsString()
  src: string;

  @ApiProperty({ required: false })
  @IsString()
  alt: string;

  @ApiProperty({ required: false })
  @IsString()
  type: string;
}
