import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { CreateProductDTO } from './create.dto';

export class CreateListProductDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateProductDTO)
  @ApiProperty({ type: [CreateProductDTO] })
  products: Array<CreateProductDTO>;
}
