import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { UpdateProductDTO } from './update.dto';

export class UpdateListProductDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateProductDTO)
  @ApiProperty({ type: [UpdateProductDTO] })
  products: Array<UpdateProductDTO>;
}
