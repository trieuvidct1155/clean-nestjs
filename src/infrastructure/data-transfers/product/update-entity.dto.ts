import { IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateProductEntityDTO {
  @IsString()
  barCode: string;

  @IsNumber()
  weight: number;

  @IsString()
  product_date: Date;

  @IsNumber()
  abv: number;

  @IsNumber()
  volume: number;

  @IsString()
  item_code: string;

  @IsOptional()
  @IsNumber()
  quantities_change: number;

  @IsNumber()
  price: number;

  @IsString()
  sku: string;

  @IsOptional()
  @IsNumber({})
  lang: number;
}
