import { IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateProductEntityDTO {
  @IsOptional()
  @IsString()
  name: string;

  @IsString()
  barCode: string;

  @IsNumber()
  weight: number;

  @IsString()
  brand_code: string;

  @IsString()
  product_date: Date;

  @IsString()
  origin_code: string;

  @IsString()
  category_code: string;

  @IsNumber()
  abv: number;

  @IsNumber()
  volume: number;

  @IsString()
  item_code: string;

  @IsNumber()
  quantities: number;

  @IsNumber()
  price: number;

  @IsString()
  sku: string;

  @IsOptional()
  @IsNumber({})
  lang: number;
}
