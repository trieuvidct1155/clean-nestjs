import { IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateProductDTO {
  @IsOptional()
  @IsString()
  name: string;

  @IsString()
  bar_code: string;

  @IsNumber()
  weight: number;

  @IsOptional()
  @IsString()
  brand_code: string;

  @IsString()
  product_date: Date;

  @IsOptional()
  @IsString()
  origin_code: string;

  @IsOptional()
  @IsString()
  category_code: string;

  @IsNumber()
  abv: number;

  @IsNumber()
  volume: number;

  @IsString()
  item_code: string;

  @IsOptional()
  @IsNumber()
  quantities: number;

  @IsNumber()
  price: number;

  @IsString()
  sku: string;

  @IsOptional()
  @IsNumber({})
  lang: number;
}
