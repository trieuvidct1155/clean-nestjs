import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { DeleteProductDTO } from './delete.dto';

export class DeleteListProductDTO {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => DeleteProductDTO)
  @ApiProperty({ type: [DeleteProductDTO] })
  products: Array<DeleteProductDTO>;
}
