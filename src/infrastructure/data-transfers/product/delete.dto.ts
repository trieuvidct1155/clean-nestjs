import { IsNumber, IsOptional, IsString } from 'class-validator';

export class DeleteProductDTO {
  @IsString()
  item_code: string;
}
