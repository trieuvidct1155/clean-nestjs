import { IsNotEmpty, IsString } from 'class-validator';

export class ReturnData<T> {
  @IsNotEmpty()
  error: boolean;

  @IsNotEmpty()
  data: T;

  @IsNotEmpty()
  @IsString()
  message: string;
}
