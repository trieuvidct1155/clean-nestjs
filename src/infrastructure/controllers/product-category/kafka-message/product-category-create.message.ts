import { ELang } from '@/domain/decorators/lang.decorator';
import { CreateCategoryDTO } from '@/infrastructure/data-transfers/category/create.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageCategoryCreate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<CreateCategoryDTO>) {
    const topic = 'erp-create-product-category';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { category_code, lang = ELang.VI, ...rest } = item;
      return {
        erpCode: category_code,
        lang,
        ...rest,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
