import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CATEGORY_UC_PROXY_TYPES } from '@/usecases/modules/categories/category-uc-proxy.type';
import { UpdateCategoriesUseCase } from '@/usecases/modules/categories/update-category-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class CategoryUpdateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(CATEGORY_UC_PROXY_TYPES.UPDATE_CATEGORY_UC_PROXY)
    private readonly updateCategoryUseCase: UseCaseProxy<UpdateCategoriesUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-update-product-category',
      { topic: 'erp-update-product-category' },
      {
        eachMessage: async ({ message }) => {
          const payload = JSON.parse(message.value.toString());
          await this.updateCategoryUseCase.getInstance().execute(payload);
        },
      },
    );
  }
}
