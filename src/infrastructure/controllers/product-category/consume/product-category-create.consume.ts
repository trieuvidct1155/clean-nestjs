import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CATEGORY_UC_PROXY_TYPES } from '@/usecases/modules/categories/category-uc-proxy.type';
import { CreateCategoriesUseCase } from '@/usecases/modules/categories/create-category-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class CategoryCreateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(CATEGORY_UC_PROXY_TYPES.CREATE_CATEGORY_UC_PROXY)
    private readonly createCategoriesUseCase: UseCaseProxy<CreateCategoriesUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-product-category',
      { topic: 'erp-create-product-category' },
      {
        eachMessage: async ({ message }) => {
          const payloads = JSON.parse(message.value.toString());
          await this.createCategoriesUseCase.getInstance().execute(payloads);
        },
      },
    );
  }
}
