import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import { Body, Controller, Inject, Post, Put, UseGuards } from '@nestjs/common';
import { LoggerService } from '../../logger/logger.service';
import { Language } from '../../common/decorators/lang.decorator';
import { ELang } from '@/domain/decorators/lang.decorator';
import { CreateListCategoryDTO } from '../../data-transfers/category/create-list.dto';
import { KafkaProducerService } from '@/infrastructure/services/kafka/kafka-producer.service';
import { KafkaMessage } from '@/infrastructure/services/kafka/kafka-message.service';
import { KafkaMessageCategoryCreate } from './kafka-message/product-category-create.message';
import { KafkaMessageCategoryUpdate } from './kafka-message/product-category-update.message';
import { UpdateListCategoryDTO } from '@/infrastructure/data-transfers/category/update-list.dto';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwtAuth.guard';

@UseGuards(JwtAuthGuard)
@Controller('/erp/category')
export class ProductCategoryController {
  constructor(
    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(LoggerService)
    private readonly loggerService: LoggerService,

    @Inject(KafkaProducerService)
    private readonly kafkaService: KafkaProducerService,
  ) { }

  @Post()
  async createProductCate(
    @Language() _lang: ELang,
    @Body() body: CreateListCategoryDTO,
  ): Promise<any> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageCategoryCreate(body.categories))
          .record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Put()
  async updateProductCate(
    @Language() _lang: ELang,
    @Body() body: UpdateListCategoryDTO,
  ): Promise<boolean> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageCategoryUpdate(body.categories))
          .record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }
}
