import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import {
  Body,
  Controller,
  Get,
  Inject,
  Post,
} from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';
import { JwtService } from '@nestjs/jwt';
import { ADMIN_UC_PROXY_TYPES } from '@/usecases/modules/admins/admin-uc-proxy.type';
import { LoginAdminUseCase } from '@/usecases/modules/admins/login-admin-proxy.service';
import { ReturnData } from '@/infrastructure/data-transfers/return-data/return-data.present';
import { RegisterAdminUseCase } from '@/usecases/modules/admins/register-admin-proxy.service';
import { LoginErpConnectDTO } from '@/infrastructure/data-transfers/erp-connect/login.dto';

@Controller('erp')
export class AdminController {
  constructor(
    @Inject(JwtService)
    private jwtService: JwtService,

    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(ADMIN_UC_PROXY_TYPES.LOGIN_ADMIN_USECASE_PROXY)
    private readonly loginUseCaseProxy: UseCaseProxy<LoginAdminUseCase>,

    @Inject(ADMIN_UC_PROXY_TYPES.RESGISTER_ADMIN_USECASE_PROXY)
    private readonly registerUseCaseProxy: UseCaseProxy<RegisterAdminUseCase>,
  ) { }

  @Get('login')
  async getAdmin(): Promise<ReturnData<any>> {
    try {
      const payload = {
        username: process.env.USERNAME_ERP,
        password: process.env.PASSWORD_ERP,
        name: process.env.NAME_ERP,
      };
      return this.registerUseCaseProxy.getInstance().execute(payload);
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Post('/login')
  async login(@Body() body: LoginErpConnectDTO): Promise<ReturnData<any>> {
    try {
      const result = await this.loginUseCaseProxy.getInstance().execute(body);
      if (result) {
        const token = this.jwtService.sign(result);
        return {
          ...result,
          token,
        }
      }
      return result;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }
}
