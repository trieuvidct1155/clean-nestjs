import { ELang } from '@/domain/decorators/lang.decorator';
import { CreateReasonDTO } from '@/infrastructure/data-transfers/reason/create.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageReasonCreate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<CreateReasonDTO>) {
    const topic = 'erp-create-reason';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { reason_code, lang = ELang.VI, ...rest } = item;
      return {
        erpCode: reason_code,
        lang,
        ...rest,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
