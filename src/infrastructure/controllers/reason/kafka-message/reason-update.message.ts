import { ELang } from './../../../../domain/decorators/lang.decorator';
import { UpdateReasonDTO } from '@/infrastructure/data-transfers/reason/update.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageReasonUpdate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<UpdateReasonDTO>) {
    const topic = 'erp-update-reason';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { reason_code, lang = ELang.VI, ...rest } = item;
      return {
        conditions: { erpCode: reason_code },
        dataUpdate: { ...rest, lang },
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
