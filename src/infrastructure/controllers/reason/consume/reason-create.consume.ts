import { ValidateVariable } from '@/infrastructure/helpers/validate-variable';
import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CreateReasonsUseCase } from '@/usecases/modules/reasons/create-reason-uc-proxy.service';
import { REASON_UC_PROXY_TYPES } from '@/usecases/modules/reasons/reason-uc-proxy.type';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class ReasonCreateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(REASON_UC_PROXY_TYPES.CREATE_REASON_UC_PROXY)
    private readonly createReasonsUseCase: UseCaseProxy<CreateReasonsUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-create',
      { topic: 'erp-create-reason' },
      {
        eachMessage: async ({ message }) => {
          let payloads = JSON.parse(message.value.toString());
          payloads = ValidateVariable.isArray(payloads) ? payloads : [payloads];
          await this.createReasonsUseCase.getInstance().execute(payloads);
        },
      },
    );
  }
}
