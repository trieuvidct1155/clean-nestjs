import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { REASON_UC_PROXY_TYPES } from '@/usecases/modules/reasons/reason-uc-proxy.type';
import { UpdateReasonsUseCase } from '@/usecases/modules/reasons/update-reason-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class ReasonUpdateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(REASON_UC_PROXY_TYPES.UPDATE_REASON_UC_PROXY)
    private readonly updateReasonsUseCase: UseCaseProxy<UpdateReasonsUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-update',
      { topic: 'erp-update-reason' },
      {
        eachMessage: async ({ message }) => {
          const payload = JSON.parse(message.value.toString());
          await this.updateReasonsUseCase.getInstance().execute(payload);
        },
      },
    );
  }
}
