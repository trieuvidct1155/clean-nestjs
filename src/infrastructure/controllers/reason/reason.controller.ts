import { JwtAuthGuard } from '@/infrastructure/common/guards/jwtAuth.guard';
import { CreateListReasonDTO } from '@/infrastructure/data-transfers/reason/create-list.dto';
import { UpdateListReasonDTO } from '@/infrastructure/data-transfers/reason/update-list.dto';
import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import { KafkaMessage } from '@/infrastructure/services/kafka/kafka-message.service';
import { LoggerService } from '@loggers/logger.service';
import { Body, Controller, Inject, Post, Put, UseGuards } from '@nestjs/common';
import { KafkaProducerService } from '@services/kafka/kafka-producer.service';
import { KafkaMessageReasonCreate } from './kafka-message/reason-create.message';
import { KafkaMessageReasonUpdate } from './kafka-message/reason-update.message';

@UseGuards(JwtAuthGuard)
@Controller('/erp/reason')
export class ReasonController {
  constructor(
    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(LoggerService)
    private readonly loggerService: LoggerService,

    @Inject(KafkaProducerService)
    private readonly kafkaService: KafkaProducerService,
  ) { }

  @Post()
  async createReason(@Body() body: CreateListReasonDTO): Promise<boolean> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageReasonCreate(body.reasons)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Put()
  async updateReason(@Body() body: UpdateListReasonDTO): Promise<boolean> {
    try {
      await this.kafkaService.send(
        new KafkaMessage(new KafkaMessageReasonUpdate(body.reasons)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }
}
