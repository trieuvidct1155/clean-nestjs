import { CreateImagesUseCase } from '../../usecases/modules/images/create-image-uc-proxy.service';
import { DIR_UPLOAD } from '@/domain/interceptors';
import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import { ReturnData } from '@data-transfers/return-data/return-data.present';
import { ImageEntity } from '@entities/image.entity';
import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Post,
  Put,
  Query,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { ImageInterceptor } from '@interceptors/image.Interceptor';
import { ValidateVariable } from '../helpers/validate-variable';
import { IMAGE_UC_PROXY_TYPES } from '../../usecases/modules/images/image-uc-proxy.type';
import { UseCaseProxy } from '../usecases-proxy/usecases-proxy';
import { SlugKenZy } from '../helpers/slug-kenzy';
import { GetListImagesUseCase } from '../../usecases/modules/images/get-list-image-uc-proxy.service';
import { Paging } from '@decorators/index';
import { IPaging } from '@/domain/decorators/paging.decorator';
import { DeleteImagesUseCase } from '../../usecases/modules/images/delete-image-uc-proxy.service';
import { DeleteImagesDTO } from '../data-transfers/image/delete-image.dto';
import { LoggerService } from '../logger/logger.service';

@Controller('images')
export class ImageController {
  constructor(
    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(LoggerService)
    private readonly loggerService: LoggerService,

    @Inject(IMAGE_UC_PROXY_TYPES.CREATE_IMAGE_UC_PROXY)
    private readonly createImageUCProxy: UseCaseProxy<CreateImagesUseCase>,

    @Inject(IMAGE_UC_PROXY_TYPES.GET_LIST_IMAGE_UC_PROXY)
    private readonly getListImageUCProxy: UseCaseProxy<GetListImagesUseCase>,

    @Inject(IMAGE_UC_PROXY_TYPES.DELETE_IMAGE_UC_PROXY)
    private readonly deleteImageUCProxy: UseCaseProxy<DeleteImagesUseCase>,
  ) {}

  @Get()
  async getList(
    @Query('search') search: string,
    @Paging({ limit: 20 }) paging: IPaging,
  ) {
    return this.getListImageUCProxy.getInstance().execute({
      search,
      paging,
    });
  }

  @Post()
  @UseInterceptors(
    new ImageInterceptor({ fileName: 'images', maxUpload: 10 }).exclude(),
  )
  async createImage(
    @UploadedFiles() files,
  ): Promise<ReturnData<ImageEntity[]>> {
    try {
      const isFilesEmpty = ValidateVariable.isArrayEmpty(files);
      if (isFilesEmpty)
        this.exceptionsService.BadRequestException({
          message: 'Vui lòng kiểm tra file.',
        });

      const payloads = files.map((item) => {
        return {
          name: item.filename,
          slug:
            DIR_UPLOAD.DOCS_STORAGE_DIR_SRC + SlugKenZy.create(item.filename),
          type: item.mimetype,
        };
      });

      return this.createImageUCProxy.getInstance().execute(payloads);
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Delete()
  async updateImageInfo(@Body() body: DeleteImagesDTO) {
    const { ids } = body;
    return this.deleteImageUCProxy.getInstance().execute({ ids });
  }
}
