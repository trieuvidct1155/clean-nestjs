import { Module } from '@nestjs/common';
import { EnvironmentConfigModule } from '../config/environment-config/environment-config.module';
import { EnvironmentConfigService } from '../config/environment-config/environment-config.service';
import { WinstonConfigModule } from '../config/winston-config/winston-config.module';
import { ExceptionsService } from '../exceptions/exceptions.service';
import { LoggerModule } from '../logger/logger.module';
import { LoggerService } from '../logger/logger.service';
import { KafkaModule } from '../services/kafka/kafka.module';
import { SlackModule } from '../services/slack/slack.module';
import { UseCasesProxyModule } from '../usecases-proxy/usecases-proxy.module';
import { CustomerController } from './customer/customer.controller';
import { ImageController } from './image.controller';
import { ProductCreateConsumer } from './product/consume/product-create.consume';
import { ProductUpdateConsumer } from './product/consume/product-update.consume';
import { ProductController } from './product/product.controller';
import { ProductDeleteConsumer } from './product/consume/product-delete.consume';
import { CustomerCreateConsumer } from './customer/consume/customer-create.consume';
import { CustomerUpdateConsumer } from './customer/consume/customer-update.consume';
import { ReasonController } from './reason/reason.controller';
import { ProductCategoryController } from './product-category/productCategory.controller';
import { ReasonCreateConsumer } from './reason/consume/reason-create.consume';
import { ReasonUpdateConsumer } from './reason/consume/reason-update.consume';
import { CategoryCreateConsumer } from './product-category/consume/product-category-create.consume';
import { CategoryUpdateConsumer } from './product-category/consume/product-category-update.consume';
import { OrderUpdateStatusConsumer } from './order/consume/order-update-status.consume';
import { OrderController } from './order/order.controller';
import { CountryOriginController } from './countryOrigin/country-origin.controller';
import { CountryOriginCreateConsumer } from './countryOrigin/consume/country-origin-create.consume';
import { CountryOriginUpdateConsumer } from './countryOrigin/consume/country-orgin-update.consume';
import { OrderCreateConsumer } from './order/consume/order-create.consume';
import { OrderCancelConsumer } from './order/consume/order-cancel.consume';
import { ProductBrandCreateConsumer } from './product-brand/consume/product-brand-create.consume';
import { ProductBrandController } from './product-brand/product-brand.controller';
import { CountryProductBrandConsumer } from './product-brand/consume/product-brand-update.consume';
import { AdminController } from './erp-connect/admin.controller';
import { JwtStrategy } from '../common/strategies/jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { LocalStrategy } from '../common/strategies/local.strategy';

const controllers = [
  ImageController,
  ReasonController,
  ProductCategoryController,
  CustomerController,
  CountryOriginController,
  ProductController,
  ProductBrandController,
  OrderController,
  AdminController,
];

const consumers = [
  ReasonCreateConsumer,
  ReasonUpdateConsumer,
  CategoryCreateConsumer,
  CategoryUpdateConsumer,
  ProductCreateConsumer,
  ProductUpdateConsumer,
  ProductDeleteConsumer,
  CustomerCreateConsumer,
  CustomerUpdateConsumer,

  OrderUpdateStatusConsumer,
  OrderCreateConsumer,
  OrderCancelConsumer,
  CountryOriginCreateConsumer,
  CountryOriginUpdateConsumer,

  ProductBrandCreateConsumer,
  CountryProductBrandConsumer,
];

@Module({
  imports: [
    EnvironmentConfigModule,
    WinstonConfigModule,
    SlackModule,
    LoggerModule,
    KafkaModule,
    UseCasesProxyModule.register(),
    JwtModule.registerAsync({
      imports: [EnvironmentConfigModule],
      inject: [EnvironmentConfigService],
      useFactory: (_envConfigService: EnvironmentConfigService) => {
        return {
          secret: _envConfigService.getJwtSecret(),
          signOptions: { expiresIn: 30 * 24 * 60 * 60 + 's' },
        };
      },
    }),
  ],
  controllers: [...controllers],
  providers: [
    LocalStrategy, JwtStrategy,
    ExceptionsService,
    EnvironmentConfigService,
    LoggerService,
    ...consumers,
  ],
})
export class ControllersModule { }
