import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import { Body, Controller, Inject, Param, Post, Put, UseGuards } from '@nestjs/common';
import { LoggerService } from '../../logger/logger.service';
import { KafkaMessage } from '@/infrastructure/services/kafka/kafka-message.service';
import { KafkaMessageOrderUpdateStatus } from './kajka-message/order-update-status.message';
import { KafkaProducerService } from '@/infrastructure/services/kafka/kafka-producer.service';
import { KafkaMessageOrderCreate } from './kajka-message/order-create.message';
import { KafkaMessageOrderCancel } from './kajka-message/order-cannel.message';
import { CreateOrderEntityDTO } from '@/infrastructure/data-transfers/order/create-entitty.dto';
import { CancelOrderEntityDTO } from '@/infrastructure/data-transfers/order/cancel.dto';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwtAuth.guard';

@UseGuards(JwtAuthGuard)
@Controller('/erp')
export class OrderController {
  constructor(
    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(LoggerService)
    private readonly loggerService: LoggerService,

    @Inject(KafkaProducerService)
    private readonly kafkaService: KafkaProducerService,
  ) { }

  @Post('createOrder')
  async createProductCate(@Body() body: CreateOrderEntityDTO): Promise<any> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageOrderCreate(body)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Put('/cancelOrder')
  async updateProductCate(@Body() body: CancelOrderEntityDTO): Promise<any> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageOrderCancel(body)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Post('orders/update-shipping-status')
  async updateStatusShipping(@Body() body: any): Promise<any> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageOrderUpdateStatus(body)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }
}
