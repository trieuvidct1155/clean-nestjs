import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageOrderCreate extends AbsKafkaMessage {
  constructor(private readonly mess: any) {
    const topic = 'erp-create-order';
    super(topic, mess);
  }

  private get cvtMessage() {
    const data = {
      ...this.mess,
    };
    return data;
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
