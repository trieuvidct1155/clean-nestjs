import { CancelOrderEntityDTO } from '@/infrastructure/data-transfers/order/cancel.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageOrderCancel extends AbsKafkaMessage {
  constructor(private readonly mess: CancelOrderEntityDTO) {
    const topic = 'erp-cancel-order';
    super(topic, mess);
  }

  private get cvtMessage() {
    const data = {
      ...this.mess,
    };
    return data;
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
