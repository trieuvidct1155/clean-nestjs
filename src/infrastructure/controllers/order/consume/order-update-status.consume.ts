import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { ORDER_UC_PROXY_TYPES } from '@/usecases/modules/orders/order-uc-proxy.type';
import { UpdateStatusShippingOrdersUseCase } from '@/usecases/modules/orders/update-status-shipping-order-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class OrderUpdateStatusConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(ORDER_UC_PROXY_TYPES.UPDATE_ORDER_STATUS_UC_PROXY)
    private readonly updateStatusShippingOrdersUseCase: UseCaseProxy<UpdateStatusShippingOrdersUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-update-status-order',
      { topic: 'erp-update-status-order' },
      {
        eachMessage: async ({ message }) => {
          const payload = JSON.parse(message.value.toString());
          const signalUpdate = await this.updateStatusShippingOrdersUseCase
            .getInstance()
            .execute(payload);

          if (!signalUpdate) {
            // Slack Module And Log Service
            // throw new Error('Error updating reason');
          }
        },
      },
    );
  }
}
