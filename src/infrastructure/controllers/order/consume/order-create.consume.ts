import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CreateOrdersUseCase } from '@/usecases/modules/orders/create-order-uc-proxy.service';
import { ORDER_UC_PROXY_TYPES } from '@/usecases/modules/orders/order-uc-proxy.type';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class OrderCreateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(ORDER_UC_PROXY_TYPES.CREATE_ORDER_UC_PROXY)
    private readonly createOrdersUseCase: UseCaseProxy<CreateOrdersUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-create-order',
      { topic: 'erp-create-order' },
      {
        eachMessage: async ({ message }) => {
          const payload = JSON.parse(message.value.toString());
          const signalUpdate = await this.createOrdersUseCase
            .getInstance()
            .execute(payload);

          if (!signalUpdate) {
            // Slack Module And Log Service
            // throw new Error('Error updating reason');
          }
        },
      },
    );
  }
}
