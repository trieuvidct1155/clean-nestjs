import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CancelOrdersUseCase } from '@/usecases/modules/orders/cancel-order-uc-proxy.service';
import { ORDER_UC_PROXY_TYPES } from '@/usecases/modules/orders/order-uc-proxy.type';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class OrderCancelConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(ORDER_UC_PROXY_TYPES.CANCEL_ORDER_UC_PROXY)
    private readonly cancelOrdersUseCase: UseCaseProxy<CancelOrdersUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-cancel-order',
      { topic: 'erp-cancel-order' },
      {
        eachMessage: async ({ message }) => {
          const payload = JSON.parse(message.value.toString());
          await this.cancelOrdersUseCase.getInstance().execute(payload);
        },
      },
    );
  }
}
