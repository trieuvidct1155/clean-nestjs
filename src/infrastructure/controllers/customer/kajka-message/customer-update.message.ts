import { UpdateCustomerDTO } from '@/infrastructure/data-transfers/customer/update.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageCustomerUpdate extends AbsKafkaMessage {
  constructor(private readonly mess: UpdateCustomerDTO) {
    const topic = 'erp-update-customer';
    super(topic, mess);
  }

  private get cvtMessage() {
    const data = {
      ...this.mess,
      erpDistrict: this.mess.name_district,
      erpWard: this.mess.name_ward,
      erpProvince: this.mess.name_province,
    };
    return data;
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
