import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import { Body, Controller, Inject, Param, Post, Put, UseGuards } from '@nestjs/common';
import { LoggerService } from '../../logger/logger.service';
import { CreateCustomerDTO } from '@/infrastructure/data-transfers/customer/create.dto';
import { KafkaMessage } from '@/infrastructure/services/kafka/kafka-message.service';
import { KafkaMessageCustomerCreate } from './kajka-message/customer-create.message';
import {
  ParamUpdateCustomerDTO,
  UpdateCustomerDTO,
} from '@/infrastructure/data-transfers/customer/update.dto';
import { KafkaMessageCustomerUpdate } from './kajka-message/customer-update.message';
import { KafkaProducerService } from '@/infrastructure/services/kafka/kafka-producer.service';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwtAuth.guard';

@UseGuards(JwtAuthGuard)
@Controller('/erp/customer')
export class CustomerController {
  constructor(
    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(LoggerService)
    private readonly loggerService: LoggerService,

    @Inject(KafkaProducerService)
    private readonly kafkaService: KafkaProducerService,
  ) { }

  @Post()
  async createProductCate(@Body() body: CreateCustomerDTO): Promise<any> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageCustomerCreate(body)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Put('/:id_customer')
  async updateProductCate(
    @Body() body: UpdateCustomerDTO,
    @Param() param: ParamUpdateCustomerDTO,
  ): Promise<any> {
    try {
      const payload = { ...body, ...param };
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageCustomerUpdate(payload)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }
}
