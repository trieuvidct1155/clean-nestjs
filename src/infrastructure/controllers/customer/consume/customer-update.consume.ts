import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CUSTOMER_UC_PROXY_TYPES } from '@/usecases/modules/customers/customer-uc-proxy.type';
import { UpdateCustomersUseCase } from '@/usecases/modules/customers/update-customer-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class CustomerUpdateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(CUSTOMER_UC_PROXY_TYPES.UPDATE_CUSTOMER_UC_PROXY)
    private readonly updateCustomersUseCase: UseCaseProxy<UpdateCustomersUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-update-customer',
      { topic: 'erp-update-customer' },
      {
        eachMessage: async ({ message }) => {
          const payload = JSON.parse(message.value.toString());
          await this.updateCustomersUseCase.getInstance().execute(payload);
        },
      },
    );
  }
}
