import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CreateCustomersUseCase } from '@/usecases/modules/customers/create-customer-uc-proxy.service';
import { CUSTOMER_UC_PROXY_TYPES } from '@/usecases/modules/customers/customer-uc-proxy.type';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class CustomerCreateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(CUSTOMER_UC_PROXY_TYPES.CREATE_CUSTOMER_UC_PROXY)
    private readonly createCustomersUseCase: UseCaseProxy<CreateCustomersUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-product-customer',
      { topic: 'erp-create-customer' },
      {
        eachMessage: async ({ message }) => {
          const payloads = JSON.parse(message.value.toString());
          const rs = await this.createCustomersUseCase
            .getInstance()
            .execute(payloads);
        },
      },
    );
  }
}
