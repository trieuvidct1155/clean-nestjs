import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CreateOriginsUseCase } from '@/usecases/modules/origins/create-origin-uc-proxy.service';
import { ORIGIN_UC_PROXY_TYPES } from '@/usecases/modules/origins/origin-uc-proxy.type';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class CountryOriginCreateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,

    @Inject(ORIGIN_UC_PROXY_TYPES.CREATE_ORIGIN_UC_PROXY)
    private readonly createOriginsUseCase: UseCaseProxy<CreateOriginsUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-create-origins',
      { topic: 'erp-create-origins' },
      {
        eachMessage: async ({ message }) => {
          const payloads = JSON.parse(message.value.toString());
          await this.createOriginsUseCase.getInstance().execute(payloads);
        },
      },
    );
  }
}
