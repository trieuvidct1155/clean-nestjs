import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { ORIGIN_UC_PROXY_TYPES } from '@/usecases/modules/origins/origin-uc-proxy.type';
import { UpdateOriginsUseCase } from '@/usecases/modules/origins/update-origin-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class CountryOriginUpdateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,

    @Inject(ORIGIN_UC_PROXY_TYPES.UPDATE_ORIGIN_UC_PROXY)
    private readonly UpdateOriginsUseCase: UseCaseProxy<UpdateOriginsUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-update-origins',
      { topic: 'erp-update-origins' },
      {
        eachMessage: async ({ message }) => {
          const payload = JSON.parse(message.value.toString());
          await this.UpdateOriginsUseCase.getInstance().execute(payload);
        },
      },
    );
  }
}
