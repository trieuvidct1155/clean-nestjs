import { ELang } from '@/domain/decorators/lang.decorator';
import { UpdateCountryOriginDTO } from '@/infrastructure/data-transfers/countryOrigin/update.dto';
import { UpdateReasonDTO } from '@/infrastructure/data-transfers/reason/update.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageCountryOriginUpdate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<UpdateCountryOriginDTO>) {
    const topic = 'erp-update-origins';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { origin_code, lang = ELang.VI, ...rest } = item;
      return {
        erpCode: origin_code,
        lang,
        ...rest,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
