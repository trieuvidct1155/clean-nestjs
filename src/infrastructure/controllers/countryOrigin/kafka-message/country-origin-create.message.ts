import { ELang } from '@/domain/decorators/lang.decorator';
import { CreateCountryOriginDTO } from '@/infrastructure/data-transfers/countryOrigin/create.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageCountryOriginCreate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<CreateCountryOriginDTO>) {
    const topic = 'erp-create-origins';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { origin_code, lang = ELang.VI, ...rest } = item;
      return {
        erpCode: origin_code,
        lang,
        ...rest,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
