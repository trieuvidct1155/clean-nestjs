import { ELang } from '@/domain/decorators/lang.decorator';
import { Language } from '@/infrastructure/common/decorators/lang.decorator';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwtAuth.guard';
import { CreateListCountryOriginDTO } from '@/infrastructure/data-transfers/countryOrigin/create-list.dto';
import { UpdateListCountryOriginDTO } from '@/infrastructure/data-transfers/countryOrigin/update-list.dto';
import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import { KafkaMessage } from '@/infrastructure/services/kafka/kafka-message.service';
import { LoggerService } from '@loggers/logger.service';
import { Body, Controller, Inject, Post, Put, UseGuards } from '@nestjs/common';
import { KafkaProducerService } from '@services/kafka/kafka-producer.service';
import { KafkaMessageCountryOriginCreate } from './kafka-message/country-origin-create.message';
import { KafkaMessageCountryOriginUpdate } from './kafka-message/country-origin-update.message';

@UseGuards(JwtAuthGuard)
@Controller('/erp/origins')
export class CountryOriginController {
  constructor(
    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(LoggerService)
    private readonly loggerService: LoggerService,

    @Inject(KafkaProducerService)
    private readonly kafkaService: KafkaProducerService,
  ) { }

  @Post()
  async createProductCate(
    @Language() _lang: ELang,
    @Body() body: CreateListCountryOriginDTO,
  ): Promise<boolean> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageCountryOriginCreate(body.origins))
          .record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Put()
  async updateProductCate(
    @Language() _lang: ELang,
    @Body() body: UpdateListCountryOriginDTO,
  ): Promise<boolean> {
    try {
      const rs = await this.kafkaService.send(
        new KafkaMessage(new KafkaMessageCountryOriginUpdate(body.origins))
          .record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }
}
