import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { PRODUCT_BRAND_UC_PROXY_TYPES } from '@/usecases/modules/brands/brand-uc-proxy.type';
import { CreateProductBrandsUseCase } from '@/usecases/modules/brands/create-brand-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class ProductBrandCreateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,

    @Inject(PRODUCT_BRAND_UC_PROXY_TYPES.CREATE_PRODUCT_BRAND_UC_PROXY)
    private readonly createProductBrandsUseCase: UseCaseProxy<CreateProductBrandsUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-create-brands',
      { topic: 'erp-create-brands' },
      {
        eachMessage: async ({ message }) => {
          const payloads = JSON.parse(message.value.toString());
          await this.createProductBrandsUseCase.getInstance().execute(payloads);
        },
      },
    );
  }
}
