import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { PRODUCT_BRAND_UC_PROXY_TYPES } from '@/usecases/modules/brands/brand-uc-proxy.type';
import { UpdateProductBrandsUseCaseProxy } from '@/usecases/modules/brands/update-brand-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class CountryProductBrandConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,

    @Inject(PRODUCT_BRAND_UC_PROXY_TYPES.UPDATE_PRODUCT_BRAND_UC_PROXY)
    private readonly updateProductBrandsUseCaseProxy: UseCaseProxy<UpdateProductBrandsUseCaseProxy>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-update-brands',
      { topic: 'erp-update-brands' },
      {
        eachMessage: async ({ message }) => {
          const payload = JSON.parse(message.value.toString());
          await this.updateProductBrandsUseCaseProxy
            .getInstance()
            .execute(payload);
        },
      },
    );
  }
}
