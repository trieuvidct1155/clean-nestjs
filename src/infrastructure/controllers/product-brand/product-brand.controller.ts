import { ELang } from '@/domain/decorators/lang.decorator';
import { Language } from '@/infrastructure/common/decorators/lang.decorator';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwtAuth.guard';
import { CreateListProductBrandDTO } from '@/infrastructure/data-transfers/productBrand/create-list.dto';
import { UpdateListProductBrandDTO } from '@/infrastructure/data-transfers/productBrand/update-list.dto';
import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import { KafkaMessage } from '@/infrastructure/services/kafka/kafka-message.service';
import { LoggerService } from '@loggers/logger.service';
import { Body, Controller, Inject, Post, Put, UseGuards } from '@nestjs/common';
import { KafkaProducerService } from '@services/kafka/kafka-producer.service';
import { KafkaMessageProductBrandCreate } from './kafka-message/product-brand-create.message';
import { KafkaMessageProductBrandUpdate } from './kafka-message/product-brand-update.message';

@UseGuards(JwtAuthGuard)
@Controller('/erp/brands')
export class ProductBrandController {
  constructor(
    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(LoggerService)
    private readonly loggerService: LoggerService,

    @Inject(KafkaProducerService)
    private readonly kafkaService: KafkaProducerService,
  ) { }

  @Post()
  async createProductBrand(
    @Language() _lang: ELang,
    @Body() body: CreateListProductBrandDTO,
  ): Promise<boolean> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageProductBrandCreate(body.brands))
          .record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Put()
  async updateProductCate(
    @Language() _lang: ELang,
    @Body() body: UpdateListProductBrandDTO,
  ): Promise<boolean> {
    try {
      const rs = await this.kafkaService.send(
        new KafkaMessage(new KafkaMessageProductBrandUpdate(body.brands))
          .record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }
}
