import { ELang } from '@/domain/decorators/lang.decorator';
import { UpdateProductBrandDTO } from '@/infrastructure/data-transfers/productBrand/update.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageProductBrandUpdate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<UpdateProductBrandDTO>) {
    const topic = 'erp-update-brands';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { brand_code, lang = ELang.VI, ...rest } = item;
      return {
        erpCode: brand_code,
        lang,
        ...rest,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
