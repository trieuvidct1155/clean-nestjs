import { ELang } from '@/domain/decorators/lang.decorator';
import { CreateProductBrandDTO } from '@/infrastructure/data-transfers/productBrand/create.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageProductBrandCreate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<CreateProductBrandDTO>) {
    const topic = 'erp-create-brands';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { brand_code, lang = ELang.VI, ...rest } = item;
      return {
        erpCode: brand_code,
        lang,
        ...rest,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
