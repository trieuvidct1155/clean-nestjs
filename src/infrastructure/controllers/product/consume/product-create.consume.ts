import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { CreateProductsUseCase } from '@/usecases/modules/products/create-product-uc-proxy.service';
import { PRODUCT_UC_PROXY_TYPES } from '@/usecases/modules/products/product-uc-proxy.type';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class ProductCreateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,

    @Inject(PRODUCT_UC_PROXY_TYPES.CREATE_PRODUCT_UC_PROXY)
    private readonly createProductsUseCase: UseCaseProxy<CreateProductsUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-create-product',
      { topic: 'erp-create-product' },
      {
        eachMessage: async ({ message }) => {
          try {
            const payloads = JSON.parse(message.value.toString());
            await this.createProductsUseCase.getInstance().execute(payloads);
          } catch (error) {
            new Error(error);
          }
        },
      },
    );
  }
}
