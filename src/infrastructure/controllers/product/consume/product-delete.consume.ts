import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { DeleteProductsUseCase } from '@/usecases/modules/products/delete-product-proxy.service';
import { PRODUCT_UC_PROXY_TYPES } from '@/usecases/modules/products/product-uc-proxy.type';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class ProductDeleteConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(PRODUCT_UC_PROXY_TYPES.DELETE_PRODUCT_UC_PROXY)
    private readonly deleteProductsUseCase: UseCaseProxy<DeleteProductsUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-delete-product',
      { topic: 'erp-delete-product' },
      {
        eachMessage: async ({ message }) => {
          try {
            const payload = JSON.parse(message.value.toString());
            const signalUpdate = await this.deleteProductsUseCase
              .getInstance()
              .execute(payload);

            if (!signalUpdate) {
              // Slack Module And Log Service
              // throw new Error('Error updating reason');
            }
          } catch (error) {
            new Error(error);
          }
        },
      },
    );
  }
}
