import { KafkaConsumerService } from '@/infrastructure/services/kafka/kafka-consumer.service';
import { PRODUCT_UC_PROXY_TYPES } from '@/usecases/modules/products/product-uc-proxy.type';
import { UpdateProductsUseCase } from '@/usecases/modules/products/update-product-uc-proxy.service';
import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { UseCaseProxy } from '@uc-proxy/usecases-proxy';

@Injectable()
export class ProductUpdateConsumer implements OnModuleInit {
  constructor(
    private readonly _consumer: KafkaConsumerService,
    @Inject(PRODUCT_UC_PROXY_TYPES.UPDATE_PRODUCT_UC_PROXY)
    private readonly updateProductsUseCase: UseCaseProxy<UpdateProductsUseCase>,
  ) {}

  async onModuleInit() {
    this._consumer.consume(
      'erp-update-product',
      { topic: 'erp-update-product' },
      {
        eachMessage: async ({ message }) => {
          try {
            const payload = JSON.parse(message.value.toString());
            const signalUpdate = await this.updateProductsUseCase
              .getInstance()
              .execute(payload);

            if (!signalUpdate) {
              // Slack Module And Log Service
              // throw new Error('Error updating reason');
            }
          } catch (error) {
            new Error(error);
          }
        },
      },
    );
  }
}
