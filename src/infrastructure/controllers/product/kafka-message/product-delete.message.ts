import { DeleteProductDTO } from '@/infrastructure/data-transfers/product/delete.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageProductDelete extends AbsKafkaMessage {
  constructor(private readonly mess: Array<DeleteProductDTO>) {
    const topic = 'erp-delete-product';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      return {
        ...item,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
