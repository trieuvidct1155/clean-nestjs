import { UpdateProductDTO } from '@/infrastructure/data-transfers/product/update.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageProductUpdate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<UpdateProductDTO>) {
    const topic = 'erp-update-product';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { bar_code, ...rest } = item;

      return {
        barCode: bar_code,
        ...rest,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
