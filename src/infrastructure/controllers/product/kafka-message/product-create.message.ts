import { CreateProductDTO } from '@/infrastructure/data-transfers/product/create.dto';
import { AbsKafkaMessage } from '@/infrastructure/services/kafka/abs-kafka-message.service';
import { ProducerRecord } from 'kafkajs';

export class KafkaMessageProductCreate extends AbsKafkaMessage {
  constructor(private readonly mess: Array<CreateProductDTO>) {
    const topic = 'erp-create-product';
    super(topic, mess);
  }

  private get cvtMessage() {
    return this.mess.map((item) => {
      const { bar_code, ...rest } = item;
      return {
        barCode: bar_code,
        ...rest,
      };
    });
  }

  get record(): ProducerRecord {
    return {
      topic: this.topic,
      messages: this.formatMessageByType(this.cvtMessage),
    };
  }
}
