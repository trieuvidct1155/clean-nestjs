import { ExceptionsService } from '@/infrastructure/exceptions/exceptions.service';
import { Body, Controller, Inject, Post, Put, Delete, UseGuards } from '@nestjs/common';
import { LoggerService } from '../../logger/logger.service';
import { KafkaProducerService } from '@/infrastructure/services/kafka/kafka-producer.service';
import { KafkaMessage } from '@/infrastructure/services/kafka/kafka-message.service';
import { KafkaMessageProductCreate } from './kafka-message/product-create.message';
import { CreateListProductDTO } from '@/infrastructure/data-transfers/product/create-list.dto';
import { UpdateListProductDTO } from '@/infrastructure/data-transfers/product/update-list.dto';
import { KafkaMessageProductUpdate } from './kafka-message/product-update.message';
import { DeleteListProductDTO } from '@/infrastructure/data-transfers/product/delete-list.dto';
import { KafkaMessageProductDelete } from './kafka-message/product-delete.message';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwtAuth.guard';

@UseGuards(JwtAuthGuard)
@Controller('/erp/products')
export class ProductController {
  constructor(
    @Inject(ExceptionsService)
    private readonly exceptionsService: ExceptionsService,

    @Inject(LoggerService)
    private readonly loggerService: LoggerService,

    @Inject(KafkaProducerService)
    private readonly kafkaService: KafkaProducerService,
  ) { }

  @Post()
  async createProduct(@Body() body: CreateListProductDTO): Promise<boolean> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageProductCreate(body.products)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Put()
  async updateProduct(@Body() body: UpdateListProductDTO): Promise<boolean> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageProductUpdate(body.products)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }

  @Delete()
  async deleteProduct(@Body() body: DeleteListProductDTO): Promise<boolean> {
    try {
      this.kafkaService.send(
        new KafkaMessage(new KafkaMessageProductDelete(body.products)).record,
      );
      return true;
    } catch (error) {
      this.exceptionsService.BadRequestException({ message: error.message });
    }
  }
}
