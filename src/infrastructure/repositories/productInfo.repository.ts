import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ProductInfoEntity } from '../entities/productInfo.entity';
import { BaseRepository } from './base.repository';

export class ProductInfoRepository extends BaseRepository<ProductInfoEntity> {
  constructor(
    @InjectModel(ProductInfoEntity.modelName)
    private readonly _modelRepository: ModelType<ProductInfoEntity>,
  ) {
    super(_modelRepository);
  }
}
