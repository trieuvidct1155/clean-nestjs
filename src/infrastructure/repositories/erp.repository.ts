import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ErpAdminEntity } from '../entities/erp.entity';
import { BaseRepository } from './base.repository';

export class ERPRepository extends BaseRepository<ErpAdminEntity> {
  constructor(
    @InjectModel(ErpAdminEntity.modelName)
    private readonly _modelRepository: ModelType<ErpAdminEntity>,
  ) {
    super(_modelRepository);
  }
}
