import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { StoreLocationEntity } from '../entities/storeLocation.entity';
import { BaseRepository } from './base.repository';

export class StoreLocationRepository extends BaseRepository<StoreLocationEntity> {
  constructor(
    @InjectModel(StoreLocationEntity.modelName)
    private readonly _modelRepository: ModelType<StoreLocationEntity>,
  ) {
    super(_modelRepository);
  }
}
