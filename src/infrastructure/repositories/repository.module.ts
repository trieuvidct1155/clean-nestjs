import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductEntity } from '@entities/product.entity';
import { ProductRepository } from '@repositories/product.repository';
import { ImageRepository } from '@repositories/image.repository';
import { ImageEntity } from '@entities/image.entity';
import { AdminRepository } from './admin.repository';
import { UserRepository } from './user.repository';
import { CountryOriginRepository } from './countryOrigin.repository';
import { CountryOriginInfoRepository } from './countryOriginInfo.repository';
import { CustomerRepository } from './customer.repository';
import { OrderRepository } from './order.repository';
import { OrderItemRepository } from './orderItem.repository';
import { ProductInfoRepository } from './productInfo.repository';
import { ProductBrandRepository } from './productBrand.repository';
import { ProductCategoryRepository } from './productCategory.repository';
import { ProductCategoryInfoRepository } from './productCategoryInfo.repository';
import { ReasonRepository } from './reason.repository';
import { ERPRepository } from './erp.repository';
import { AdminEntity } from '../entities/admin.entity';
import { UserEntity } from '../entities/user.entity';
import { CountryOriginEntity } from '../entities/countryOrigin.entity';
import { CountryOriginInfoEntity } from '../entities/countryOriginInfo.entity';
import { CustomerEntity } from '../entities/customer.entity';
import { OrderEntity } from '../entities/order.entity';
import { OrderItemEntity } from '../entities/orderItems.entity';
import { ProductInfoEntity } from '../entities/productInfo.entity';
import { ProductBrandEntity } from '../entities/productBrand.entity';
import { ProductCategoryEntity } from '../entities/productCategory.entity';
import { ProductCategoryInfoEntity } from '../entities/productCategoryInfo.entity';
import { ReasonEntity } from '../entities/reason.entity';
import { ErpAdminEntity } from '../entities/erp.entity';
import { AddressRepository } from './address.repository';
import { AddressEntity } from '../entities/address.entity';
import { ProductVariantRepository } from './productVariant.repository';
import { ProductVariantEntity } from '../entities/productVariant.entity';
import { PromotionRepository } from './promotion.repository';
import { LoggerRepository } from './logger.repostitory';
import { StoreLocationRepository } from './storeLocation.repository';
import { PromotionEntity } from '../entities/promotion.entity';
import { LoggerEntity } from '../entities/logger.entity';
import { StoreLocationEntity } from '../entities/storeLocation.entity';

const providers = [
  ProductRepository,
  ImageRepository,
  AdminRepository,
  UserRepository,
  CountryOriginRepository,
  CountryOriginInfoRepository,
  CustomerRepository,
  OrderRepository,
  OrderItemRepository,
  ProductInfoRepository,
  ProductBrandRepository,
  ProductCategoryRepository,
  ProductCategoryInfoRepository,
  ReasonRepository,
  ERPRepository,
  AddressRepository,
  ProductVariantRepository,
  PromotionRepository,
  LoggerRepository,
  StoreLocationRepository,
];

const entities = [
  ProductEntity,
  ImageEntity,
  AdminEntity,
  UserEntity,
  CountryOriginEntity,
  CountryOriginEntity,
  CountryOriginInfoEntity,
  CustomerEntity,
  OrderEntity,
  OrderItemEntity,
  ProductInfoEntity,
  ProductBrandEntity,
  ProductCategoryEntity,
  ProductCategoryInfoEntity,
  ReasonEntity,
  ErpAdminEntity,
  AddressEntity,
  ProductVariantEntity,

  PromotionEntity,
  LoggerEntity,
  StoreLocationEntity,
];

@Module({
  imports: [
    MongooseModule.forFeature([
      ...entities.map((entity) => ({
        name: entity.modelName,
        schema: entity.model.schema,
      })),
    ]),
  ],
  providers: providers,
  exports: [...providers],
})
export class RepositoryModule {}
