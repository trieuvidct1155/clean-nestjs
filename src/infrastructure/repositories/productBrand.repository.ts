import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ProductBrandEntity } from '../entities/productBrand.entity';
import { BaseRepository } from './base.repository';

export class ProductBrandRepository extends BaseRepository<ProductBrandEntity> {
  constructor(
    @InjectModel(ProductBrandEntity.modelName)
    private readonly _modelRepository: ModelType<ProductBrandEntity>,
  ) {
    super(_modelRepository);
  }
}
