import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { BaseRepository } from './base.repository';
import { AdminEntity } from '../entities/admin.entity';

export class AdminRepository extends BaseRepository<AdminEntity> {
  constructor(
    @InjectModel(AdminEntity.modelName)
    private readonly _modelRepository: ModelType<AdminEntity>,
  ) {
    super(_modelRepository);
  }
}
