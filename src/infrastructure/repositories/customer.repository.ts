import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import path from 'path';
import { CustomerEntity } from '../entities/customer.entity';
import { BaseRepository } from './base.repository';

export class CustomerRepository extends BaseRepository<CustomerEntity> {
  constructor(
    @InjectModel(CustomerEntity.modelName)
    private readonly _modelRepository: ModelType<CustomerEntity>,
  ) {
    super(_modelRepository);
  }

  async existIdCustomer(id_customer: string): Promise<boolean> {
    const signalGet = await this.get({
      conditions: { erpCode: id_customer },
    });

    return !!signalGet;
  }

  async getDataCustomerWithErpCode(erpCode: string): Promise<any> {
    const signalget = await this.get({
      conditions: { erpCode: erpCode },
      populates: [
        {
          path: 'listAddress',
          populate: [
            {
              path: 'default',
            },
          ],
        },
      ],
    });
    return {
      location: {
        address: signalget?.listAddress?.default?.['address'] || '',
        province: signalget?.listAddress?.default?.['erpProvince'] || '',
        district: signalget?.listAddress?.default?.['erpDistrict'] || '',
        ward: signalget?.listAddress?.default?.['erpWard'] || '',
      },
      customer: signalget?._id || null,
      email: signalget?.email || '',
      name: signalget?.name || '',
      customerPhone: signalget?.phone || '',
    };
  }
}
