import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { CountryOriginEntity } from '../entities/countryOrigin.entity';
import { BaseRepository } from './base.repository';

export class CountryOriginRepository extends BaseRepository<CountryOriginEntity> {
  constructor(
    @InjectModel(CountryOriginEntity.modelName)
    private readonly _modelRepository: ModelType<CountryOriginEntity>,
  ) {
    super(_modelRepository);
  }
}
