import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { BaseRepository } from './base.repository';
import { PromotionEntity } from '../entities/promotion.entity';
import { IPromotionRepository } from '@/domain/repositories/promotion.interface';

export class PromotionRepository
  extends BaseRepository<PromotionEntity>
  implements IPromotionRepository
{
  constructor(
    @InjectModel(PromotionEntity.modelName)
    private readonly _modelRepository: ModelType<PromotionEntity>,
  ) {
    super(_modelRepository);
  }

  public async getDataPromotion(promotionId: any) {
    try {
      return this.get({
        conditions: { _id: promotionId, dateExpired: { $gt: new Date() } },
        populates: [],
        select: '',
      });
    } catch (e) {}
  }
}
