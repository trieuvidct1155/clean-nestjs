import { IProductRepository } from '@/domain/repositories/product.interface';
import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ProductEntity } from '../entities/product.entity';
import { BaseRepository } from './base.repository';

export class ProductRepository
  extends BaseRepository<ProductEntity>
  implements IProductRepository
{
  constructor(
    @InjectModel(ProductEntity.modelName)
    private readonly _modelRepository: ModelType<ProductEntity>,
  ) {
    super(_modelRepository);
  }

  updateQuantityProductOrderCancel = async (listOrderItems) => {
    for (let i = 0; i < listOrderItems.length; i++) {
      const productId = listOrderItems?.[i]?.product || null;
      const _quantity = listOrderItems?.[i]?.quantities || 0;

      const updateQuantity = await this.updateV2({
        matchConditions: { _id: productId },
        updateQuery: {
          $inc: {
            quantities: +parseInt(_quantity),
          },
        },
      });
      if (!!!updateQuantity) {
        return false;
      }
    }

    return true;
  };

  updateQuantityProductOrderSuccess = async (listOrderItems) => {
    for (let i = 0; i < listOrderItems.length; i++) {
      const productId = listOrderItems?.[i]?.product || null;
      const _quantity = listOrderItems?.[i]?.quantities || 0;
      const updateQuantity = await this.updateV2({
        matchConditions: { _id: productId },
        updateQuery: {
          $inc: {
            quantities: -parseInt(_quantity),
          },
        },
      });
      if (!!!updateQuantity) {
        return false;
      }
    }
    return true;
  };
}
