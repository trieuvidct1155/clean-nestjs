import {
  ICreateOrderItem,
  IOrderItemRepository,
} from '@/domain/repositories/orderItem.interface';
import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { OrderItemEntity } from '../entities/orderItems.entity';
import { BaseRepository } from './base.repository';

export class OrderItemRepository
  extends BaseRepository<OrderItemEntity>
  implements IOrderItemRepository
{
  constructor(
    @InjectModel(OrderItemEntity.modelName)
    private readonly _modelRepository: ModelType<OrderItemEntity>,
  ) {
    super(_modelRepository);
  }

  public async createOrderItem(
    payload: ICreateOrderItem,
  ): Promise<OrderItemEntity> {
    try {
      const signalCreate = await this.create({
        product: payload.signalProducts._id || 'null',
        quantities: payload.order_items?.quantities || 0,
        order: payload.id,
        price: payload.order_items?.price || 0,
        totalPrice: payload.order_items?.totalPrice || 0,
        category: payload.signalProducts.category,
      });

      return signalCreate;
    } catch (e) {}
  }

  public async getDataListOrderItem(conditions: any): Promise<any> {
    try {
      const signalData = await this.getList({
        conditions: { ...conditions },
        select: '',
      });
      const data = signalData?.docs || [];
      return data;
    } catch (e) {}
  }
}
