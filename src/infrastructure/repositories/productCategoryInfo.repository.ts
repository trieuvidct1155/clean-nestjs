import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ProductCategoryInfoEntity } from '../entities/productCategoryInfo.entity';
import { BaseRepository } from './base.repository';

export class ProductCategoryInfoRepository extends BaseRepository<ProductCategoryInfoEntity> {
  constructor(
    @InjectModel(ProductCategoryInfoEntity.modelName)
    private readonly _modelRepository: ModelType<ProductCategoryInfoEntity>,
  ) {
    super(_modelRepository);
  }
}
