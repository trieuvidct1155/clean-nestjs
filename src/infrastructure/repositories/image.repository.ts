import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { BaseRepository } from './base.repository';
import { IImageRepository } from '@/domain/repositories/image.interface';
import { ImageEntity } from '../entities/image.entity';

export class ImageRepository
  extends BaseRepository<ImageEntity>
  implements IImageRepository
{
  constructor(
    @InjectModel(ImageEntity.modelName)
    private readonly _modelRepository: ModelType<ImageEntity>,
  ) {
    super(_modelRepository);
  }
}
