import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { CountryOriginInfoEntity } from '../entities/countryOriginInfo.entity';
import { BaseRepository } from './base.repository';

export class CountryOriginInfoRepository extends BaseRepository<CountryOriginInfoEntity> {
  constructor(
    @InjectModel(CountryOriginInfoEntity.modelName)
    private readonly _modelRepository: ModelType<CountryOriginInfoEntity>,
  ) {
    super(_modelRepository);
  }
}
