import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { LoggerEntity } from '../entities/logger.entity';
import { BaseRepository } from './base.repository';

export class LoggerRepository extends BaseRepository<LoggerEntity> {
  constructor(
    @InjectModel(LoggerEntity.modelName)
    private readonly _modelRepository: ModelType<LoggerEntity>,
  ) {
    super(_modelRepository);
  }

  async createLogger({ _id, user, type, actions }): Promise<boolean> {
    try {
      const dataLogger = {
        _id: _id,
        user: user,
        type: type,
        actions: actions,
      };
      const signalCreate = await this.create(dataLogger);
      return !!signalCreate;
    } catch (error) {
      return false;
    }
  }
}
