import {
  IBaseRepository,
  ICountRepository,
  ICreateOrUpdateRepository,
  IGetListRepository,
  IGetRepository,
  IRemoveManyRepository,
  IUpdateRepository,
} from '@/domain/repositories/base.interface';
import { Injectable } from '@nestjs/common';
import { ModelType } from '@typegoose/typegoose/lib/types';

@Injectable()
export class BaseRepository<MODEL> implements IBaseRepository<MODEL> {
  constructor(model: ModelType<MODEL>) {
    this._model = model;
  }

  protected _model: ModelType<MODEL>;

  protected get modelName(): string {
    return this._model.modelName;
  }

  /**
   * FIND ONE WITH CONDITIONS
   * @param conditions Model Query
   * @returns the first document that match the model query conditions
   */
  async get(payload: IGetRepository<MODEL>): Promise<MODEL> {
    const { conditions = {}, populates = [], select = '' } = payload;
    try {
      const queryPromisePipeline = this._model.findOne(conditions);

      populates.forEach((populate) => {
        queryPromisePipeline.populate(populate);
      });

      if (select) queryPromisePipeline.select(select);

      const doc: MODEL = await queryPromisePipeline.lean();
      return doc;
    } catch (error) {
      return null;
    }
  }

  async getList(payload: Partial<IGetListRepository<MODEL>>): Promise<any> {
    const {
      conditions = {},
      skip = 0,
      limit = 0,
      select = null,
      sort = null,
      populates = [],
      search = null,
    } = payload;
    try {
      if (search) conditions['$text'] = { $search: `"${search}"` };
      let queryPromisePipeline = this._model.find(conditions);

      populates.forEach((populate) => {
        queryPromisePipeline.populate(populate);
      });

      const _skip = parseInt(`${skip}`),
        _limit = parseInt(`${limit}`);

      if (sort && typeof sort === 'object') {
        queryPromisePipeline = queryPromisePipeline.sort(sort);
      }

      if (_skip) queryPromisePipeline = queryPromisePipeline.skip(skip);
      if (_limit && limit > 0)
        queryPromisePipeline = queryPromisePipeline.limit(_limit);

      if (select) queryPromisePipeline.select(select);

      const docs: MODEL[] = await queryPromisePipeline.lean();
      const total = await this._model.countDocuments(conditions);

      return {
        docs,
        total,
        skip: _skip + docs.length,
        limit: _limit,
      };
    } catch (error) {
      return [];
    }
  }

  async create(payload: Partial<MODEL>): Promise<MODEL> {
    // try {
    payload['createdAt'] = new Date();
    const doc = await new this._model(payload).save();
    return doc as MODEL;
    // } catch (error) {
    //   return null;
    // }
  }

  async createMany(payloads: Array<Partial<MODEL>>): Promise<MODEL[]> {
    const session = await this._model.startSession();
    try {
      //start transaction
      session.startTransaction();
      const docs = await this._model.insertMany(payloads);
      await session.commitTransaction();
      session.endSession();
      return docs as MODEL[];
    } catch (error) {
      session.abortTransaction();
      session.endSession();
      return [];
    }
  }

  async updateMany(payload: IUpdateRepository<MODEL>): Promise<MODEL> {
    try {
      const { conditions = {}, dataUpdate = {} } = payload;
      const result = await this._model
        .updateMany(conditions, {
          $set: dataUpdate,
        })
        .lean();
      return result as MODEL;
    } catch (error) {
      return null;
    }
  }

  async updateOne({
    conditions = {},
    dataUpdate = {},
  }: IUpdateRepository<MODEL>): Promise<MODEL> {
    try {
      const result = await this._model
        .updateOne(conditions, {
          $set: dataUpdate,
        })
        .lean();
      return result as MODEL;
    } catch (error) {
      return null;
    }
  }

  async updateOneV2({
    conditions = {},
    dataUpdate = {},
  }: IUpdateRepository<MODEL>): Promise<MODEL> {
    try {
      const result = await this._model
        .updateOne(conditions, dataUpdate, { upsert: true })
        .lean();
      return result as MODEL;
    } catch (error) {
      return null;
    }
  }

  updateV2 = async ({ matchConditions = {}, updateQuery = {} }) => {
    try {
      updateQuery['modifiedAt'] = new Date();
      const updateResult = await this._model.findOneAndUpdate(
        matchConditions,
        updateQuery,
        { new: true },
      );
      return updateResult as MODEL;
    } catch (error) {
      return null;
    }
  };

  async createOrUpdate(payload: ICreateOrUpdateRepository<MODEL>) {
    try {
      const {
        conditions = {},
        dataUpdate = {},
        options = {},
        populates = [],
      } = payload;
      const result = await this._model
        .findOneAndUpdate(conditions, dataUpdate, options)
        .populate(populates)
        .lean();
      return result as MODEL;
    } catch (error) {
      return null;
    }
  }

  async count(payload: ICountRepository<MODEL>): Promise<number> {
    try {
      const { conditions = {} } = payload;
      const result = await this._model.count(conditions);
      return result as number;
    } catch (error) {
      return 0;
    }
  }

  remove = async (
    id,
    callback = () => {
      return;
    },
  ) => {
    try {
      const removeResult = await this._model.remove({ _id: id });
      await callback();
      return removeResult;
    } catch (error) {
      return null;
    }
  };

  async removeMany(payload: IRemoveManyRepository<MODEL>): Promise<boolean> {
    const session = await this._model.startSession();
    try {
      const { conditions } = payload;
      session.startTransaction();
      const result = await this._model.deleteMany(conditions);
      await session.commitTransaction();
      await session.endSession();
      return result.acknowledged;
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      return false;
    }
  }
}
