import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { UserEntity } from '../entities/user.entity';
import { BaseRepository } from './base.repository';

export class UserRepository extends BaseRepository<UserEntity> {
  constructor(
    @InjectModel(UserEntity.modelName)
    private readonly _modelRepository: ModelType<UserEntity>,
  ) {
    super(_modelRepository);
  }
}
