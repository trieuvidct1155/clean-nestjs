import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ProductVariantEntity } from '../entities/productVariant.entity';
import { BaseRepository } from './base.repository';

export class ProductVariantRepository extends BaseRepository<ProductVariantEntity> {
  constructor(
    @InjectModel(ProductVariantEntity.modelName)
    private readonly _modelRepository: ModelType<ProductVariantEntity>,
  ) {
    super(_modelRepository);
  }
}
