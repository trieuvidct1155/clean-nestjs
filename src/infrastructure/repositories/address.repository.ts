import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { BaseRepository } from './base.repository';
import { IAddressRepository } from '@/domain/repositories/address.interface';
import { AddressEntity } from '@entities/address.entity';

export class AddressRepository
  extends BaseRepository<AddressEntity>
  implements IAddressRepository
{
  constructor(
    @InjectModel(AddressEntity.modelName)
    private readonly _modelRepository: ModelType<AddressEntity>,
  ) {
    super(_modelRepository);
  }

  public async getDataListAddress(dataPayload: Partial<AddressEntity>) {
    try {
      const signalCreateAddress = await this.create(dataPayload);
      if (signalCreateAddress) {
        const addressId = signalCreateAddress?._id || null;

        const dataUpdateAddressCustomer = {};
        const addressDefault = addressId || {};
        dataUpdateAddressCustomer['default'] = addressDefault;
        dataUpdateAddressCustomer['list'] = [];

        return dataUpdateAddressCustomer;
      }
      throw new Error('Does not create address');
    } catch (e) {}
  }

  public async updateAddressId(id, dataUpdate: Partial<AddressEntity>) {
    return this.updateOne({
      conditions: { _id: id },
      dataUpdate: dataUpdate,
    });
  }
}
