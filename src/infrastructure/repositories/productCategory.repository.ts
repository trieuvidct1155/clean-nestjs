import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ProductCategoryEntity } from '../entities/productCategory.entity';
import { BaseRepository } from './base.repository';

export class ProductCategoryRepository extends BaseRepository<ProductCategoryEntity> {
  constructor(
    @InjectModel(ProductCategoryEntity.modelName)
    private readonly _modelRepository: ModelType<ProductCategoryEntity>,
  ) {
    super(_modelRepository);
  }
}
