import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { OrderEntity } from '../entities/order.entity';
import { BaseRepository } from './base.repository';

export class OrderRepository extends BaseRepository<OrderEntity> {
  constructor(
    @InjectModel(OrderEntity.modelName)
    private readonly _modelRepository: ModelType<OrderEntity>,
  ) {
    super(_modelRepository);
  }
}
