import { InjectModel } from '@nestjs/mongoose';
import { ModelType } from '@typegoose/typegoose/lib/types';
import { ReasonEntity } from '../entities/reason.entity';
import { BaseRepository } from './base.repository';

export class ReasonRepository extends BaseRepository<ReasonEntity> {
  constructor(
    @InjectModel(ReasonEntity.modelName)
    private readonly _modelRepository: ModelType<ReasonEntity>,
  ) {
    super(_modelRepository);
  }
}
