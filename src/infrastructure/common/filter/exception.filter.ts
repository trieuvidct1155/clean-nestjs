import { SlackBlock } from '@/infrastructure/helpers/slack-block';
import { LoggerService } from '@/infrastructure/logger/logger.service';
import { SlackService } from '@/infrastructure/services/slack/slack.service';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';

interface IError {
  message: string;
  code_error: string;
}

@Catch(HttpException)
export class AllExceptionFilter implements ExceptionFilter {
  constructor(
    private readonly logger: LoggerService,
    private readonly slack: SlackService,
  ) {}
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response: Response = ctx.getResponse();
    const request: Request = ctx.getRequest();

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    const message =
      exception instanceof HttpException
        ? (exception.getResponse() as IError)
        : { message: (exception as Error).message, code_error: null };

    const responseData = {
      ...{
        statusCode: status,
        timestamp: new Date().toISOString(),
        path: request.url,
      },
      ...message,
    };

    this.logMessage(request, message, status, exception);

    response.status(status).json(responseData);
  }

  private logMessage(
    request: any,
    message: IError,
    status: number,
    exception: any,
  ) {
    const _broker = new SlackBlock([]);
    _broker.setHeaderBlock('Message from Exception Filter API');
    _broker.setDividerBlock();
    _broker.setSectionTextBlock({
      message,
      method: request.method,
      url: request.url,
      status,
      query: request.query,
      body: request.body,
      params: request.params,
    });

    this.slack.sendSlackMessage({
      type: 'err',
      broker: _broker,
      message: message.message,
    });

    if (status === 500) {
      this.logger.error(
        `End Request for ${request.path}`,
        `method=${request.method} status=${status} code_error=${
          message.code_error ? message.code_error : null
        } message=${message.message ? message.message : null}`,
        status >= 500 ? exception.stack : '',
      );
    } else {
      this.logger.error(
        `End Request for ${request.path}`,
        `method=${request.method} status=${status} code_error=${
          message.code_error ? message.code_error : null
        } message=${message.message ? message.message : null}`,
      );
    }
  }
}
