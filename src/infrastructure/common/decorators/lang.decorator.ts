import { ELang, ILanguage } from '@/domain/decorators/lang.decorator';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';

const DEFAULT_LANG = ELang.VI;

export const Language = createParamDecorator(
  (data: Partial<ILanguage>, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { lang } = request.query;

    const _lang = lang ? lang : data?.lang || DEFAULT_LANG;

    return _lang;
  },
);
