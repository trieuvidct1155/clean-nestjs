import { IPaging } from '@/domain/decorators/paging.decorator';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';

const DEFAULT_LIMIT = 10;
const DEFAULT_SKIP = 0;

export const Paging = createParamDecorator(
  (data: Partial<IPaging>, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const { skip, limit } = request.query;

    const _skip = skip ? parseInt(skip, 10) : data?.skip || DEFAULT_SKIP;
    const _limit = limit ? parseInt(limit, 10) : data?.limit || DEFAULT_LIMIT;

    return {
      skip: _skip,
      limit: _limit,
    };
  },
);
