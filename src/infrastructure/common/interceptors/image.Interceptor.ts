import { diskStorage } from 'multer';
import { FilesInterceptor } from '@nestjs/platform-express';
import { DIR_UPLOAD } from '@/domain/interceptors';
import { ValidateFile } from '@/infrastructure/helpers/validate-file';
import { FileKenZy } from '@/infrastructure/helpers/file-kenzy';

interface IPayloadImageInterceptor {
  fileName: string;
  maxUpload: number;
}

export class ImageInterceptor {
  private fileName: string;
  private maxUpload: number;

  constructor(payload: IPayloadImageInterceptor) {
    this.fileName = payload.fileName;
    this.maxUpload = payload.maxUpload;
  }

  private filter() {
    return (req, file, cb) => {
      if (!new ValidateFile(file).isImage()) {
        req.imageError = 'Only image files are allowed!';
        return cb(null, false);
      }
      return cb(null, true);
    };
  }

  private multerOptions() {
    return {
      destination: DIR_UPLOAD.IMAGES_STORAGE_DIR,
      filename: function (req, file, cb) {
        if (!new ValidateFile(file).isImage()) {
          cb(new Error('Only image files are allowed!'), false);
        }
        const fileKenZy = new FileKenZy(file.originalname);
        const filename = fileKenZy.getFileName();
        cb(null, filename);
      },
    };
  }

  public exclude() {
    return FilesInterceptor(this.fileName, this.maxUpload, {
      storage: diskStorage(this.multerOptions()),
      fileFilter: this.filter(),
    });
  }
}
