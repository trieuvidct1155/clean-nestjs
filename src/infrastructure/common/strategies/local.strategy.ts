import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { UseCaseProxy } from '@/infrastructure/usecases-proxy/usecases-proxy';
import { ADMIN_UC_PROXY_TYPES } from '@/usecases/modules/admins/admin-uc-proxy.type';
import { LoginAdminUseCase } from '@/usecases/modules/admins/login-admin-proxy.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(
    @Inject(ADMIN_UC_PROXY_TYPES.GET_ADMIN_USECASE_PROXY)
    private readonly loginAdminUseCase: UseCaseProxy<LoginAdminUseCase>,
  ) {
    super();
  }

  async validate(payload): Promise<any> {
    const user = await this.loginAdminUseCase.getInstance().execute(payload);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
