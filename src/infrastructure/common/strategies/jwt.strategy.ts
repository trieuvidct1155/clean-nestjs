import { UseCaseProxy } from '@/infrastructure/usecases-proxy/usecases-proxy';
import { ADMIN_UC_PROXY_TYPES } from '@/usecases/modules/admins/admin-uc-proxy.type';
import { GetAdminUseCase } from '@/usecases/modules/admins/get-admin-proxy.service';
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @Inject(ADMIN_UC_PROXY_TYPES.GET_ADMIN_USECASE_PROXY)
    private readonly getAdminUseCase: UseCaseProxy<GetAdminUseCase>,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: any) {
    const signalGet = await this.getAdminUseCase.getInstance().execute(payload);

    if (!!!signalGet) {
      throw new UnauthorizedException();
    }
    return signalGet;
  }
}
