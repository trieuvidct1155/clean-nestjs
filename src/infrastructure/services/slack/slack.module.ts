import { EnvironmentConfigModule } from '@/infrastructure/config/environment-config/environment-config.module';
import { EnvironmentConfigService } from '@/infrastructure/config/environment-config/environment-config.service';
import { Module } from '@nestjs/common';
import { SlackService } from './slack.service';

@Module({
  imports: [EnvironmentConfigModule],
  providers: [EnvironmentConfigService, SlackService],
  exports: [SlackService],
})
export class SlackModule {}
