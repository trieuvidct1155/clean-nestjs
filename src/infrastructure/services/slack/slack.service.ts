import { SlackChannelDev } from './../../helpers/slack-channel/slack-channel-dev';
import {
  IPayloadSlackSendMessage,
  IPayloadSlackSendMessageWarn,
  ISlackService,
} from '@/domain/adapters/slack.interface';
import { EnvironmentConfigService } from '@/infrastructure/config/environment-config/environment-config.service';
import { Injectable } from '@nestjs/common';
import { Block, KnownBlock, WebClient } from '@slack/web-api';
import { SlackChannelERR } from '@/infrastructure/helpers/slack-channel/slack-channel-err';
import { SlackBlock } from '@/infrastructure/helpers/slack-block';

@Injectable()
export class SlackService implements ISlackService {
  private client: WebClient;
  private channels = {
    dev: SlackChannelDev,
    err: SlackChannelERR,
  };
  constructor(
    private readonly environmentConfigService: EnvironmentConfigService,
  ) {
    const token = this.environmentConfigService.getTokenSlack();
    this.client = new WebClient(token);
  }

  getChannel(type: string): string {
    return new this.channels[type](this.environmentConfigService).getChannel();
  }

  async sendSlackMessage(
    slackMessage: IPayloadSlackSendMessage,
  ): Promise<void> {
    const { type, message, broker } = slackMessage;

    const _channel = this.getChannel(type);
    await this.client.chat.postMessage({
      channel: _channel,
      text: message || 'Message from KenZy',
      blocks: broker.getBlocks(),
    });
  }

  async sendSlackMessageWarn(
    slackMessage: IPayloadSlackSendMessageWarn,
  ): Promise<void> {
    const { type, title, data, payload } = slackMessage;

    const _broker = new SlackBlock([]);
    _broker.setHeaderBlock(title);
    _broker.setDividerBlock();
    _broker.setSectionTextBlock({
      data: data,
      payload: payload,
    });

    const _channel = this.getChannel(type);
    await this.client.chat.postMessage({
      channel: _channel,
      text: 'Message Error',
      blocks: _broker.getBlocks(),
    });
  }
}
