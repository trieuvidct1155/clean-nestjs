import { Message, ProducerRecord } from 'kafkajs';

export abstract class AbsKafkaMessage {
  protected readonly topic: string;
  protected readonly payload: any;
  constructor(topic: string, payload: any) {
    this.topic = topic;
    this.payload = payload;
  }
  abstract get record(): ProducerRecord;

  private formatArrayMessage(messages: Array<unknown>): Message[] {
    return messages.map((m) => {
      return {
        value: JSON.stringify(m),
      };
    });
  }

  private formatMessage(message: unknown): Message[] {
    return [
      {
        value: JSON.stringify(message),
      },
    ];
  }

  protected formatMessageByType(message: Array<unknown> | unknown): Message[] {
    if (Array.isArray(message)) {
      return this.formatArrayMessage(message);
    }
    return this.formatMessage(message);
  }
}
