import { AbsKafkaMessage } from './abs-kafka-message.service';

export class KafkaMessage {
  constructor(private readonly instance: AbsKafkaMessage) {}

  get record() {
    return this.instance.record;
  }
}
