import { EnvironmentConfigService } from '@/infrastructure/config/environment-config/environment-config.service';
import {
  Injectable,
  OnApplicationShutdown,
  OnModuleInit,
} from '@nestjs/common';
import { Kafka, Producer, ProducerRecord } from 'kafkajs';

@Injectable()
export class KafkaProducerService
  implements OnModuleInit, OnApplicationShutdown
{
  private producer: Producer;
  private kafka: Kafka;

  constructor(
    private readonly environmentConfigService: EnvironmentConfigService,
  ) {
    this.init();
  }

  init() {
    this.kafka = new Kafka({
      brokers: this.environmentConfigService.getBrokers(),
    });

    this.producer = this.kafka.producer();
  }

  async onApplicationShutdown() {
    await this.producer.disconnect();
  }
  async onModuleInit() {
    await this.producer.connect();
  }

  async send(record: ProducerRecord) {
    await this.producer.send(record);
  }
}
