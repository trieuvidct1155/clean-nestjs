import { EnvironmentConfigService } from '@/infrastructure/config/environment-config/environment-config.service';
import { Injectable, OnApplicationShutdown } from '@nestjs/common';
import {
  Consumer,
  ConsumerRunConfig,
  ConsumerSubscribeTopic,
  ConsumerSubscribeTopics,
  Kafka,
} from 'kafkajs';

@Injectable()
export class KafkaConsumerService implements OnApplicationShutdown {
  private kafka;
  private consumers: Consumer[];

  constructor(
    private readonly environmentConfigService: EnvironmentConfigService,
  ) {
    this.init();
  }

  init() {
    this.consumers = [];
    this.kafka = new Kafka({
      brokers: this.environmentConfigService.getBrokers(),
    });
  }

  async onApplicationShutdown() {
    console.log('disconnecting consumers');
    for (const consumer of this.consumers) {
      await consumer.disconnect();
    }
  }

  async consume(
    groupId: string,
    topic: ConsumerSubscribeTopics | ConsumerSubscribeTopic,
    config: ConsumerRunConfig,
  ) {
    const consumer: Consumer = this.kafka.consumer({ groupId: groupId });
    await consumer.connect().catch((e) => console.error(e));
    await consumer.subscribe(topic);
    await consumer.run(config);
    this.consumers.push(consumer);
  }
}
