import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class JwtTokenService {
  constructor(private readonly jwtService: JwtService) {}

  async checkToken(token: string): Promise<any> {
    const decode = await this.jwtService.verifyAsync(token);
    return decode;
  }

  async decode(token: string, spe: string): Promise<any> {
    const [, _token] = this.getTokenByString(token, spe);
    const decode = await this.jwtService.decode(_token);
    return decode;
  }

  private getTokenByString(str: string, spe = ' ') {
    if (!str) return null;
    return str.split(spe);
  }

  createToken(payload: any, secret: string, expiresIn: string): string {
    return this.jwtService.sign(payload, {
      secret: secret,
      expiresIn: expiresIn,
    });
  }
}
