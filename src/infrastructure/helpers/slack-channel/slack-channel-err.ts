import { ISlackChannel } from '@/domain/helpers/slack-channel';
import { EnvironmentConfigService } from '@/infrastructure/config/environment-config/environment-config.service';

export class SlackChannelERR implements ISlackChannel {
  private channel: string;
  constructor(env: EnvironmentConfigService) {
    this.channel = env.getChancelErr();
  }

  getChannel(): string {
    return this.channel;
  }
}
