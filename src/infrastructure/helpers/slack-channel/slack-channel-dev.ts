import { ISlackChannel } from '@/domain/helpers/slack-channel';
import { EnvironmentConfigService } from '@/infrastructure/config/environment-config/environment-config.service';

export class SlackChannelDev implements ISlackChannel {
  private channel: string;
  constructor(env: EnvironmentConfigService) {
    this.channel = env.getChancelDev();
  }

  getChannel(): string {
    return this.channel;
  }
}
