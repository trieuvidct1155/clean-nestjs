export class ValidateVariable {
  public static isUndefined(variable: any): boolean {
    return typeof variable === 'undefined';
  }

  public static isNull(variable: any): boolean {
    return variable === null;
  }

  public static isNullOrUndefined(variable: any): boolean {
    return this.isUndefined(variable) || this.isNull(variable);
  }

  public static isEmpty(variable: any): boolean {
    return this.isNullOrUndefined(variable) || variable === '';
  }

  public static isNotEmpty(variable: any): boolean {
    return !this.isEmpty(variable);
  }

  public static isObject(variable: any): boolean {
    return typeof variable === 'object';
  }

  public static isString(variable: any): boolean {
    return typeof variable === 'string';
  }

  public static isNumber(variable: any): boolean {
    return typeof variable === 'number';
  }

  public static isBoolean(variable: any): boolean {
    return typeof variable === 'boolean';
  }

  public static isArray(variable: any): boolean {
    return Array.isArray(variable);
  }

  public static isFunction(variable: any): boolean {
    return typeof variable === 'function';
  }

  public static isPromise(variable: any): boolean {
    return variable instanceof Promise;
  }

  public static isObjectEmpty(variable: any): boolean {
    return this.isObject(variable) && Object.keys(variable).length === 0;
  }

  public static isObjectNotEmpty(variable: any): boolean {
    return !this.isObjectEmpty(variable);
  }

  public static isArrayEmpty(variable: any): boolean {
    return this.isArray(variable) && variable.length === 0;
  }

  public static isArrayNotEmpty(variable: any): boolean {
    return !this.isArrayEmpty(variable);
  }
}
