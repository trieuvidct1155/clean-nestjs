import { IValidateFile } from '@/domain/helpers/validate-file';
export class ValidateFile implements IValidateFile {
  private file: any;
  constructor(file: any) {
    this.file = file;
  }

  public isImage() {
    const fileType = this.file.mimetype.split('/')[0] || '';
    return fileType == 'image';
  }

  public isVideo() {
    const fileType = this.file.mimetype.split('/')[0] || '';
    return fileType == 'video';
  }

  public isDocument() {
    const fileType = this.file.mimetype.split('/')[0] || '';
    return fileType == 'application';
  }
}
