export class VietNameseTone {
  private vietnameseTone = [
    {
      tone: 1,
      regex: /([aàáạảãăằắặẳẵâầấậẩẫ])/g,
      replace: 'a',
    },
    {
      tone: 2,
      regex: /([eèéẹẻẽêềếệểễ])/g,
      replace: 'e',
    },
    {
      tone: 3,
      regex: /([iìíịỉĩ])/g,
      replace: 'i',
    },
    {
      tone: 4,
      regex: /([oòóọỏõôồốộổỗơờớợởỡ])/g,
      replace: 'o',
    },
    {
      tone: 5,
      regex: /([uùúụủũưừứựửữ])/g,
      replace: 'u',
    },
    {
      tone: 6,
      regex: /([yỳýỵỷỹ])/g,
      replace: 'y',
    },
    {
      tone: 7,
      regex: /([dđ])/g,
      replace: 'd',
    },
  ];

  private clearSpaceChar(str: string) {
    return str.replace(/ + /g, ' ');
  }

  private clearIndividualSpecialCharacter(str: string) {
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ''); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    return str;
  }

  private removeSpecialCharacters(str: string) {
    const regex =
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g;
    return str.replace(regex, '');
  }

  private clearVNCharacter(str: string) {
    return this.vietnameseTone.reduce((acc, curr) => {
      return acc.replace(curr.regex, curr.replace);
    }, str);
  }

  public remove(str) {
    str = this.removeSpecialCharacters(str);
    str = this.clearVNCharacter(str);
    str = this.clearIndividualSpecialCharacter(str);
    str = this.clearSpaceChar(str);
    str = str.trim();
    str = str.replace(/\s+/g, '-');
    return str.toLocaleLowerCase();
  }
}
