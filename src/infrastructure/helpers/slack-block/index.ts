import { ISlackBlock } from '@/domain/helpers/slack-block';
import { Block, KnownBlock } from '@slack/web-api';

export class SlackBlock implements ISlackBlock {
  private _brokers: (Block | KnownBlock)[];
  constructor(brokers: any) {
    this._brokers = brokers;
  }

  setSectionTextBlock(payload: any): void {
    const blockItem: KnownBlock = {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: '```' + JSON.stringify(payload, null, 2) + '```',
      },
    };
    this._brokers.push(blockItem);
  }
  setHeaderBlock(text: string): void {
    const blockItem: Block | KnownBlock = {
      type: 'header',
      text: {
        type: 'plain_text',
        text: text,
        emoji: true,
      },
    };
    this._brokers.push(blockItem);
  }

  setDividerBlock(): void {
    const blockItem: Block | KnownBlock = {
      type: 'divider',
    };
    this._brokers.push(blockItem);
  }

  getBlocks(): (Block | KnownBlock)[] {
    return this._brokers;
  }
}
