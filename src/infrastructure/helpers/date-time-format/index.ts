const moment = require('moment');
moment.locale('vi');

const DATE_FORMAT_FN = (date, format = 'DD/MM/YYYY HH:mm') => {
  let temp = new Date(date);
  temp.setHours(temp.getHours() + 7);
  return moment(temp).format(format);
};

export { DATE_FORMAT_FN };
