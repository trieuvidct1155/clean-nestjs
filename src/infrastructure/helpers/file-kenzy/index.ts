import { VietNameseTone } from '../vietnamese-tone';

export class FileKenZy {
  private originalname: string;
  constructor(originalname: string) {
    this.originalname = originalname;
  }

  public getFileExtension() {
    if (!this.originalname) return '';
    return this.originalname.split('.').pop();
  }

  public getFileRawName() {
    if (!this.originalname) return '';
    const fileExtensions = this.getFileExtension();
    return this.originalname.split('.' + fileExtensions)[0];
  }

  public getFileName() {
    const fileExtensions = this.getFileExtension();
    const fileRawName = this.getFileRawName();
    const cleanFileName = decodeURIComponent(
      new VietNameseTone().remove(fileRawName),
    );
    return `${cleanFileName}-${Math.round(
      Math.random() * 10000,
    )}.${fileExtensions}`;
  }
}
