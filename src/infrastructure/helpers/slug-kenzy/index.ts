export class SlugKenZy {
  static create(title: string): string {
    let str = title || '';
    const tones = [
      { from: 'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ', to: 'a' },
      { from: 'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ', to: 'e' },
      { from: 'ì|í|ị|ỉ|ĩ', to: 'i' },
      { from: 'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ', to: 'o' },
      { from: 'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ', to: 'u' },
      { from: 'ỳ|ý|ỵ|ỷ|ỹ', to: 'y' },
      { from: 'đ', to: 'd' },
      {
        from: '',
        to: ' ',
      },
      { from: '', to: ' ' },
    ];
    tones.forEach((tone) => {
      str = str.replace(new RegExp(tone.from, 'g'), tone.to);
    });
    str = str.trim();
    const regex =
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g;
    str.replace(regex, '');
    str = str.replace(/ + /g, '');

    return str;
  }

  static createURL(title: string): string {
    let str = title;
    if (str && str.length > 0) {
      str = str.toLowerCase();
      str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
      str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
      str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
      str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
      str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
      str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
      str = str.replace(/đ/g, 'd');
      str = str.replace(
        /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
        ' ',
      );
      str = str.replace(/ + /g, ' ');
      str = str.trim();
      str = str.replace(/\s+/g, '-');
    }
    return str;
  }

  static createShortenName(str) {
    const fullName = str.split(' ');
    let shorten = '';
    fullName.forEach((e) => (shorten += e.charAt(0)));
    return shorten.toUpperCase();
  }
}
