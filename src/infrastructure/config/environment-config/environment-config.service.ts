import { KafkaConfig } from '@/domain/config/kafka.interface';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DatabaseConfig } from '@domain/config/database.interface';
import { JWTConfig } from '@domain/config/jwt.interface';
import { SlackConfig } from '@/domain/config/slack.interface';

@Injectable()
export class EnvironmentConfigService
  implements DatabaseConfig, JWTConfig, SlackConfig, KafkaConfig
{
  constructor(private configService: ConfigService) {}
  getBrokers(): string[] {
    return this.configService.get<string>('KAFKA_BROKERS').split(',');
  }
  getGroupId(): string {
    return this.configService.get<string>('KAFKA_GROUP_ID');
  }
  getTokenSlack(): string {
    return this.configService.get<string>('TOKEN_SLACK');
  }
  getChancelDev(): string {
    return this.configService.get<string>('WINEMART_DEV_SLACK');
  }
  getChancelErr(): string {
    return this.configService.get<string>('WINEMART_ERROR_SLACK');
  }

  nodeEnv(): string {
    return this.configService.get<string>('NODE_ENV');
  }

  getJwtSecret(): string {
    return this.configService.get<string>('JWT_SECRET');
  }

  getJwtExpirationTime(): string {
    return this.configService.get<string>('JWT_EXPIRATION_TIME');
  }

  getJwtRefreshSecret(): string {
    return this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET');
  }

  getJwtRefreshExpirationTime(): string {
    return this.configService.get<string>('JWT_REFRESH_TOKEN_EXPIRATION_TIME');
  }

  getDatabaseHost(): string {
    return this.configService.get<string>('DATABASE_HOST');
  }

  getDatabasePort(): number {
    return this.configService.get<number>('DATABASE_PORT');
  }

  getDatabaseUser(): string {
    return this.configService.get<string>('DATABASE_USER');
  }

  getDatabasePassword(): string {
    return this.configService.get<string>('DATABASE_PASSWORD');
  }

  getDatabaseName(): string {
    return this.configService.get<string>('DATABASE_NAME');
  }

  getDatabaseUri(): string {
    return `mongodb://${this.getDatabaseHost()}:${this.getDatabasePort()}/${this.getDatabaseName()}`;
  }
}
