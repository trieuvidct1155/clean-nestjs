import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleFactoryOptions } from '@nestjs/mongoose';
import { EnvironmentConfigModule } from '../environment-config/environment-config.module';
import { EnvironmentConfigService } from '../environment-config/environment-config.service';

export const getTypegooseModuleOptions = (
  config: EnvironmentConfigService,
): MongooseModuleFactoryOptions =>
  ({
    uri: config.getDatabaseUri(),
    // dbName: config.getDatabaseName(),
    // user: config.getDatabaseUser(),
    // pass: config.getDatabasePassword(),
  } as MongooseModuleFactoryOptions);

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [EnvironmentConfigModule],
      inject: [EnvironmentConfigService],
      useFactory: getTypegooseModuleOptions,
    }),
  ],
})
export class TypegooseConfigModule {}
