import { Module } from '@nestjs/common';
import { EnvironmentConfigService } from '../environment-config/environment-config.service';
import { WinstonConfigService } from './winston-config.service';

@Module({
  imports: [],
  providers: [EnvironmentConfigService, WinstonConfigService],
  exports: [WinstonConfigService],
})
export class WinstonConfigModule {}
