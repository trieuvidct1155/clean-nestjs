import { Injectable } from '@nestjs/common';
import * as winston from 'winston';
import 'winston-daily-rotate-file';
import { EnvironmentConfigService } from '../environment-config/environment-config.service';

@Injectable()
export class WinstonConfigService {
  private nodeEnv;
  private o0oKenDailyRotateFile;
  constructor(private _envConfigService: EnvironmentConfigService) {
    this.nodeEnv = this._envConfigService.nodeEnv();
    this.o0oKenDailyRotateFile = winston.transports.DailyRotateFile;
  }

  get winstonConfig(): winston.LoggerOptions {
    return {
      transports: [
        new this.o0oKenDailyRotateFile({
          level: 'debug',
          filename: `./logs/${this.nodeEnv}/debug-%DATE%.log`,
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json(),
          ),
        }),
        new this.o0oKenDailyRotateFile({
          level: 'error',
          filename: `./logs/${this.nodeEnv}/error-%DATE%.log`,
          datePattern: 'YYYY-MM-DD',
          zippedArchive: false,
          maxSize: '20m',
          maxFiles: '30d',
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json(),
          ),
        }),
        new winston.transports.Console({
          level: 'debug',
          handleExceptions: true,
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp({
              format: 'DD-MM-YYYY HH:mm:ss',
            }),
            winston.format.simple(),
          ),
        }),
      ],
      exitOnError: false,
    };
  }
}
