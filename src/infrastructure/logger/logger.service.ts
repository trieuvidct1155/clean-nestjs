import { ConsoleLogger, Injectable } from '@nestjs/common';
import { ILogger } from '../../domain/logger/logger.interface';
import * as winston from 'winston';
import { WinstonConfigService } from '../config/winston-config/winston-config.service';
import { EnvironmentConfigService } from '../config/environment-config/environment-config.service';

@Injectable()
export class LoggerService extends ConsoleLogger implements ILogger {
  private readonly _logger: winston.Logger;
  constructor(
    private readonly _winstonConfigService: WinstonConfigService,
    private readonly _environmentConfigService: EnvironmentConfigService,
  ) {
    super(LoggerService.name, { timestamp: true });
    this._logger = winston.createLogger(_winstonConfigService.winstonConfig);
    if (_environmentConfigService.nodeEnv() !== 'production') {
      this._logger.debug('Logging initialized at debug level');
    }
  }

  debug(context: string, message: string) {
    if (process.env.NODE_ENV !== 'production') {
      this._logger.debug(`[DEBUG] ${message}`, context);
    }
  }
  log(context: string, message: string) {
    this._logger.log(`[INFO] ${message}`, context);
  }
  error(context: string, message: string, trace?: string) {
    this._logger.error(`[ERROR] ${message}`, trace, context);
  }
  warn(context: string, message: string) {
    this._logger.warn(`[WARN] ${message}`, context);
  }
  verbose(context: string, message: string) {
    if (process.env.NODE_ENV !== 'production') {
      this._logger.verbose(`[VERBOSE] ${message}`, context);
    }
  }
}
