import { Module } from '@nestjs/common';
import { EnvironmentConfigService } from '../config/environment-config/environment-config.service';
import { WinstonConfigModule } from '../config/winston-config/winston-config.module';
import { WinstonConfigService } from '../config/winston-config/winston-config.service';
import { LoggerService } from './logger.service';

@Module({
  imports: [WinstonConfigModule],
  providers: [WinstonConfigService, EnvironmentConfigService, LoggerService],
  exports: [LoggerService],
})
export class LoggerModule {}
